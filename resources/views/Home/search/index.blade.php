@extends('Home.HomePublic.public')
@section('content')
<div class="container">
<div class="row">
<div class="tag-box">
    <h3>搜索：{{$name}}</h3>
</div>

<div class="col-xs-12" style="padding:0 5px;">
    @foreach($data as $row)
	<div class="col-xs-1-5 col-sm-4 col-xs-6 movie-item">
		<div class="movie-item-in">
			<a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
				<img alt="{{$row->name}}" title="{{$row->name}}" src="../uploads/video/{{$row->pic}}">
				<span class="qtag hdtag"></span>
				<div class="item-hover"></div>
			</a>
			<div class="meta">
				<h1><a href="http://demo.ctcms.cn/770/770.html" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
				<div class="otherinfo">类型：{{$row->type}}</div>
			</div>
		</div>
	</div>
	@endforeach
</div>
</div>
<!-- <div class="pager-bg" style="display:none;">
	<ul class="pagination pagination-sm">
	
		<li class="am-active"><a href="http://demo.ctcms.cn/search?wd=%E4%BA%91&amp;sort=addtime&amp;order=desc&amp;page=1">1</a></li>
	
	</ul>
</div> -->
</div>
@endsection