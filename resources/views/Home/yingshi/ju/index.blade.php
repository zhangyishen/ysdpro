@extends('Home.HomePublic.public')
@section('content')
<div class="container">
<div class="row">
<div class="tag-box">
    <h3>电视剧</h3>
</div>

<div class="col-xs-12" style="padding:0 5px;">
    @foreach($ss as $row)
	<div class="col-xs-1-5 col-sm-4 col-xs-6 movie-item">
		<div class="movie-item-in">
			<a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="">
				<img alt="{{$row->name}}" title="{{$row->name}}" src="./uploads/video/{{$row->pic}}">
				<div class="item-hover"></div>
			</a>
			<div class="meta">
				<h1><a href="/783/783.html" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
				<div class="otherinfo">类型：<a target="tags" href="">{{$row->type}}</a> </div>
			</div>
		</div>
	</div>
	@endforeach

</div>
</div>
<div class="pager-bg">
	<ul class="pagination pagination-sm">
	
		<!-- <li class='am-active'><a href='/list/1~1.html'>1</a></li><li><a href='/list/1~2.html'>2</a></li><li><a href='/list/1~3.html'>3</a></li>
	
		<li><a href="/list/1~2.html" rel="next">下一页</a></li>
		<li><a href="/list/1~3.html">末页</a></li> -->
		{{$ss->render()}}
	
	</ul>
</div>
</div>
@endsection