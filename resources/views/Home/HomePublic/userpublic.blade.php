<!DOCTYPE html>
<html class="js cssanimations"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>会员中心 - Ctcms管理系统</title>
    <meta name="keywords" content="Ctcms视频系统">
    <meta name="description" content="Ctcms视频系统">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <link rel="icon" href="../home/show/images/YSD.png">
    <link rel="stylesheet" href="../home/user/css/amazeui.css">
    <link rel="stylesheet" href="../home/user/css/style.css">
</head>
<body>

<link rel="stylesheet" href="../home/user/css/bootstrap.css">
<link rel="stylesheet" rev="stylesheet" type="text/css" media="all" href="../home/user/css/style_002.css">
<header style="background:#FFF;">
<!-- <div style="padding:5px 0;border-bottom:1px solid #DDD;">
    <div class="container">
        <span class="header-help">欢迎来到Ctcms管理系统，一起分享电影给我们带来的快乐。</span>
        <div class="pull-right">
            <a class="header-help" href="javascript:alert('');">Ctrl+D 加入收藏夹</a> -
            <a class="header-help" style="color:red;" href="http://demo.ctcms.cn/index/short">保存到桌面</a>
        </div>
    </div>
</div> -->
<div class="container" style="padding:20px 15px;padding-right:0;">
    <a class="logo pull-left" href="../home/show/images/logo.png"></a>
    <div class="pull-right">
        <form class="navbar-form pull-right" accept-charset="utf-8" action="/search" role="search" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="请输入电影名字">
            </div><button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 搜索</button>
        </form>
        <!-- <div class="header-hot ">
        <a href="http://demo.ctcms.cn/727/727.html">男与女...</a><a href="http://demo.ctcms.cn/714/714.html">刘老庄八...</a><a href="http://demo.ctcms.cn/713/713.html">纳瓦隆大...</a><a href="http://demo.ctcms.cn/712/712.html">铁血一千...</a><a href="http://demo.ctcms.cn/711/711.html">纳粹军妓...</a>      
        </div> -->
    </div>
</div>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
            <ul class="nav navbar-nav">
                <li id="nav_0"><a href="/">首页</a></li>
                
                @foreach($shou as $row)
                <li><a href="/shaixuan/{{$row->id}}" id="nav_0">{{$row->name}}</a></li>
                @endforeach
                
                <!-- <li><a href="http://demo.ctcms.cn/whole/2.html">电视剧</a></li>
                
                <li><a href="http://demo.ctcms.cn/whole/3.html">动漫</a></li>
                
                <li><a href="http://demo.ctcms.cn/whole/4.html">综艺</a></li> -->
                
                <li id="nav_1"><a href="/jiaoliu">交流圈</a></li>
                <!-- <li id="nav_1"><a href="http://demo.ctcms.cn/comm/all~1.html">电影排行</a></li> -->
               <!--  <li class="dropdown nav_opt"><a href="javascript:;" class="dropdown-toggle">排行榜<span class="caret"></span></a>
                    <ul class="dropdown-menu nav2_opt" style="display: none;">
                        <li><a href="http://demo.ctcms.cn/opt/index/new">最新上映</a></li>
                        <li><a href="http://demo.ctcms.cn/opt/index/hot">最近热播</a></li>
                        <li><a href="http://demo.ctcms.cn/opt/index/hotest">电影排名</a></li>
                    </ul>
                </li> -->
            </ul>
            <div class="pull-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown nav_user">
                            <a style="cursor: pointer" class="dropdown-toggle">{{session('user')->name}}</a>
                    <ul class="dropdown-menu nav2_user">
                        <li><a href="/user">会员中心</a></li>
                        <li><a href="/edit/{{session('user')->id}}">修改资料</a></li>
                        <li><a href="/shoucang">我的收藏</a></li>
                        <li><a href="/pay/{{session('user')->id}}">充值升级</a></li>
                        <li><a href="/login">退出登录</a></li>
                    </ul>
                </li>
            </ul>


            </div>
     </div>
</nav>
</header>
<div class="am-g am-g-fixed am-padding-top">
    <ol class="am-breadcrumb am-breadcrumb-slash">
       <li><a href="/user" class="am-icon-home">首页</a></li>
       <li class="am-active">会员中心</li>
    </ol>
    <div class="am-g">
                <div class="am-u-sm-6 am-u-md-4 am-u-lg-3">

            <table class="am-table am-table-bordered am-table-radius am-table-hover am-text-center">

                <thead>

                    <tr><th class="am-text-center">导航列表</th></tr>

                </thead>

                <tbody>

                    <tr id="index"><td><a href="/user">会员中心</a></td></tr>

                    <tr id="edit"><td><a href="/edit/{{session('user')->id}}">资料设置</a></td></tr>

                    <tr id="edit"><td><a href="/imgedit/{{session('user')->id}}">修改头像</a></td></tr>

                    <tr id="fav"><td><a href="/shoucang">我的视频</a></td></tr>

                    <tr id="comm"><td><a href="/wenzhang/{{session('user')->id}}">我的文章</a></td></tr>

                    <tr id="pay"><td><a href="/pay/{{session('user')->id}}">充值升级</a></td></tr>

                    <tr id="buy"><td><a href="/order">消费记录</a></td></tr>

                    <tr id="buy"><td><a href="/login">退出登陆</a></td></tr>

                </tbody>

            </table>

        </div>
        @section('content')
        
        @show
    </div>
</div>

<script src="../home/user/js/jquery.js"></script>
<script src="../home/user/js/amazeui.js"></script>
<script src="../home/user/js/common.js"></script>
<script>$('#index').addClass('am-primary');</script>

</body>
</html>