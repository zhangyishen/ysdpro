<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <title>YSD影视播放</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="keywords" content="YSD影视播放" />
    <meta name="description" content="YSD影视播放" />
    <link rel="icon" href="../home/show/images/YSD.png">
    <link rel="stylesheet" href="../home/show/css/bootstrap.min.css">
    <link rel="stylesheet" rev="stylesheet" type="text/css" media="all" href="../home/show/css/style.css">
    <script src="./home/show/js/jquery.min.js"></script>
    <script src="./home/show/js/bootstrap.min.js"></script>
    <script src="./home/show/js/main.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../home/show/js/html5shiv.min.js"></script>
    <script src="../home/show/js/respond.js"></script>
    <![endif]-->
</head>
<body>
<header style="background:#FFF;">
<!-- <div style="padding:5px 0;border-bottom:1px solid #DDD;">
    <div class="container">
        <span class="header-help">欢迎来到YSD视频系统，一起分享电影给我们带来的快乐。</span>
        <div class="pull-right">
            <a class="header-help" href="javascript:alert('快捷键Ctrl+D可以快速添加到收藏夹。');">Ctrl+D 加入收藏夹</a> -
            <a class="header-help" style="color:red;" href="/index/short">保存到桌面</a>
        </div>
    </div>
</div> -->
<div class="container" style="padding:20px 15px;padding-right:0;">
    <a class="logo pull-left" href="/"></a>
    <div class="pull-right">
        <form class="navbar-form pull-right" accept-charset="utf-8" action="/search" role="search" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="请输入电影名字">
            </div><button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> 搜索</button>
        </form>
        <!-- <div class="header-hot ">
		<a href="/725/725.html">别跟我谈...</a><a href="/712/712.html">铁血一千...</a><a href="/711/711.html">纳粹军妓...</a><a href="/710/710.html">忠烈杨家...</a><a href="/709/709.html">革命往事...</a>      
		</div> -->
    </div>
</div>
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
            <ul class="nav navbar-nav">
				<li><a href="/">首页</a></li>
				
                @foreach($shou as $row)
                <li><a href="/shaixuan/{{$row->id}}" id="nav_0">{{$row->name}}</a></li>
                @endforeach

				<!-- <li><a href="/whole/1.html">电影</a></li>
				
				<li><a href="/whole/2.html">电视剧</a></li>
				
				<li><a href="/whole/3.html">动漫</a></li>
				
				<li><a href="/whole/4.html">综艺</a></li> -->
				
                <li id="nav_1"><a href="/jiaoliu" target="_black">交流圈</a></li>
				<!-- <li id="nav_1"><a href="/comm/all~1.html">电影排行</a></li> -->
				<!-- <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">排行榜<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
           				<li><a href="/opt/index/new">最新上映</a></li>
           				<li><a href="/opt/index/hot">最近热播</a></li>
           				<li><a href="/opt/index/hotest">电影排名</a></li>
					</ul>
				</li> -->
			</ul>
            <div class="pull-right">
			   <ul class="nav navbar-nav">
                    @if(!\Session::get('user'))
                    <li><a href="/reg/create"> 注册</a></li>
                    <li><a href="/login/create">登录</a></li>
                    @else
                    <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{session('user')->name}}</a>
                        <ul class="dropdown-menu" role="menu">
                                <li><a href="/user">会员中心</a></li>
                                <li><a href="/edit/{{session('user')->id}}">修改资料</a></li>
                                <li><a href="/shoucang">我的收藏</a></li>
                                <li><a href="/pay/{{session('user')->id}}">充值升级</a></li>
                                <li><a href="/login">退出登录</a></li>
                        </ul>
                    </li>
                   
              </ul>
 @endif
            </div>
     </div>
</nav>
</header>
@section('content')

@show


<script>
$('#nav_0').addClass('active');
</script>
<!-- <script type="text/javascript">
$(function(){
    $("#player-box").allofthelights(); 
});
</script> -->
<footer class="footer">
   <!--  <div class="container" style="padding:0;">
    <p>免责声明：本网站所有内容都是靠程序在互联网上自动搜集而来，仅供测试和学习交流。<br/>目前正在逐步删除和规避程序自动搜索采集到的不提供分享的版权影视。</p>
    <p>若侵犯了您的权益，请发邮件通知站长，邮箱：admin@ctcms.cn</p>
    <em>♥ <a href="http://demo.ctcms.cn">Ctcms管理系统</a> · 统计代码</em>
    </div> -->
</footer>
</body>
</html>