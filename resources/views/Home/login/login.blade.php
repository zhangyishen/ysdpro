<!doctype html>

<html class="no-js">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>会员登陆 - Ctcms管理系统</title>

    <meta name="keywords" content="Ctcms视频系统" />

    <meta name="description" content="Ctcms视频系统" />

    <!-- Set render engine for 360 browser -->

    <meta name="renderer" content="webkit">

    <!-- No Baidu Siteapp-->

    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="../home/login/css/amazeui.css">

    <link rel="stylesheet" href="../home/login/css/style.css">

</head>

<body>


<link rel="stylesheet" href="../home/login/css/bootstrap.min.css">
<link rel="stylesheet" rev="stylesheet" type="text/css" media="all" href="../home/login/css/style_1.css">
<header style="background:#FFF;">

<nav class="navbar navbar-default" role="navigation">
    <div class="container">
            <ul class="nav navbar-nav">
				<li id="nav_0"><a href="/">首页</a></li>
				
				@foreach($shou as $row)
                <li><a href="/shaixuan/{{$row->id}}" id="nav_0">{{$row->name}}</a></li>
                @endforeach
				<li id="nav_1"><a href="/jiaoliu">交流圈</a></li>
				<!-- <li><a href="/whole/2.html">电视剧</a></li>
				
				<li><a href="/whole/3.html">动漫</a></li>
				
				<li><a href="/whole/4.html">综艺</a></li>
				
				
				<li class="dropdown nav_opt"><a href="javascript:;" class="dropdown-toggle">排行榜<span class="caret"></span></a>
					<ul class="dropdown-menu nav2_opt">
           				<li><a href="/opt/index/new">最新上映</a></li>
           				<li><a href="/opt/index/hot">最近热播</a></li>
           				<li><a href="/opt/index/hotest">电影排名</a></li>
					</ul>
				</li> -->
			</ul>
            <div class="pull-right">
			    <ul class="nav navbar-nav">
                    <li><a href="/reg/create"> 注册</a></li>
                    <li><a href="/login/create">登录</a></li>
              </ul>
            </div>
     </div>
</nav>
</header>

<div class="am-g am-g-fixed am-padding-top">

<div class="am-g">

  <div class="am-u-md-8 am-u-sm-centered">

    <form class="am-form" action="/login" method="post" data-am-validator>
	{{csrf_field()}}
    <fieldset>

    <legend>会员登录</legend>

    <div class="am-form-group">

      <label for="doc-vld-email-2">账号：</label>

      <input type="text" name="name" id="doc-vld-name-2" placeholder="输入账号、邮箱、手机号" required/>

    </div>

    <div class="am-form-group">

      <label for="doc-vld-pass-2">密码：</label>

      <input name="pass" type="password" id="doc-vld-pass-2" placeholder="输入登录密码" required/>

    </div>

	<div>
  
  <div class="am-form-group">

      <label for="doc-vld-email-2">验证码：</label>

      <input type="text" name="code" minlength="4" placeholder="输入下面的验证码" required="">    

      <span class="am-text-left"><img src="/code" onclick="this.src=this.src+'?a=1'"></span>

    </div>
      <span class="am-fl am-padding-bottom-sm"><a href="/reg/create" title="注册">没有账号，注册</a></span>

      <span class="am-fr am-padding-bottom-sm"><a href="/zhao/create" title="找回">忘记密码，找回</a></span>

	</div>

    <button type="submit" class="am-btn am-btn-primary am-btn-block">确定登录</button>

  </fieldset>

</form>

  </div>

</div>

</div>

<!-- <footer class="footer">
    <div class="container" style="padding:0;">
    <p>免责声明：本网站所有内容都是靠程序在互联网上自动搜集而来，仅供测试和学习交流。<br/>目前正在逐步删除和规避程序自动搜索采集到的不提供分享的版权影视。</p>
    <p>若侵犯了您的权益，请发邮件通知站长，邮箱：admin@ctcms.cn</p>
	<em>♥ <a href="http://demo.ctcms.cn">Ctcms管理系统</a> · 统计代码</em>
    </div>
</footer> -->
<script src="../home/login/js/jquery.min.js"></script>
<script src="../home/login/js/amazeui.js"></script>
<script src="../home/login/js/common.js"></script>

</body>

</html>