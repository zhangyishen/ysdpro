<!doctype html>

<html class="no-js">

<head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <title>会员注册</title>
    <style type="text/css">
      .cur{
        border:1px solid red;
      }
       .curs{
        border:1px solid green;
      }
    </style>

    <meta name="keywords" content="Ctcms视频系统" />

    <meta name="description" content="Ctcms视频系统" />

    <!-- Set render engine for 360 browser -->

    <meta name="renderer" content="webkit">

    <!-- No Baidu Siteapp-->

    <meta http-equiv="Cache-Control" content="no-siteapp"/>

    <link rel="stylesheet" href="../home/reg/css/amazeui.css">

    <link rel="stylesheet" href="../home/reg/css/style.css">

</head>

<body>


<link rel="stylesheet" href="../home/reg/css/bootstrap.min.css">
<link rel="stylesheet" rev="stylesheet" type="text/css" media="all" href="../home/reg/css/style_1.css">
<header style="background:#FFF;">
<div style="padding:5px 0;border-bottom:1px solid #DDD;">
    <div class="container">
        <span class="header-help">欢迎来到Ctcms管理系统，一起分享电影给我们带来的快乐。</span>
        <div class="pull-right">
            <a class="header-help" href="javascript:alert('快捷键Ctrl+D可以快速添加到收藏夹。');">Ctrl+D 加入收藏夹</a> -
            <a class="header-help" style="color:red;" href="/index/short">保存到桌面</a>
        </div>
    </div>
</div>


</header>

<div class="am-g am-g-fixed am-padding-top">

<div class="am-g">

  <div class="am-u-md-6 am-u-sm-centered">

    <form class="am-form" action="/reg" method="post" id="ff" data-am-validator>
      {{csrf_field()}}
    <fieldset>

    <legend>会员注册</legend>

    <div class="am-form-group">

      用户名 : <input type="text" class="" name="user" placeholder="输入用户名" required reminder="用户名不能为空"/><span></span>

    </div>

    <div class="am-form-group">

      手机号 : <input type="text" class="" name="phone" placeholder="输入手机号" required reminder="请输入正确的手机号"/><span></span>

    </div>

    <div class="am-form-group">

      输入校验码 : <input name="code" class="" type="text" placeholder="输入校验码" required reminder="请输入正确的校验码"/><span></span>
      <a href="javascript:void(0)" class="btn btn-success" id="ss">获取校验码</a>

    </div>

    <div class="am-form-group">

      密码 : <input type="password" class="" name="pass" placeholder="" required reminder="请输入正确的密码"/><span></span>

    </div>

    <div class="am-form-group">

      重复密码 : <input type="password" class="" name="repass" placeholder="" required reminder="重复密码不正确"/><span></span>

    </div>


     <span class="am-fr am-padding-top-sm"><a href="/login/create" title="登录">已有账号，登录</a></span>
    <button type="submit" class="am-btn am-btn-primary am-btn-block">注账账号</button>

  </fieldset>

</form>

  </div>

</div>

</div>


<script src="../home/reg/js/jquery.min.js"></script>
<!-- <script src="../home/reg/js/amazeui.js"></script> -->
<!-- <script src="../home/reg/js/common.js"></script> -->

</body>
  <script type="text/javascript">
    var PHONE=false;
    var CODE=false;
    // alert($);
    //获取每个 input 绑定获取焦点事件
    $("input").focus(function(){
      reminder=$(this).attr('reminder');
      $(this).next("span").css('color','red').html(reminder);
      //添加类样式
      $(this).addClass('cur');
      //移除类样式
      $(this).removeClass('curs');
    });

    //绑定手机号 绑定失去焦点事件
    $("input[name='phone']").blur(function(){
      //获取手机号
      p=$(this).val();
      o=$(this);
      //正则匹配 match 匹配不到正则的话 返回null
      if(p.match(/^\d{11}$/)==null){
        $(this).next("span").css("color",'red').html('手机号码不合法');
        $(this).addClass('cur');
        PHONE=false;
      }else{
        //判断手机号是否重复
        $.get("/checkphone",{p:p},function(data){
            // alert(data);
            if(data==1){
              //手机号码已经注册
              o.next("span").css("color",'red').html('手机号码被注册');
              o.addClass('cur');
              //把获取校验码按钮设置为禁用
              $("#ss").attr('disabled',true);
              PHONE=false;
            }else{
              //手机号可以使用
              o.next("span").css("color",'green').html('手机可以使用');
              o.addClass('curs');
              //把获取校验码按钮设置为激活
              $("#ss").attr('disabled',false);
              PHONE=true;
            }
        });
      }

    });

    //获取密码,绑定失去焦点事件
  $("input[name='pass']").blur(function(){
    //获取密码
    mm=$(this).val();
    //ajax里$(this)解析不了
    mmo=$(this);
    //正则匹配 match 匹配不到的话,返回null
    if(mm.match(/^[\w]{6,12}$/)==null){
      $(this).next("span").css('color','red').html('密码不符合');
      $(this).addClass('cur');
      MIMA=false;
    }else{
      //密码可以使用
      mmo.next("span").css('color','green').html('密码可以使用');
      mmo.addClass('curs');
      MIMA=true;
    }
  });
  //获取重复密码,绑定失去焦点事件
  $("input[name='repass']").blur(function(){
    //获取重复密码
    cfm=$(this).val();
    // alert(cfm);
    //ajax里$(this)解析不了
    cfo=$(this);
    //判断重复密码是否一致
    $.get("/checkmima",{cfm:cfm,mm:mm},function(data){
      // alert(data);
      if(data==1){
        //重复密码一致
        cfo.next("span").css('color','green').html('重复密码一致');
        cfo.addClass('curs');
        CFMIMA=true;
      }else if(data==0){
        //重复密码不一致
        cfo.next("span").css('color','red').html('重复密码不一致');
        cfo.addClass('cur');
        CFMIMA=false;
      }else if(data==2){
        cfo.next("span").css('color','red').html('密码不能为空');
        cfo.addClass('cur');
        CFMIMA=false;
      }
    });
  });



    //获取发送短信校验码按钮 绑定单机时间
    $("#ss").click(function(){
      s=$(this);
      //获取注册的手机号
      pp = $("input[name='phone']").val();
      //Ajax
      $.get("/sendphone",{pp:pp},function(data){
        // alert(data);
        if(data.code==000000){
          m=60;
          //定时器
          mytime = setInterval(function(){
            m--;
            //赋值给当前按钮
            s.html(m+"秒后重新发送");
            s.attr('disabled',true);
            //判断
            if(m==0){
              //清除定时器
              clearInterval(mytime);
              s.html("重新发送");
              s.attr('disabled',false);
            }
          },1000);
        }
      },'json');
    });

    //获取输入验证码input
    $("input[name='code']").blur(function(){
      c=$(this);
      //获取输入的校验码
    code=$(this).val();
      $.get("/checkcode",{code:code},function(data){
        if(data==1){
          //校验码一致
          c.next("span").css('color','green').html('校验码一致');
          c.addClass('curs');
          CODE=true;
        }else if(data==2){
          //校验码不一致
          c.next("span").css('color','red').html('校验码不一致');
          c.addClass('cur');
          CODE=false;
        }else if(data==3){
          //校验码为空
          c.next("span").css('color','red').html('校验码为空');
          c.addClass('cur');
          CODE=false;
        }else if(data==4){
          //校验码过期
          c.next("span").css('color','red').html('校验码已过期');
          c.addClass('cur');
          CODE=false;
        }
      })
    });

    //表单提交
    $("#ff").submit(function(){
      //trigger 某个元素触发某个事件
      $('input').trigger("blur");
      if(PHONE && CODE){
        return true;//成功提交
      }else{
        return false;//阻止提交
      }
      
    });


    $("input[name='user']").blur(function(){
      //获取手机号
      user=$(this).val();
      // alert(u);
      o=$(this);
      if(user==''){
        $(this).next("span").css("color",'red').html('用户名不能为空');
        $(this).addClass('cur');
        // PHONE=false;
      }else{
        //判断用户名是否重复
        $.get("/checkuser",{user:user},function(data){
            // alert(data);
            // alert(data);
            if(data==1){
              //手机号码已经注册
              o.next("span").css("color",'red').html('用户名被注册');
              o.addClass('cur');
            }else{
              //手机号可以使用
              o.next("span").css("color",'green').html('用户名可以使用');
              o.addClass('curs');
            }

        });
      }

    });
  </script>
</html>