@extends("Home.HomePublic.public")
@section('content')
<link rel="stylesheet" type="text/css" href="../home/huifu/comm.css">
<div class="container">
<div class="row">
    <article class="col-xs-8 padding-right-0">
<div class="panel panel-default topic-box">
    <div class="panel-heading topic-header">
    <div class="pull-right">
        <a href="javascript:;"><img class="avatar-large" src="../uploads/{{$data->u_pic}}" alt="avatar"></a>
    </div>
    <!-- <div class="topic-breadcrumb">
        <a href="http://demo.ctcms.cn/">Ctcms管理系统</a> / <a href="http://demo.ctcms.cn/comm/all~1.html">圈子</a> / <a href="http://demo.ctcms.cn/comm/2~1.html">影迷交流</a>
    </div> -->
    <h1 class="topic-title text-break">{{$data->title}}</h1>
        <span class="topic-meta">
            <span class="list-username">作者：<a href="javascript:;">{{$data->uname}}</a></span>
             •
            <span>发布于：{{date("Y-m-d H:i:s",$data->addtime)}}</span>
             •
            <span>浏览量：{{$data->liulan}}</span>
        </span>
    </div>
    <div class="panel-body topic-content">
    

	{!!$data->content!!}


	</div>
    <div class="panel-footer topic-footer">
        <a style="margin-left:0;" href="javascript:void(0);" id="fav" s="0" onclick="collect('/comm/collect')"><span class="glyphicon glyphicon-star-empty"></span><font id="sc">加入收藏</font></a>
        <div class="pull-right">
        	<input type="hidden" id="tie" name="tie" value="{{$data->id}}">
			<a id="z" href="javascript:;" onclick="clike({{$data->id}})"><span class="glyphicon glyphicon-thumbs-up"></span><font id="zhits"> {{$data->zan}} 赞</font></a>
        </div>
    </div>
</div>


<div class="comment">
    <ul class="list-group" style="margin-bottom:5px;">
	
		<li class="list-group-item list-head">共 <font id="hfnum">{{$num}}</font> 条回复</li>
	
	
	<li class="list-group-item list-topic-item reply262">
		<table style="width:100%;overflow:auto;">
        <tbody>
        	@foreach($huifu as $row)
            <tr>
				<td width="48px" valign="top" align="center">
					<a href="javascript:;"><img class="avatar-middle" title="{{$row->user_name}}" src="../uploads/{{$row->user_pic}}"></a>
				</td>
				<td width="10px"></td>
				<td width="auto">
					<div class="reply-author">
						<a href="javascript:;">
							@if($row->user_name==$data->uname)
								楼主
							@else
								{{$row->user_name}}
							@endif
						</a> 
						<span class="list-time">{{date('Y-m-d H:i:s',$row->addtime)}}</span>
					</div>
					<div class="reply-content" id="reply62">
						{!!$row->content!!}
						<div style="overflow:auto;"><a href="javascript:;" onclick="reply({{$row->id}},{{$data->id}},'回复：{{$row->user_name}}')" class="reply-btn">回复</a></div>
						@foreach($erhui as $val)
							@if($val->ping_id==$row->id)
							<div class="reply263" style="border-top:1px solid rgb(230,230,230);width:100%;margin:10px 0;"></div>
							<div class="container-fluid reply263">
								<div class="reply-author">
									<a href="javascript:;"><img style="border-radius:3px;" title="{{$val->u_name}}" src="../uploads/{{$val->user_pic}}" width="28px"></a>
									<a href="javascript:;">{{$val->u_name}}</a> 
									<span class="list-time">{{date("Y-m-d H:i:s",$val->addtime)}}</span>
								</div>
								<div class="reply-content reply-child">
									{!!$val->content!!}
									<!-- <div style="overflow:auto;"><a href="javascript:;" onclick="reply(62,356,'回复：q573927428')" class="reply-btn">回复</a></div> -->
								</div>
							</div>
							@endif
						@endforeach
						
					</div>
				</td>
			</tr>
			<input type="hidden" id="ping_id" value="{{$row->id}}">
			@endforeach
		</tbody>
		</table>
	</li>
	
	</ul>
    <div style="clear:both;"></div>
</div>
<div class="pager-bg" style="display:none;">
	<ul class="pagination pagination-sm">
	
		<li class="am-active"><a href="http://demo.ctcms.cn/article/3~1.html">1</a></li>
	
	</ul>
</div>

<form action="/huifu" method="post">
	{{csrf_field()}}
<div class="panel panel-default list-head">
        <!-- <input type="hidden" id="uid" value="{{$data->uid}}"> -->
        <input type="hidden"  name="tie_id" value="{{$data->id}}">
        <div class="panel-heading title-breadcrumb">
			<div style="float:left;">{{session('user')->name}}回复：楼主</div>
			<!-- <div style="float:left;padding-left:15px;color:red;" id="rpnotice"></div> -->
			<!-- <div id="closereply" style="float:right;display:none;"> -->
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
		</div>
		
			<div ><textarea  class="form-control" name="content" rows="5"></textarea></div>
        <div ><button type="submit" class="btn btn-primary btn-block">回复</button></div>
		</form>  
</div>
</form>

<div class="panel panel-default list-head" id="reply-box" style="display: none">
        
        <input type="hidden" id="tie_id" value="{{$data->id}}">
        <div class="panel-heading title-breadcrumb">
			<div style="float:left;" id="reply-to-box">{{session('user')->name}}回复：楼主</div>
			<div style="float:left;padding-left:15px;color:red;" id="rpnotice"></div>
			<div id="closereply" style="float:right;display:none;">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
		</div>
        <div class="panel-body"><textarea id="replycontent" class="form-control" rows="5"></textarea></div>
        <div class="panel-footer"><button onclick="rpsubmit('/comm/reply');" class="btn btn-primary btn-block">回复</button></div>
</div>

</article>
   
</div>
</div>
<script>
$('#nav_1').addClass('active');
</script>
<!-- <script type="text/javascript" src="../home/huifu/main.js"></script> -->
<script type="text/javascript" src="../home/huifu/jquery.js"></script>
<script type="text/javascript" src="../home/huifu/comm.js"></script>
    <script src="../home/show/js/jquery.min.js"></script>
    <script src="../home/show/js/bootstrap.min.js"></script>
    <script src="../home/show/js/main.js"></script>
	<script type="text/javascript">
		var id = $("#tie").val();
		// alert(id);
		p = $("#z");
		$.get('/chazan',{id:id},function(data){
			// alert(data);
			if(data==1){
				p.css('color','blue');;
			}else if(data==2){
				p.css('color','777777');
			}
		})
	</script>
    <script type="text/javascript">
    	function clike(id){
    		// alert(id);
    		p = $("#z");
    		$.get('/zan',{id:id},function(data){
    			// alert(data);
    			if(data==1){
    				// alert('点赞成功');
    				location.href='/jiaoliu/'+id;
    				// p.css('color','blue');
    			}else if(data==3){
    				// alert('取消点赞');
    				location.href='/jiaoliu/'+id;
    				// p.css('color','777777');
    			}else if(data==2){
    				alert('点赞失败');
    			}
    		});
    	}
    </script>
@endsection