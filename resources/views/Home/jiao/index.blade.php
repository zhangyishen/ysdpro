@extends("Home.HomePublic.public")
@section('content')
<link rel="stylesheet" type="text/css" href="../home/jiao/comm.css">
<script type="text/javascript" src="../home/jiao/comm.js"></script>
<script type="text/javascript" src="../home/jiao/main.js"></script>
<script type="text/javascript" src="../home/jiao/jquery.js"></script>
<div class="container">
<div class="row">
<div class="col-xs-8 padding-right-0">
    
<div class="panel panel-default topic-list">
    <div class="panel-heading">
        全部文章
        <div class="pull-right">
            <a class="head-btn btn-blue" href="/jiaoliu/create">发表</a>
        </div>
    </div>
    <ul class="list-group">
		
		@foreach($tie as $row)
		<li class="list-group-item">
				<table style="width:100%;overflow:auto;">
				<tbody>
					
					<tr >
						<td width="48px" valign="top" align="center">
							<a href="javascript:;"><img title="头像" class="avatar-middle" src="../uploads/{{$row->tu_pic}}" width="48px;"></a>
						</td>
						<td width="10px"></td>
						<td width="auto">
							<h1 class="list-post-title"><a onclick="liulan({{$row->tid}})" target="_blank" href="/jiaoliu/{{$row->tid}}">{{$row->ttitle}}</a></h1>
							<div class="list-meta">
								<span class="list-meta-node" ><a href="">{{$row->typename}}</a></span>&nbsp;•
								<span class="list-time">{{date("Y-m-d H:i:s",$row->taddtime)}}</span>&nbsp;•
								<span class="list-username"><a href="javascript:;">{{$row->tuname}}</a></span>
							</div>
						</td>
						<td width="50px" align="right">
							<span class="pull-right list-reply-count">{{$row->tnum}}</span>
						</td>
					</tr>

				</tbody>
				</table>
		</li>
		@endforeach
		
		
	</ul>
</div>
<!-- <div class="pager-bg" style="display:none;">
	<ul class="pagination pagination-sm">
	
		<li class="am-active"><a href="http://demo.ctcms.cn/comm/all~1.html">1</a></li>
	
	</ul>
</div> -->
</div>
    <div class="col-xs-4">     
		<div class="panel panel-default">
			<div class="panel-heading title-breadcrumb">最新加入会员：</div>
			<div class="panel-body" style="padding:5px 5px;overflow:auto;clear:both;">
				@foreach($user as $row)
				<a href="javascript:;" class="col-xs-2 viewer-list-avatar">
					<img src="../uploads/{{$row->top_pic}}" title="">
				</a>
				@endforeach
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading title-breadcrumb">所有圈子：</div>
			<div class="panel-body tags">
				<a class="tag active" href="/jiaoliu">全部</a>
				@foreach($tietype as $row)
				<a class="tag" href="/jiaotype/{{$row->id}}">{{$row->name}}</a>   
				@endforeach 
				
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading title-breadcrumb">最新文章：</div>
			<div class="list-group">
                @foreach($xin as $row)
                <a href="/jiaoliu/{{$row->id}}" class="list-group-item latest-topic-list">{{$row->title}}</a>
                @endforeach
			</div>
		</div>    
	</div>
</div>
</div>
<script>
$('#nav_1').addClass('active');
</script>

<script type="text/javascript">
	function liulan(id){
		// alert(id);
		$.get('/liulan',{id:id},function(data){
			// alert(data);
		});
	}
</script>
@endsection