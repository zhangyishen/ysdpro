@extends("Home.HomePublic.public")
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-8 padding-right-0">
            <form action="/jiaoliu" method="post">
            	{{csrf_field()}}
                <input id="edtitle" name="title" type="text" class="form-control create-box" placeholder="请输入标题">

                <div class="wangEditor-container">
                   <script id="editor" type="text/plain" name="content" style="width:100%;height:500px;"></script>
                </div>
               
                <div style="padding:15px;background:#FFF;margin:15px 0;">

                    <div class="form-group create-box">

                        <label for="topic-type">分类：</label>

                        <select name="type" id="circle" class="form-control-inline">
                            @foreach($tietype as $row)
                            <option value="{{$row->id}}" selected="selected">{{$row->name}}</option>
                            @endforeach
                        </select>

                        <!-- <button type="submit" id="origin_edsubmit" class="btn btn-primary" style="display: none;">发布</button> -->
                        <button type="submit" id="edsubmit" class="btn btn-primary">发布</button>	
                       <!--  <span id="ednotice"></span>
						<span id="popup-captcha"><div class="gt_input">
							<input class="geetest_challenge" type="hidden" name="geetest_challenge">
							<input class="geetest_validate" type="hidden" name="geetest_validate">
							<input class="geetest_seccode" type="hidden" name="geetest_seccode"></div>
						</span> -->

                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-4">
            <div class="panel panel-default">
                <div class="panel-heading title-breadcrumb">最新加入会员：</div>
                <div class="panel-body" style="padding:5px 5px;overflow:auto;clear:both;">
                    @foreach($user as $row)
					<a href="javascript:;" class="col-xs-2 viewer-list-avatar">
						<img src="../uploads/{{$row->top_pic}}" title="">
					</a>
					@endforeach
                   
                </div>
            </div>
            <!-- <div class="panel panel-default">
                <div class="panel-heading title-breadcrumb">所有圈子：</div>
                <div class="panel-body tags">	
                	<a class="tag active" href="http://demo.ctcms.cn/comm/all~1.html">全部</a>
					<a class="tag" href="">推荐电影</a> 	
					<a class="tag" href="">影迷交流</a> 
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading title-breadcrumb">最新文章：</div>
                <div class="list-group"> 


                	<a href="http://demo.ctcms.cn/article/60~1.html" class="list-group-item latest-topic-list">fffsdfsdfsdfsdfd</a>
 			

                </div>
            </div> -->
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8" src="../home/tie/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="../home/tie/ueditor.all.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="../home/tie/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
    	var ue = UE.getEditor('editor');
    </script>


@endsection