@extends("Home.HomePublic.userpublic")
@section('content')
<div class="am-u-sm-6 am-u-md-8 am-u-lg-9">

            <form class="am-form am-form-horizontal" action="/img/{{session('user')->id}}" method="post" data-am-validator="" novalidate="novalidate" enctype="multipart/form-data">
      {{csrf_field()}}
      {{method_field("PUT")}}
            <fieldset>

            <ul class="am-nav am-nav-tabs am-margin-bottom-lg" style="margin-top:0;">

                <li class="am-active"><a href="/img/{{session('user')->id}}">修改头像</a></li>

                <!-- <li><a href="http://demo.ctcms.cn/user/edit/logo">修改头像</a></li>

                <li><a href="http://demo.ctcms.cn/user/edit/pass">修改密码</a></li> -->

            </ul>

              <div class="am-form-group">

                <label for="doc-ipt-1" class="am-fl am-form-label">头&nbsp;&nbsp;&nbsp;&nbsp;像</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-ipt-1" value="" type="file" name="top_pic">

                </div>

              </div>

              <div class="am-form-group">

                <div class="am-u-sm-10 am-u-sm-offset-1">

                  <button type="submit" class="am-btn am-btn-primary">提交修改</button>

                </div>

              </div>

            </fieldset>

            </form>

        </div>
@endsection