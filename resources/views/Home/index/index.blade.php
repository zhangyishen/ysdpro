@extends('Home.HomePublic.public')
@section('content')

<div class="container">
    @if(session('success'))
   <div class="alert alert-success" id="disx">{{session('success')}}</div>
@endif

                         
@if(session('error'))
      <div class="alert alert-danger" id="disx">{{session('error')}}</div>
@endif
<div class="announcement alert-success" style="margin:-5px 0 10px 0;"><span class="glyphicon glyphicon-comment"></span>公告：{{$gonggao->content}}</div>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    
        <li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
        @for($i=0;$i<$count;$i++)
        <li data-target="#carousel-example-generic" data-slide-to="{{$i}}"></li>
        @endfor
    </ol>
    <div class="carousel-inner" role="listbox">
    
        <div class="item active">
            <a href=""><img width="100%" src="./home/index/picture/005diyl9gy1fwo8e0we7cj30s20cin17.jpg" alt="大轰炸"></a>
            <div class="carousel-caption">大轰炸</div>
        </div>

        @foreach($img as $row)
        <div class="item">
            <a href=""><img width="100%" height="400px" src="./uploads/lunbo/{{$row->pic}}" alt="{{$row->name}}"></a>
            <div class="carousel-caption">{{$row->name}}</div>
        </div>
        @endforeach
    </div>
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">上一张</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">下一张</span>
    </a>
</div>
<div class="row rec-box">
    @foreach($vi as $row)
    @if($row->shangjia==0)
    <div class="col-xs-3" style="padding:0 10px;">
        <div class="movie-item-in">
        <a style="position:relative;display:block;height:135px;overflow:hidden;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
            <img alt="{{$row->name}}" title="{{$row->name}}" src="./uploads/video/{{$row->pic}}">
        <div class="item-hover"></div>
        </a>
        <div class="meta">
            <h1><a href="/show/{{$row->id}}" target="_blank" title="{{$row->name}}">{{$row->name}}</a></h1>
        </div>
        </div>
    </div>
    @endif   
    @endforeach
   <!--  <div class="col-xs-3" style="padding:0 10px;">
        <div class="movie-item-in">
        <a style="position:relative;display:block;height:135px;overflow:hidden;" title="证人" target="_blank" href="/783/783.html">
            <img alt="证人" title="证人" src="./home/index/picture/p665243753.jpg">
        <div class="item-hover"></div>
        </a>
        <div class="meta">
            <h1><a href="/783/783.html" target="_blank" title="证人">证人</a></h1>
        </div>
        </div>
    </div>   
    
    <div class="col-xs-3" style="padding:0 10px;">
        <div class="movie-item-in">
        <a style="position:relative;display:block;height:135px;overflow:hidden;" title="待绽蔷薇" target="_blank" href="/760/760.html">
            <img alt="待绽蔷薇" title="待绽蔷薇" src="./home/index/picture/p1902182383.jpg">
        <div class="item-hover"></div>
        </a>
        <div class="meta">
            <h1><a href="/760/760.html" target="_blank" title="待绽蔷薇">待绽蔷薇</a></h1>
        </div>
        </div>
    </div>   
    
    <div class="col-xs-3" style="padding:0 10px;">
        <div class="movie-item-in">
        <a style="position:relative;display:block;height:135px;overflow:hidden;" title="啊，男孩" target="_blank" href="/761/761.html">
            <img alt="啊，男孩" title="啊，男孩" src="./home/index/picture/p2206183838.jpg">
        <div class="item-hover"></div>
        </a>
        <div class="meta">
            <h1><a href="/761/761.html" target="_blank" title="啊，男孩">啊，男孩</a></h1>
        </div>
        </div>
    </div>    -->
    
</div>

<div class="row">
    <div class="col-xs-9 index-box padding-right-5">
        
        <h2 class="index-title">最新电影<span><a target="_blank" href="/ying">更多&gt;&gt;</a></span></h2>
        
        <div class="row" style="margin:0 -10px;">
            @foreach($ss as $row)
            @if($row->shangjia==0)
            <div class="col-xs-3 movie-item">
                <div class="movie-item-in">
                    <a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
                        <img alt="{{$row->name}}" title="{{$row->name}}" style="height: 250px;" src="./uploads/video/{{$row->pic}}">
                        <span class="qtag bdtag"></span>
                        <div class="item-hover"></div>
                    </a>
                    <div class="meta">
                        <h1><a href="/show/{{$row->id}}" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
                        <div class="otherinfo">类型：<a target="tags" href="/show/{{$row->id}}">{{$row->type}}</a> </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
        
        </div>    
    </div>
    <div class="col-xs-3 sidebar padding-left-10" style="width:255px;margin-left:5px;">
        <h3>近期热门</h3>
        <div class="list-group">
            
            @foreach($paihang as $row)
            @if($row->shangjia==0)
            <a title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}" class="list-group-item"><span class="square-item-blue">{{$row->id}}</span>{{$row->name}}</a>
            @endif
            @endforeach
            
            <!-- <a title="啊，男孩" target="_blank" href="/761/761.html" class="list-group-item"><span class="square-item-red">2</span>啊，男孩</a>
            
            <a title="这个杀手不太冷" target="_blank" href="/784/784.html" class="list-group-item"><span class="square-item-red">3</span>这个杀手不太冷</a>
            
            <a title="海扁王" target="_blank" href="/762/762.html" class="list-group-item"><span class="square-item-blue">4</span>海扁王</a>
            
            <a title="待绽蔷薇" target="_blank" href="/760/760.html" class="list-group-item"><span class="square-item-blue">5</span>待绽蔷薇</a>
            
            <a title="功夫小蝇" target="_blank" href="/781/781.html" class="list-group-item"><span class="square-item-blue">6</span>功夫小蝇</a>
            
            <a title="第一滴血" target="_blank" href="/777/777.html" class="list-group-item"><span class="square-item-blue">7</span>第一滴血</a>
            
            <a title="内在美" target="_blank" href="/665/665.html" class="list-group-item"><span class="square-item-blue">8</span>内在美</a>
            
            <a title="无相劫" target="_blank" href="/767/767.html" class="list-group-item"><span class="square-item-blue">9</span>无相劫</a>
            
            <a title="诸神之战" target="_blank" href="/765/765.html" class="list-group-item"><span class="square-item-blue">10</span>诸神之战</a> -->
            
        </div>
        <h3>本周推荐</h3>
        <div class="list-group">
            @foreach($paihang2 as $row)
            @if($row->shangjia==0)
            <a title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}" class="list-group-item"><span class="square-item-blue">{{$row->id}}</span>{{$row->name}}</a>
            @endif
            @endforeach
            <!-- <a title="海扁王" target="_blank" href="/762/762.html" class="list-group-item"><span class="square-item-red">2</span>海扁王</a>
            
            <a title="待绽蔷薇" target="_blank" href="/760/760.html" class="list-group-item"><span class="square-item-red">3</span>待绽蔷薇</a>
            
            <a title="无相劫" target="_blank" href="/767/767.html" class="list-group-item"><span class="square-item-blue">4</span>无相劫</a>
            
            <a title="微笑人生" target="_blank" href="/766/766.html" class="list-group-item"><span class="square-item-blue">5</span>微笑人生</a>
            
            <a title="诸神之战" target="_blank" href="/765/765.html" class="list-group-item"><span class="square-item-blue">6</span>诸神之战</a>
            
            <a title="这不是斯巴达" target="_blank" href="/763/763.html" class="list-group-item"><span class="square-item-blue">7</span>这不是斯巴达</a>
            
            <a title="101真狗" target="_blank" href="/754/754.html" class="list-group-item"><span class="square-item-blue">8</span>101真狗</a>
            
            <a title="逗阵儿" target="_blank" href="/759/759.html" class="list-group-item"><span class="square-item-blue">9</span>逗阵儿</a>
            
            <a title="金太狼的幸福生活" target="_blank" href="/758/758.html" class="list-group-item"><span class="square-item-blue">10</span>金太狼的幸福生活</a> -->
            
        </div>    
    </div>
    
    <div class="col-xs-12 index-box">
        <h2 class="index-title">最新电视剧<span><a target="_blank" href="/ju">更多&gt;&gt;</a></span></h2>
        <div style="margin:0 -10px;">
            @foreach($ss1 as $row)
            @if($row->shangjia==0)
            <div class="col-xs-1-5 movie-item">
                <div class="movie-item-in">
                    <a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
                        <img alt="{{$row->name}}" title="{{$row->name}}" src="./uploads/video/{{$row->pic}}">
                        <div class="item-hover"></div>
                    </a>
                    <div class="meta">
                        <h1><a href="/show/{{$row->id}}" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
                        <div class="otherinfo">主演：{{$row->to_star}}</div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach 
        </div>
    </div>
    
    <div class="col-xs-12">
        
    </div>
    
    
    <div class="col-xs-12 index-box">
        <h2 class="index-title">最新动漫<span><a target="_blank" href="/man">更多&gt;&gt;</a></span></h2>
        <div style="margin:0 -10px;">
            
            <!-- <div class="col-xs-1-5 movie-item">
                <div class="movie-item-in">
                    <a style="position:relative;display:block;" title="史酷比:舞台风波" target="_blank" href="/639/639.html">
                        <img alt="史酷比:舞台风波" title="史酷比:舞台风波" src="./home/index/picture/p2101196903.jpg">
                        <div class="item-hover"></div>
                    </a>
                    <div class="meta">
                        <h1><a href="/639/639.html" target="_blank" title="史酷比:舞台风波">史酷比:舞台风波</a><em> - 0</em></h1>
                        <div class="otherinfo">主演：伊莎贝拉·阿塞尔斯,特罗伊·贝克,杰夫·贝内特,Eric,韦恩·布莱迪</div>
                    </div>
                </div>
            </div> -->
            
                    
            @foreach($ss2 as $row)
            @if($row->shangjia==0)
            <div class="col-xs-1-5 movie-item">
                <div class="movie-item-in">
                    <a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
                        <img alt="{{$row->name}}" title="{{$row->name}}" src="./uploads/video/{{$row->pic}}">
                        <div class="item-hover"></div>
                    </a>
                    <div class="meta">
                        <h1><a href="/show/{{$row->id}}" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
                        <div class="otherinfo">主演：{{$row->to_star}}</div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach 
        </div>
    </div>
    
    
    <div class="col-xs-12 index-box">
        <h2 class="index-title">最新综艺<span><a target="_blank" href="/zong">更多&gt;&gt;</a></span></h2>
        <div style="margin:0 -10px;">
            @foreach($ss3 as $row)
            @if($row->shangjia==0)
            <div class="col-xs-1-5 movie-item">
                <div class="movie-item-in">
                    <a style="position:relative;display:block;" title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}">
                        <img alt="{{$row->name}}" title="{{$row->name}}" src="./uploads/video/{{$row->pic}}">
                        <div class="item-hover"></div>
                    </a>
                    <div class="meta">
                        <h1><a href="/show/{{$row->id}}" target="_blank" title="{{$row->name}}">{{$row->name}}</a><em> - {{$row->year}}</em></h1>
                        <div class="otherinfo">主演：{{$row->to_star}}</div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach 
                    
        </div>
    </div>
    
    
</div>
</div>
<script src="../home/user/js/jquery.js"></script>
<script type="text/javascript">
     $("#disx").click(function(){
    $(this).fadeOut("slow");
  });
</script>
@endsection