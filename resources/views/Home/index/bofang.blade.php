@extends('Home.HomePublic.public')
@section('content')
    <link href="http://sdk-release.qnsdk.com/qiniuplayer-0.3.9.min.css" rel="stylesheet">
    
<div class="container" style="position:relative;">
<div class="container-fluid" style="background:#FFF;padding:15px;">
    <ol class="breadcrumb">
        <li><a href="/">首页</a></li>
        <li><a target="_blank" href="/list/8~1.html">{{$data->type}}</a></li>
		<li class="active">{{$data->name}}-在线观看</li>      
    </ol>
    <div class="row">
        <div class="col-xs-12">
            <h1 class="movie-title">{{$data->name}}-在线观看</h1>
            <div class="extra-btn">
                 <a id="switch"></a>
            </div>
        </div>
        <div class="col-xs-12">
        <div id="player-box">
        	    <video id="demo-video" class="video-js vjs-big-play-centered" src="{{$data->video_address}}"></video>
            <!-- <div id="player"><iframe src="http://demo.ctcms.cn/packs/player/ydisk/index.php?url={{$data->video_address}}" marginwidth="0" marginheight="0" border="0" scrolling="no" frameborder="0" topmargin="0" width="100%" height="100%"></iframe></div> -->
        </div>
        </div>
        <div class="col-xs-12">
            <div style="padding:15px 0;text-align:right;">
				<a class="btn btn-js-action btn-sm" href="javascript:;" onclick="$('#jsModal').modal('show');"><span class="glyphicon glyphicon-list"></span> 选集</a>
				<a href="" class="btn btn-js-action btn-sm"><span class="glyphicon glyphicon-step-backward"></span> 上集</a>
				<a href="" class="btn btn-js-action btn-sm">下集 <span class="glyphicon glyphicon-step-forward"></span></a>
			</div>
			<!-- 选集框 -->
			<div class="modal fade" id="jsModal" tabindex="-1" resid="" role="dialog" aria-labelledby="jsModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="welcomeModalLabel">请选择：</h4>
						</div>
						<div class="modal-body" style="overflow:auto;max-height:350px;">
						
						<!-- <div class="row tv-js-list">
							
							<div class="col-xs-1 play-ji play-8"><a href="/play/781~0~0.html" style="height:36px;line-height:36px;" class="tv-js-btn">HD超清</a></div>
							
						</div> -->
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					</div>
				</div>
			</div>
		</div>
        <!-- <div class="alert-x alert-warning" style="line-height:28px;padding:5px 10px;">按键 Ctrl+D 把 <b>Ctcms管理系统</b> 加入收藏夹，下次找电影更方便！！<div style="float:right;"><div style="height:28px;">
		<div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a title="分享到QQ空间" href="#" class="bds_qzone" data-cmd="qzone"></a><a title="分享到新浪微博" href="#" class="bds_tsina" data-cmd="tsina"></a><a title="分享到腾讯微博" href="#" class="bds_tqq" data-cmd="tqq"></a><a title="分享到人人网" href="#" class="bds_renren" data-cmd="renren"></a><a title="分享到微信" href="#" class="bds_weixin" data-cmd="weixin"></a></div>
		<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=1.js?cdnversion='+~(-new Date()/36e5)];</script>
		</div></div><div style="float:right;">电影不错？分享给朋友：</div></div> -->
        </div>
        <div class="col-xs-9 movie-info" style="width:780px;">
            <div class="row">
            <div class="col-xs-12"><h2>主演：</h2></div>
            <div class="col-xs-12"><p>{{$data->director}}</p></div></div>
            <div class="row">
                <div class="col-xs-12"><h2>剧情简介：</h2></div>
                <div class="col-xs-12"><p>{{$data->jieshao}}</p>
                <a class="btn btn-success" target="_blank" href="{{$data->video_address}}"><i class="glyphicon glyphicon-download-alt"></i> {{$data->name}}高清下载</a>                </div>
                <div class="col-xs-12"><h2>精彩推荐</h2></div>
				<div style="padding:0 10px;">
					@foreach($vi as $row)
					<div class="col-xs-2 movie-recommened">
						<a style="position:relative;display:block;" target="_blank" title="{{$row->name}}" href="/show/{{$row->id}}">
							<img width="100%" src="../uploads/video/{{$row->pic}}" />
							<div class="item-hover"></div>
						</a>
						<a target="_blank" title="{{$row->name}}" href="/show/{{$row->id}}"><h5>{{$row->name}}</h5></a>
					</div>  
					@endforeach
				
					<!-- <div class="col-xs-2 movie-recommened">
						<a style="position:relative;display:block;" target="_blank" title="无相劫" href="/767/767.html">
							<img width="100%" src="static/picture/p1990617008.jpg" />
							<div class="item-hover"></div>
						</a>
						<a target="_blank" title="无相劫" href="/767/767.html"><h5>无相劫</h5></a>
					</div>  
				
					<div class="col-xs-2 movie-recommened">
						<a style="position:relative;display:block;" target="_blank" title="微笑人生" href="/766/766.html">
							<img width="100%" src="static/picture/p2396323517.jpg" />
							<div class="item-hover"></div>
						</a>
						<a target="_blank" title="微笑人生" href="/766/766.html"><h5>微笑人生</h5></a>
					</div>  
				
					<div class="col-xs-2 movie-recommened">
						<a style="position:relative;display:block;" target="_blank" title="少林杀手：血钱" href="/768/768.html">
							<img width="100%" src="static/picture/p1997228806.jpg" />
							<div class="item-hover"></div>
						</a>
						<a target="_blank" title="少林杀手：血钱" href="/768/768.html"><h5>少林杀手：血钱</h5></a>
					</div>   -->
				
				</div>
            </div>
        </div>
        <div class="col-xs-3 sidebar" style="padding-left:5px;width:230px;">
            <h3>本周精选</h3>
            <div class="list-group">
				@foreach($paihang2 as $row)
            <a title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}" class="list-group-item"><span class="square-item-blue">{{$row->id}}</span>{{$row->name}}</a>
            @endforeach
				<!-- <a target="_blank" href="/760/760.html" class="list-group-item"><span class="square-item-red">1</span>待绽蔷薇</a>
			
				<a target="_blank" href="/761/761.html" class="list-group-item"><span class="square-item-red">2</span>啊，男孩</a>
			
				<a target="_blank" href="/783/783.html" class="list-group-item"><span class="square-item-red">3</span>证人</a>
			
				<a target="_blank" href="/781/781.html" class="list-group-item"><span class="square-item-blue">4</span>功夫小蝇</a>
			
				<a target="_blank" href="/777/777.html" class="list-group-item"><span class="square-item-blue">5</span>第一滴血</a>
			
				<a target="_blank" href="/762/762.html" class="list-group-item"><span class="square-item-blue">6</span>海扁王</a>
			
				<a target="_blank" href="/750/750.html" class="list-group-item"><span class="square-item-blue">7</span>饭局也疯狂</a>
			
				<a target="_blank" href="/586/586.html" class="list-group-item"><span class="square-item-blue">8</span>坑王驾到 第二季</a>
			
				<a target="_blank" href="/784/784.html" class="list-group-item"><span class="square-item-blue">9</span>这个杀手不太冷</a>
			
				<a target="_blank" href="/778/778.html" class="list-group-item"><span class="square-item-blue">10</span>线人</a> -->
			
			</div>
		</div>
    </div>
</div>
</div>
<script src="../home/show/js/jquery.min.js"></script>
<script src="../home/show/js/bootstrap.min.js"></script>
    <script src="../home/show/js/main.js"></script>
<script src="./home/show/js/jquery.min.js"></script>
<script src="http://sdk-release.qnsdk.com/qiniuplayer-0.3.9.min.js"></script>
<script type="text/javascript">
		// var src = $("#demo-video").src();
		// alert(src);
	    var options = {
        controls: true,
        url: 'http://og9dz2jqu.cvoda.com/Zmlyc3R2b2RiOm9jZWFucy0xLm1wNA==_q00000001.m3u8',
        type: 'hls',
        preload: true,
        autoplay: false // 如为 true，则视频将会自动播放
    };
    var player = new QiniuPlayer('demo-video', options);
</script>
<script type="text/javascript">
	var player = new QiniuPlayer('element-id', options, callback);
</script>
@endsection