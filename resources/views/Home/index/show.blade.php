@extends('Home.HomePublic.public')
@section('content')
<div class="container">
<div class="container-fulid" style="background:#FFF;padding:15px;">
    <ol class="breadcrumb">
        <li><a href="/">首页</a></li>
        <li><a target="_blank" href="">{{$data->type}}</a></li>
		<li class="active">{{$data->name}}</li>
    </ol>
<div class="row">
    <div class="col-xs-9 movie-info padding-right-5" style="width:780px;">
        <h1>{{$data->name}}  <span class="year">({{$data->year}})</span></h1>
        <div class="row">
            <div class="col-xs-4 padding-right-0">
                <img class="img-thumbnail" width="100%" src="../uploads/video/{{$data->pic}}">                
				<!-- <em>最后更新：2018-11-17</em> -->
				<div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a title="分享到QQ空间" href="#" class="bds_qzone" data-cmd="qzone"></a><a title="分享到新浪微博" href="#" class="bds_tsina" data-cmd="tsina"></a><a title="分享到腾讯微博" href="#" class="bds_tqq" data-cmd="tqq"></a><a title="分享到人人网" href="#" class="bds_renren" data-cmd="renren"></a><a title="分享到微信" href="#" class="bds_weixin" data-cmd="weixin"></a></div>
				<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdPic":"","bdStyle":"0","bdSize":"16"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/../home/show/api/js/share.js?v=1.js?cdnversion='+~(-new Date()/36e5)];</script>
            </div>
            <div class="col-xs-8">
                <table class="table table-striped table-condensed table-bordered" style="margin-bottom:10px;font-size:13px;">
				<tbody>
					<tr>
						<td class="span2"><span class="info-label">导演</span></td>
						<td>{{$data->director}}</td>
					</tr>
					<tr>
						<td class="span2"><span class="info-label">主演</span></td>
						<td id="casts" style="position:relative;">{{$data->to_star}}</td>
					</tr>
					<tr>
						<td class="span2"><span class="info-label">类型</span></td>
						<td>{{$data->type}}</td>
					</tr>
					<tr>
						<td class="span2"><span class="info-label">地区</span></td>
						<td>{{$data->region}}</td>
					</tr>
					<tr>
						<td class="span2"><span class="info-label">语言</span></td>
						<td>{{$data->language}}</td>
					</tr>
					<tr>
    					<td class="span2"><span class="info-label">年份</span></td>
    					<td>{{$data->year}}</td>
					</tr>
					<!-- <tr>
    					<td class="span2"><span class="info-label">清晰度</span></td>
    					<td><a target="tags" href="/whole/0_______0_id__1.html"></a> </td>
					</tr> -->
					<tr>
    					<td class="span2"><span class="info-label">状态</span></td>
    					<td>{{$data->status==0?'完结':'未完结'}}</td>
					</tr>
					<!-- <tr>
					<td class="span2"><span class="info-label">人气</span></td>
					<td>日:2 / 周:3 / 月:23 / 总:29</td>
					</tr> -->
				</tbody>
				</table>
				<button onclick="fav({{$data->id}});" type="button" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-star-empty"></i>我要追剧</button>
				<a class="btn btn-success btn-sm" onclick="bo({{$data->id}})" id="bofang" href="javascript::void(0)"><span class="glyphicon glyphicon-check"></span> 在线播放</a>
            </div>
        </div>
		<script>
		if($('#casts').children('a').length > 7 ){
			$('#casts').children('a').each(function(i,n){
				var obj = $(n);
				if(i > 5) obj.hide();
			});
			$(".casts-swith").show();
		}
		</script>
		<div class="row" style="margin-top:5px;">
			<div class="col-xs-12"><h2>剧情介绍</h2></div>
			<div class="col-xs-12 movie-introduce"><p>{{$data->jieshao}}</p></div>
		</div>
		<!-- <div style="padding:0;margin:15px 0 0;width:760px;background:#EEE;max-height:90px;">
             760x90 AD
		</div> -->
        <!-- <div class="row">
			<div class="col-xs-12"><h2>资源列表</h2></div>
				<div class="col-xs-12" style="margin-top:10px;">
					<ul id="tvTabs" class="nav nav-tabs" role="tablist">
					
						<li role="presentation" class="active"><a aria-expanded="true" href="#zu1" id="zu1-tab" role="tab" data-toggle="tab" aria-controls="zu1">视频云</a></li>
					
					</ul>
					<div id="tvTabContent" class="tab-content">
					
						<div role="tabpanel" class="tab-pane active in" id="zu1" aria-labelledby="zu1-tab">
							<div class="row tv-js-list" style="margin-top:15px;">
							    
								<div class="col-xs-1 play-8"><a target="_blank" href="/play/781~0~0.html" class="tv-js-btn">HD超清</a></div>
								
							</div>
						</div>
					
					
					</div>
				</div>
		</div> -->
        <div class="row">
            <div class="col-xs-12"><h2>影片评论</h2></div>
				<div class="col-xs-12">
					<div class="comment" style="margin-top:10px;">
						<hr class="dline" />
                        <div id="ctcms_pl">加载中...</div><script src="../home/show/js/pl.js"></script><script>var pl_did=781,ctcms_pllink = "/pl/index",ctcms_pladdlink = "/pl/add";setTimeout(function(){get_pl(1);},1000);</script>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xs-3 sidebar" style="padding-left:15px;width:220px;padding-right:5px;">
			<h3>最近更新</h3>
			<div class="list-group">
				@foreach($paihang as $row)
            <a title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}" class="list-group-item"><span class="square-item-blue">{{$row->id}}</span>{{$row->name}}</a>
            @endforeach
				<!-- <a target="_blank" href="/783/783.html" class="list-group-item"><span class="square-item-red">1</span>证人</a>
			
				<a target="_blank" href="/784/784.html" class="list-group-item"><span class="square-item-red">2</span>这个杀手不太冷</a>
			
				<a target="_blank" href="/781/781.html" class="list-group-item"><span class="square-item-red">3</span>功夫小蝇</a>
			
				<a target="_blank" href="/780/780.html" class="list-group-item"><span class="square-item-blue">4</span>断箭</a>
			
				<a target="_blank" href="/779/779.html" class="list-group-item"><span class="square-item-blue">5</span>敢死队</a>
			
				<a target="_blank" href="/778/778.html" class="list-group-item"><span class="square-item-blue">6</span>线人</a>
			
				<a target="_blank" href="/777/777.html" class="list-group-item"><span class="square-item-blue">7</span>第一滴血</a>
			
				<a target="_blank" href="/776/776.html" class="list-group-item"><span class="square-item-blue">8</span>第一滴血2</a>
			
				<a target="_blank" href="/775/775.html" class="list-group-item"><span class="square-item-blue">9</span>第一滴血3</a>
			
				<a target="_blank" href="/774/774.html" class="list-group-item"><span class="square-item-blue">10</span>第一滴血4</a>
			 -->
			</div>
			<h3>本周热播</h3>
			<div class="list-group">
				@foreach($paihang2 as $row)
            <a title="{{$row->name}}" target="_blank" href="/show/{{$row->id}}" class="list-group-item"><span class="square-item-blue">{{$row->id}}</span>{{$row->name}}</a>
            @endforeach
			
				<!-- <a target="_blank" href="/760/760.html" class="list-group-item"><span class="square-item-red">1</span>待绽蔷薇</a>
			
				<a target="_blank" href="/783/783.html" class="list-group-item"><span class="square-item-red">2</span>证人</a>
			
				<a target="_blank" href="/761/761.html" class="list-group-item"><span class="square-item-red">3</span>啊，男孩</a>
			
				<a target="_blank" href="/781/781.html" class="list-group-item"><span class="square-item-blue">4</span>功夫小蝇</a>
			
				<a target="_blank" href="/777/777.html" class="list-group-item"><span class="square-item-blue">5</span>第一滴血</a>
			
				<a target="_blank" href="/762/762.html" class="list-group-item"><span class="square-item-blue">6</span>海扁王</a>
			
				<a target="_blank" href="/750/750.html" class="list-group-item"><span class="square-item-blue">7</span>饭局也疯狂</a>
			
				<a target="_blank" href="/586/586.html" class="list-group-item"><span class="square-item-blue">8</span>坑王驾到 第二季</a>
			
				<a target="_blank" href="/784/784.html" class="list-group-item"><span class="square-item-blue">9</span>这个杀手不太冷</a>
			
				<a target="_blank" href="/778/778.html" class="list-group-item"><span class="square-item-blue">10</span>线人</a>
			 -->
			</div>
		</div>
	</div>
</div>
</div>
<script src="../home/show/js/jquery.min.js"></script>
    <script src="../home/show/js/bootstrap.min.js"></script>
    <script src="../home/show/js/main.js"></script>
<script src="./home/show/js/jquery.min.js"></script>
<script type="text/javascript">
	function fav(id){
		// alert(id);
		// alert($);
		$.get('/cang',{id:id},function(data){
			// alert(data);
			if(data==1){
				alert('收藏成功');
			}else if(data==2){
				alert('收藏失败');
			}else if(data==3){
				alert('您已经收藏过了 请不要重复收藏');
			}else if(data==4){
				alert('请先登录 再收藏');
				location.href='/login/create';
			}
		});
	}


	function bo(id){
		// alert(id);
		var a=id;
		// p=$("#bofang");
		$.get('/vippr',{id:id},function(data){
			// alert(data);
			if(data==4){
				alert('请先登录,再观看');
			}else if(data=='vip'){
				alert('您是vip会员,可无限观看');
				location.href='/bofang/'+a;
			}else if(data==0){
				alert('您已经没有观看次数,请充值');
			}else{
				alert('您还有'+data+'次观看机会,无限观看请移步充值会员');
				location.href='/bofang/'+a;
			}
		});
		
	}
</script>
@endsection