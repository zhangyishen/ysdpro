
<link rel="stylesheet" type="text/css" href="../home/zhao/amazeui.css">
<style type="text/css">
      .cur{
        border:1px solid red;
      }
       .curs{
        border:1px solid green;
      }
</style>
<script type="text/javascript" src="../home/show/js/jquery.min.js"></script>
<div class="am-g am-g-fixed am-padding-top">

<div class="am-g">

  <div class="am-u-md-8 am-u-sm-centered">
    @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
    <form class="am-form" action="/zhao/{{$id}}" method="post" data-am-validator="" novalidate="novalidate">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <fieldset>

    <legend>找回密码</legend>

    <div class="am-form-group">

      <label for="doc-vld-name-2">密码：</label>

      <input type="password" name="pass" id="doc-vld-name-2" placeholder="输入密码" required>

    </div>

    <div class="am-form-group">

      <label for="doc-vld-email-2">重复密码：</label>

      <input type="password" name="repass" id="doc-vld-email-2" placeholder="重复密码" required>

    </div>
    <button type="submit" class="am-btn am-btn-primary am-btn-block">确定改密</button>

  </fieldset>

</form>

  </div>

</div>

</div>
<script type="text/javascript">
    $("#disx").click(function(){
    $(this).fadeOut("slow");
  });
</script>
