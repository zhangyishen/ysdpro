@extends("Home.HomePublic.public")
@section('content')
<link rel="stylesheet" type="text/css" href="../home/zhao/amazeui.css">
<script type="text/javascript" src="../home/show/js/jquery.min.js"></script>
<div class="am-g am-g-fixed am-padding-top">

<div class="am-g">

  <div class="am-u-md-8 am-u-sm-centered">
    @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
    <form class="am-form" action="/zhao" method="post" data-am-validator="" novalidate="novalidate">
    {{csrf_field()}}
    <fieldset>

    <legend>找回密码</legend>

    <div class="am-form-group">

      <label for="doc-vld-name-2">账号：</label>

      <input type="text" name="user" id="doc-vld-name-2" placeholder="输入登陆账号" required>

    </div>

    <div class="am-form-group">

      <label for="doc-vld-email-2">邮箱：</label>

      <input type="email" name="email" id="doc-vld-email-2" placeholder="输入邮箱" required pattern="^((([a-zA-Z]|\d|[!#\$%&amp;'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&amp;'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$">

    </div>

    <div class="am-form-group">

      <label for="doc-vld-email-2">验证码：</label>

      <input type="text" name="code" minlength="4" placeholder="输入下面的验证码" required="">	  

      <span class="am-text-left"><img src="/code" onclick="this.src=this.src+'?a=1'"></span>

    </div>

    <button type="submit" class="am-btn am-btn-primary am-btn-block">确定提交</button>

  </fieldset>

</form>

  </div>

</div>

</div>
<script type="text/javascript">
    $("#disx").click(function(){
    $(this).fadeOut("slow");
  });
</script>
@endsection