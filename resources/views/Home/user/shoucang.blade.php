@extends("Home.HomePublic.userpublic")
@section('content')
<div class="am-u-sm-6 am-u-md-8 am-u-lg-9">

            <fieldset>

            <legend>我的收藏视频<a title="充值记录" class="am-fr am-text-right" href="/quandel/{{session('user')->id}}" style="font-size:16px;" onclick="return confirm('删除后不能恢复，确定吗？');">删除全部收藏</a></legend>

      <div class="am-g am-g-fixed">

                <div class="am-u-sm-12">

                    <ul class="am-nav am-nav-pills">

            <li class="am-active"><a href="">我的收藏</a></li>

            

            

                    </ul>



    <ul data-am-widget="gallery" class="am-gallery am-avg-sm-2 am-avg-md-5 am-gallery-default am-thumbnails am-margin-top am-no-layout">


                      
                      @foreach($ss as $row)

                        <li>

                            <div class="am-gallery-item">

                                <a title="{{$row->vname}}" href="/show/{{$row->vid}}" target="_blank">

                                    <img alt="{{$row->vname}}" style="height:190px;" src="../uploads/video/{{$row->pic}}">

                              <div style="z-index:5;position:relative;margin-top:-20px;background: rgba(0,0,0,0.5);color:#eee;font-size:12px;padding-left:5px;">{{$row->vsta==0?'完结':'未完结'}}</div>

                                    <h3 class="am-gallery-title">{{$row->vname}}</h3>

                                    </a><div class="am-gallery-desc"><a title="{{$row->vname}}" href="/show/{{$row->vid}}" target="_blank">{{date('Y-m-d H:i:s',$row->atime)}}
                              
                            </a>
                            <a title="删除" class="btn btn-danger btn-sm" href="/delcang/{{$row->sid}}">x</a>

                          </div>

                                

                            </div>

                        </li>
                        @endforeach

    

      </ul>

                </div>

      </div>

            </fieldset>

      <ul class="am-pagination am-pagination-centered am-hide">

        <li class="am-disabled"><a href="http://demo.ctcms.cn/user/fav/index/1">«</a></li>

        <li class="am-active"><a href="http://demo.ctcms.cn/user/fav/index/1">1</a></li>

        <li class="am-disabled"><a href="http://demo.ctcms.cn/user/fav/index/1">»</a></li>

      </ul>

        </div>

    </div>

</div>

@endsection