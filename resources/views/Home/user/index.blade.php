@extends('Home.HomePublic.userpublic')
@section('content')
<div class="am-u-sm-6 am-u-md-8 am-u-lg-9" style="position:relative;">
        <img style="height:120px;width:120px;" src="../uploads/{{session('user')->top_pic}}">
        <fieldset>
            <legend>会员中心</legend>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">账号</div>
                <div class="am-u-sm-10">{{session('user')->user}}</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">昵称</div>
                <div class="am-u-sm-10">{{session('user')->name}}</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">邮箱</div>
                <div class="am-u-sm-10">{{session('user')->email}}</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">手机</div>
                <div class="am-u-sm-10">{{session('user')->phone}}</div>
              </div>
              <!-- <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">扣扣</div>
                <div class="am-u-sm-10"></div>
              </div> -->
              <!-- <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">性别</div>
                <div class="am-u-sm-10">保密</div>
              </div> -->
            @if(session('user')->vipstatus==0)
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">级别</div>
                <div class="am-u-sm-10">普通用户</div>
              </div>
              @else
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">级别</div>
                <div class="am-u-sm-10">{{session('user')->vipstatus==1?'会员':''}}</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">充值日期</div>
                <div class="am-u-sm-10">{{date("Y-m-d",session('user')->vipstart_time)}}</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">到期日期</div>
                <div class="am-u-sm-10">{{date("Y-m-d",session('user')->vipstop_time)}}</div>
              </div>
              @endif
              <!-- <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">登录次数</div>
                <div class="am-u-sm-10">4次</div>
              </div> -->
              <!-- <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">登录IP</div>
                <div class="am-u-sm-10">115.124.31.85</div>
              </div>
              <div class="am-form-group am-padding-sm">
                <div class="am-u-sm-2">登录时间</div>
                <div class="am-u-sm-10">2018-12-24 18:13:07</div>
              </div> -->
            </fieldset>
        </div>
@endsection