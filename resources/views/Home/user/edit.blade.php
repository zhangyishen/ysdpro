@extends("Home.HomePublic.userpublic")
@section('content')
<div class="am-u-sm-6 am-u-md-8 am-u-lg-9">

            <form class="am-form am-form-horizontal" action="/user/{{session('user')->id}}" method="post" data-am-validator="" novalidate="novalidate">
			{{csrf_field()}}
			{{method_field("PUT")}}
            <fieldset>

	          <ul class="am-nav am-nav-tabs am-margin-bottom-lg" style="margin-top:0;">

  	            <li class="am-active"><a href="/edit/{{session('user')->id}}">修改资料</a></li>

  	            <!-- <li><a href="http://demo.ctcms.cn/user/edit/logo">修改头像</a></li>

  	            <li><a href="http://demo.ctcms.cn/user/edit/pass">修改密码</a></li> -->

	          </ul>

              <div class="am-form-group">

                <label for="doc-ipt-1" class="am-fl am-form-label">账&nbsp;&nbsp;&nbsp;&nbsp;号</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-ipt-1" value="{{$data->user}}" type="text" name="user">

                </div>

              </div>

              <div class="am-form-group">

                <label for="doc-ipt-2" class="am-fl am-form-label">昵&nbsp;&nbsp;&nbsp;&nbsp;称</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-ipt-2" placeholder="输入昵称" type="text" name="name" value="{{$data->name}}">

                </div>

              </div>

              <div class="am-form-group">

                <label for="doc-ipt-3" class="am-fl am-form-label">邮&nbsp;&nbsp;&nbsp;&nbsp;箱</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-ipt-3" value="{{$data->email}}" type="email" name="email" placeholder="输入联系邮箱" pattern="^((([a-zA-Z]|\d|[!#\$%&amp;'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-zA-Z]|\d|[!#\$%&amp;'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-zA-Z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-zA-Z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$">

                </div>

              </div>

              <div class="am-form-group">

                <label for="doc-tel-3" class="am-fl am-form-label">手&nbsp;&nbsp;&nbsp;&nbsp;机</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-tel-3" type="text" name="phone" placeholder="输入联系手机" value="{{$data->phone}}" pattern="^1[3|4|5|7|8][0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$">

                </div>

              </div>

              <!-- <div class="am-form-group">

                <label for="doc-tel-4" class="am-fl am-form-label">扣&nbsp;&nbsp;&nbsp;&nbsp;扣</label>

                <div class="am-u-sm-10">

                  <input class="am-form-field" id="doc-tel-4" type="text" name="qq" placeholder="输入联系qq" pattern="^[0-9]\d{4,20}$">

                </div>

              </div> -->

              <!-- <div class="am-form-group">

                <label for="doc-tel-4" class="am-fl am-form-label">性&nbsp;&nbsp;&nbsp;&nbsp;别</label>

                <div class="am-u-sm-10">

                     <label id="sex-1" sex="1" class="sex">男</label>

                     <label id="sex-2" sex="2" class="sex">女</label>

                     <label id="sex-0" sex="0" class="sex on">保密</label>

					 <input id="sex" type="hidden" name="sex" value="0">

                </div>

              </div> -->

              <div class="am-form-group">

                <div class="am-u-sm-10 am-u-sm-offset-1">

                  <button type="submit" class="am-btn am-btn-primary">提交修改</button>

                </div>

              </div>

            </fieldset>

            </form>

        </div>
@endsection