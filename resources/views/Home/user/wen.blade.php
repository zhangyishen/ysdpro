@extends("Home.HomePublic.userpublic")
@section('content')

        <div class="am-u-sm-6 am-u-md-8 am-u-lg-9">

            <fieldset>

	          <ul class="am-nav am-nav-tabs am-margin-bottom-lg" style="margin-top:0;">

  	            <li class="am-active"><a href="">我发布的文章</a></li>

  	            <!-- <li><a href="http://demo.ctcms.cn/user/comm/fav">我收藏的文章</a></li>

  	            <li><a href="http://demo.ctcms.cn/user/comm/zan">我赞过的文章</a></li> -->

	          </ul>

			<div class="am-g am-g-fixed">

                <div class="am-u-sm-12">

                    <ul class="am-nav am-nav-pills">

					  <li class="am-active"><a href="http://demo.ctcms.cn/user/comm">全部</a></li>

					  

                      <!-- <li><a href="http://demo.ctcms.cn/user/comm/index/1">推荐电影</a></li>

					  

                      <li><a href="http://demo.ctcms.cn/user/comm/index/2">影迷交流</a></li> -->

					  

                    </ul>



			  <table class="am-table">

    			  <thead>

        			  <tr>

            			  <th>文章标题</th>

            			  <th>浏览</th>

            			  <th>评论</th>

            			  <th>被赞</th>

            			  <th>发布时间</th>

        			  </tr>

    			  </thead>

    			  <tbody>




					@foreach($data as $row)
       			      <tr>

           			      <td><a href="/jiaoliu/{{$row->id}}" target="_blank">{{$row->title}}</a></td>

            			  <td>{{$row->liulan}}</td>

            			  <td>{{$row->num}}</td>

            			  <td>{{$row->zan}}</td>

            			  <td>{{date("Y-m-d H:i:s",$row->addtime)}}</td>

        			  </tr>
					@endforeach


    			  </tbody>

			  </table>

            </div></div></fieldset>

                </div>

			</div>

            

			<ul class="am-pagination am-pagination-centered am-hide">

  			<li class="am-disabled"><a href="http://demo.ctcms.cn/user/comm/index/1">«</a></li>

  			<li class="am-active"><a href="http://demo.ctcms.cn/user/comm/index/1">1</a></li>

  			<li class="am-disabled"><a href="http://demo.ctcms.cn/user/comm/index/1">»</a></li>

			</ul>

        </div>
@endsection