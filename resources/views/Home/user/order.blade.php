@extends('Home.HomePublic.userpublic')
@section('content')

<div class="am-u-sm-6 am-u-md-8 am-u-lg-9">

            <fieldset>

	          <ul class="am-nav am-nav-tabs am-margin-bottom-lg" style="margin-top:0;">

  	            <li class="am-active"><a href="http://demo.ctcms.cn/user/pay/lists">消费记录</a></li>

	          </ul>



			  <table class="am-table">

    			  <thead>

        			  <tr>

            			  <th>订单号</th>

            			  <th>类型</th>

            			  <th>金额</th>

            			  <th>方式</th>

            			  <th>状态</th>

            			  <th>充值时间</th>

        			  </tr>

    			  </thead>

    			  <tbody>



					
					@foreach($data as $row)

       			      <tr>

           			      <td>{{$row->order_id}}</td>

            			  <td>{{$row->setmeal}}</td>

            			  <td>{{$row->price}}元</td>

            			  <td>在线支付</td>

            			  <td>@if($row->status==0)
	            			  	<font color="yellow">支付失败</font>
	            			  @elseif($row->status==1)
	            			  	<font color="green">支付成功</font>
	            			  @endif
            			  </td>

            			  <td>{{date('Y-m-d H:i:s',$row->addtime)}}</td>

        			  </tr>
					@endforeach


    			  </tbody>

			  </table>

            </fieldset>

                </div>

			</div>

            

			<ul class="am-pagination am-pagination-centered am-hide">

  			<li class="am-disabled"><a href="http://demo.ctcms.cn/user/pay/lists/1">«</a></li>

  			<li class="am-active"><a href="http://demo.ctcms.cn/user/pay/lists/1">1</a></li>

  			<li class="am-disabled"><a href="http://demo.ctcms.cn/user/pay/lists/1">»</a></li>

			</ul>

        </div>				
@endsection