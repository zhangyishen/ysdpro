@extends("Admin.AdminPublic.public")
@section("content")
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>分类管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">分类管理</li>
                            <li class="breadcrumb-item active">年份列表</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="header">
                            <h2>年份列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                @if(session('success'))
                                    <div class="alert alert-info" id="disx">{{session('success')}}</div>
                                @endif

                         
                                @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                                @endif
                                 <!-- <form id="navbar-search" class="navbar-form search-form" action="/aduser" method="get">
                                    <input value="{{$request['keywords'] or ''}}" class="form-control" placeholder="搜索..." type="text" name="keywords">
                                    <button type="submit" class="btn btn-default"><i class="icon-magnifier"></i></button>
                                </form> -->
                                <table class="table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>名字</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td><span>{{$row->name}}</span></td>
                                            <td><span class="text-info">{{$row->status==0?'启用':'禁用'}}</span></td>
                                            <td>
                                                <a href="/adyear/{{$row->id}}/edit" class="btn btn-info" style="float:left">修改</a>
                                                <form action="/adyear/{{$row->id}}" method="post">
                                                    {{csrf_field()}}
                                                    {{method_field("DELETE")}}
                                                    <button class="btn btn-danger" style="float:left">删除</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection