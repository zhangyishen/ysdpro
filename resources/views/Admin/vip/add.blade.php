@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>会员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">会员管理</li>
                            <li class="breadcrumb-item active">添加套餐</li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="row clearfix">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2> 添加套餐 </h2>
                        </div>
                        <div class="body">
                        	<form action="/advip" method="post">
                        		{{csrf_field()}}
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">套餐名</span>
                                </div>
                                <input type="text" class="form-control" placeholder="请填写套餐名" aria-label="请填写套餐名" aria-describedby="basic-addon1" name="name" required>
                            </div>
                                
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="请填写套餐使用时长(填写数字即可)" aria-label="请填写套餐使用时长(填写数字即可)" aria-describedby="basic-addon2" required name="time">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">天</span>
                                </div>
                            </div>
                            
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">套餐价格(填写数字)</span>
                                </div>
                                <input type="text" class="form-control" aria-label="填写价格" required name="price">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">状态</span>
                                </div>
                                <!-- <input type="text" class="form-control" placeholder="请填写套餐名" aria-label="请填写套餐名" aria-describedby="basic-addon1" required> -->
                                <select name="status" class="form-control" aria-describedby="basic-addon1" required>
                                	<option selected value="0">启用</option>
                                	<option value="1">禁用</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">添加</button>
                            <button type="reset" class="btn btn-info">重置</button>
                            </form>
                            
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection