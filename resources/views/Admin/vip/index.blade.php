@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>套餐列表</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">会员管理</li>
                            <li class="breadcrumb-item active">套餐列表</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

				@foreach($data as $row)
                <div class="col-lg-4 col-md-12" style="float: left"> 
                    <div class="card" >
                        <div class="carousel slide twitter w_feed" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <i class="fa-2x  icon-basket-loaded"></i>
                                    <p>ID : {{$row->id}}</p>
                                    <h4>套餐 : <span>{{$row->name}}</span><br>状态 : {{$row->status==0?'启用':'禁用'}}</h4>
                                    <div class="m-t-20"><i>套餐时长 : {{$row->time}}天</i></div>
                                    <a href="/advip/{{$row->id}}/edit"><button class="btn btn-success btn-sm" style="float: left">修改</button></a>

                                    <form action="/advip/{{$row->id}}" method="post">
                                    	{{csrf_field()}}
                                    	{{method_field('DELETE')}}
                                    	<button type="submit" class="btn btn-danger btn-sm" style="float: left">删除</button>
                                    	
                                    </form>
                                </div>
                                <div class="carousel-item">
                                    <i class="fa-2x  icon-basket-loaded"></i>
                                    <p>ID : {{$row->id}}</p>
                                    <h4>套餐 : <span>{{$row->name}}</span><br>状态 : {{$row->status==0?'启用':'禁用'}}</h4>
                                    <div class="m-t-20"><i>套餐时长 : {{$row->time}}天</i></div>
                                    <a href="/advip/{{$row->id}}/edit"><button class="btn btn-success btn-sm" style="float: left">修改</button></a>

                                    <form action="/advip/{{$row->id}}" method="post">
                                    	{{csrf_field()}}
                                    	{{method_field('DELETE')}}
                                    	<button type="submit" class="btn btn-danger btn-sm" style="float: left">删除</button>
                                    	
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				@endforeach


            </div>
        </div>
    </div>
@endsection