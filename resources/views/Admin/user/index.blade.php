@extends("Admin.AdminPublic.public")
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>用户管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">用户管理</li>
                            <li class="breadcrumb-item active">用户列表</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>用户列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <form id="navbar-search" class="navbar-form search-form" action="/aduser" method="get">
                                    <input value="{{$request['keywords'] or ''}}" class="form-control" placeholder="搜索..." type="text" name="keywords">
                                    <button type="submit" class="btn btn-default"><i class="icon-magnifier"></i></button>
                                </form>
                                <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>头像</th>
                                            <th>账号</th>
                                            <th>昵称</th>
                                            <th>邮箱</th>
                                            <th>是否会员</th>
                                            <th>创建时间</th>
                                            <th>会员充值时间</th>
                                            <th>会员到期时间</th>
                                            <th>状态</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                        @foreach($data as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td>
                                                <img src="./uploads/{{$row->top_pic}}" class="rounded-circle avatar" alt="">
                                            </td>
                                            <td>{{$row->user}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->vipstatus==1?'会员':'普通用户'}}</td>
                                            <td>{{date("Y年m月d日",$row->useradd_time)}}</td>
                                            <td>{{$row->vipstatus == 1?date("Y年m月d日",$row->vipstart_time):'无'}}</td>
                                            <td>{{$row->vipstatus == 1?date("Y年m月d日",$row->vipstop_time):'无'}}</td>
                                            <td>{{$row->status}}</td>
                                            <!-- <td>
                                                <span class="phone"><i class="fa fa-phone m-r-10"></i>264-625-2583</span>
                                            </td>                                   
                                            <td>
                                                <address><i class="fa fa-map-marker"></i>123 6th St. Melbourne, FL 32904</address>
                                            </td> -->
                                            <!-- <td>                                            
                                                <button type="button" class="btn btn-info" title="Edit"><i class="fa fa-edit"></i></button>
                                                <button type="button" data-type="confirm" class="btn btn-danger js-sweetalert" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td> -->
                                            <td>
                                            <form action="/aduser/{{$row->id}}" method="post">
                                                {{csrf_field()}}
                                                {{ method_field('DELETE')}}
                                                <button id="delete" type="submit" style="float: left" onclick="if(confirm('确定删除此条数据?')==false)return false" class="btn btn-danger">删除</button>
                                            </form>
                                            <a href="/aduser/{{$row->id}}/edit"><button  style="float: left" class="btn btn-info">修改</button></a>
                                            <a href="/aduser/{{$row->id}}"><button style="float:left" class="btn btn-success">信息</button></a>
                                        </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div  class="pagination">{{$data->appends($request)->render()}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection