@extends("Admin.AdminPublic.public")
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>用户管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">用户管理</li>
                            <li class="breadcrumb-item active">添加用户</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>添加用户</h2>
                        </div>
                        @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                        <div class="body">
                            <form id="basic-form" method="post" novalidate action="/aduser" enctype="multipart/form-data">
                            @if (count($errors)>0)
                            <div class="alert alert-danger" id="disx">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                             @endif
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>昵称</label> <span style="float:right"><font color="red">必填*</font></span>
                                    <input type="text" class="form-control" name="name">
                                </div>
                                <div class="form-group">
                                    <label>账号</label><span style="float:right"><font color="red">必填*</font></span>
                                    <input type="text" class="form-control" name="user" required>
                                </div>
                                <div class="form-group">
                                    <label>密码</label><span style="float:right"><font color="red">必填*</font></span>
                                    <input type="password" class="form-control" name="pass" required>
                                </div>
                                <!-- <div class="form-group">
                                    <label>重复密码</label>
                                    <input type="repassword" class="form-control" name="repass" required>
                                </div> -->
                                <div class="form-group">
                                    <label>手机号</label><span style="float:right"><font color="red">必填*</font></span>
                                    <input type="phone" class="form-control" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label>邮箱</label><span style="float:right"><font color="red">必填*</font></span>
                                    <input type="email" class="form-control" name="email" required>
                                </div>
                                
                                <div class="form-group">
                                    <label>签名</label>
                                    <textarea class="form-control" rows="5" cols="30" required="" name="content" value="他/她很懒.....什么也没留下">他/她很懒.....什么也没留下</textarea>
                                </div>

                                <div class="form-group">
                                    <label>是否会员</label>
                                    <br />
                                    <label class="fancy-radio">
                                        <input type="radio" name="vipstatus"  value="0" required data-parsley-errors-container="#error-radio" checked>
                                        <span><i></i>不是</span>
                                    </label>
                                    <label class="fancy-radio">
                                        <input type="radio" name="vipstatus" value="1" >
                                        <span><i></i>是</span>
                                    </label>
                                    <p id="error-radio"></p>
                                </div>
                                
                                <div class="form-group">
                                    <label>用户状态</label>
                                    <br />
                                    <label class="fancy-radio">
                                        <input type="radio" name="status"  value="1" required data-parsley-errors-container="#error-radio" checked>
                                        <span><i></i>禁用</span>
                                    </label>
                                    <label class="fancy-radio">
                                        <input type="radio" name="status" value="0" >
                                        <span><i></i>启用</span>
                                    </label>
                                    <p id="error-radio"></p>
                                </div>

                                <div class="form-group">
                                    <label>头像</label>
                                    <input type="file" class="form-control" name="top_pic" required>
                                </div>
                                <!-- <div class="form-group">
                                    <label>Text Area</label>
                                    <textarea class="form-control" rows="5" cols="30" required></textarea>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label>Checkbox</label>
                                    <br/>
                                    <label class="fancy-checkbox">
                                        <input type="checkbox" name="checkbox" required data-parsley-errors-container="#error-checkbox">
                                        <span>Option 1</span>
                                    </label>
                                    <label class="fancy-checkbox">
                                        <input type="checkbox" name="checkbox">
                                        <span>Option 2</span>
                                    </label>
                                    <label class="fancy-checkbox">
                                        <input type="checkbox" name="checkbox">
                                        <span>Option 3</span>
                                    </label>
                                    <p id="error-checkbox"></p>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label>Radio Button</label>
                                    <br />
                                    <label class="fancy-radio">
                                        <input type="radio" name="gender" value="male" required data-parsley-errors-container="#error-radio">
                                        <span><i></i>Male</span>
                                    </label>
                                    <label class="fancy-radio">
                                        <input type="radio" name="gender" value="female">
                                        <span><i></i>Female</span>
                                    </label>
                                    <p id="error-radio"></p>
                                </div> -->
                               <!--  <div class="form-group">
                                    <label for="food">Multiselect</label>
                                    <br/>
                                    <select id="food" name="food[]" class="multiselect multiselect-custom" multiple="multiple" data-parsley-required data-parsley-trigger-after-failure="change" data-parsley-errors-container="#error-multiselect">
                                        <option value="cheese">Cheese</option>
                                        <option value="tomatoes">Tomatoes</option>
                                        <option value="mozarella">Mozzarella</option>
                                        <option value="mushrooms">Mushrooms</option>
                                        <option value="pepperoni">Pepperoni</option>
                                        <option value="onions">Onions</option>
                                    </select>
                                    <p id="error-multiselect"></p>
                                </div> -->
                                <br>
                                <button type="submit" class="btn btn-primary">添加</button>
                                <button type="reset" class="btn btn-danger">重置</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
@endsection