@extends("Admin.AdminPublic.public")
@section('content')
<div id="main-content" class="profilepage_2 blog-page">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>用户详情</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>
                            <li class="breadcrumb-item">用户管理</li>
                            <li class="breadcrumb-item active">用户详情</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">

                <div class="col-lg-12 col-md-12">
                    <div class="card profile-header">
                        <div class="body">
                            <div class="profile-image"> <img src="../uploads/{{$data->top_pic}} " width="200px" class="rounded-circle" alt=""> </div>
                            <div>
                                <h4 class="m-b-0"><strong>{{$data->name}}</strong></h4>
                                <span>{{$data->email}}</span>
                                <p class="font-14 m-b-0 m-t-10">{{$data->content}}</p>
                            </div>
                            <div class="border-top p-t-20 m-t-20 text-left">
                            	<table width="100%">
                            		<tr>
	                            		<th>昵称</th>
	                            		<th>电话</th>
	                            		<th>邮箱</th>
	                            		<th>状态</th>
	                            		<th>创建时间</th>
	                            		<th>是否vip</th>
	                            		<th>vip开始时间</th>
	                            		<th>vip结束时间</th>
	                            	</tr>

	                            	<tr>
	                            		<td>{{$data->name}}</td>
                                        <td>{{$data->phone}}</td>
                                        <td>{{$data->email}}</td>
                                        <td>{{$data->status == 0?'已激活':'已禁用'}}</td>
                                        <td>{{date("Y年m月d日",$data->useradd_time)}}</td>
										<td>{{$data->vipstatus == 1?'是':'不是'}}</td>
                                        <td>{{$data->vipstart_time == 0?'无':date("Y年m月d日",$data->vipstart_time)}}</td>
                                        <td>{{$data->vipstop_time == 0?'无':date("Y年m月d日",$data->vipstop_time)}}</td>
	                            	</tr>
                            	</table>
                                <!-- <ul class="list-unstyled profile-social">
                                    <li><a class="text-col-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i> Facebook</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-instagram"></i> Instagram</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i> Twitter</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection