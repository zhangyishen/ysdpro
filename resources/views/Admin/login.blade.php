<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link rel="stylesheet" href="../admin/login/c/css/bootstrap.css">
    <link rel="stylesheet" href="../admin/login/c/login/login.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css">
	
    <script src="../admin/login/j/js/jquery.js"></script>
    <script src="../admin/login/j/js/bootstrap.js"></script>
	
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>YSD影视后台登录</title>
    <style type="text/css">
        .ioc_text{
            width:90px;
            height:120px;
            border-radius: 50%;
        }
        .ioc_text .img_text{
            margin-left: 150px;
            text-align: center;
            width:100%;
            height:25px;
            border: 1px solid  transparent;
        }
        .ioc_text img{
            width:100%;
            height:100%;
            border-radius: 50%;
            margin-left: 150px;

        }
    </style>
</head>
<body>

    <div>
        <div class="be-content pren">

            <div class="ioc_text">
                <img src="../admin/login/moren.jpg" alt="">
                <div class="img_text">YSD影视后台登录</div>
            </div>

            <div>
                <form action="/adlogin" method="post">
                    @if(session('error'))
                        <div class="alert alert-danger" id="disx">
                            {{session('error')}}
                        </div>
                    @endif
                    <div class="br-content">

                         <div class="input-group mb-4 bootint">
                             <div class="input-group-prepend">
                                 <span class="input-group-text"><i class="fa fa-user"></i></span>
                             </div>
                             <input type="text" class="form-control" placeholder="账号" name="username">
                         </div>

                         <div class="input-group mb-4 bootint">
                             <div class="input-group-prepend">
                                 <span class="input-group-text"><i class="fa fa-unlock-alt"></i></span>
                             </div>
                             <input type="password" class="form-control" placeholder="密码" name="password">
                         </div>

                        <div style="padding-top: 10px">
                            {{csrf_field()}}
                            <input type="submit" class="btn" value="登录">
                            
                        </div>
                        <div class="be-con">
                           
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
	
</body>
<script type="text/javascript">
    $("#disx").click(function(){
    $(this).fadeOut("slow");
  });
</script>
</html>