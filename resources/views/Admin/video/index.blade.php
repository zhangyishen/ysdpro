@extends("Admin.AdminPublic.public")
@section("content")
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>视频管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">视频管理</li>
                            <li class="breadcrumb-item active">视频列表</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>视频列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <form id="navbar-search" class="navbar-form search-form" action="/advideo" method="get">
                                    <input value="{{$request['keywords'] or ''}}" class="form-control" placeholder="搜索..." type="text" name="keywords">
                                    <button type="submit" class="btn btn-default"><i class="icon-magnifier"></i></button>
                                </form>
                                @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                                @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                    	<th>图片</th>
                                        <th>片名</th>
                                        <th>导演</th>
                                        <th>主演</th>
                                        <th>视频地址</th>
                                        <th>介绍</th>
                                        <th>类别</th>
                                        <th>类型</th>
                                        <th>地区</th>
                                        <th>语言</th>
                                        <th>年份</th>
                                        <th>状态</th>
                                        <th>是否上架</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($video as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                    	<td>
                                                <img src="../uploads/video/{{$row->pic}}" width="50px" class="rounded-circle avatar" alt="">
                                        </td>
                                        <td>{{$row->name}}</td>
                                        <td>{{mb_substr($row->director,0,3).'....'}}</td>
                                        <td>{{mb_substr($row->to_star,0,3).'.....'}}</td>
                                        <td>{{mb_substr($row->video_address,0,30).'...'}}</td>
                                        <td>{{mb_substr($row->jieshao,0,20).'...'}}</td>
                                        <td>{{$row->category}}</td>
                                        <td>{{$row->type}}</td>
                                        <td>{{$row->region}}</td>
                                        <td>{{$row->language}}</td>
                                        <td>{{$row->year}}</td>
                                        <td>{{$row->status==0?'完结':'未完结'}}</td>
                                        <td><label class="badge">{{$row->shangjia==0?'上架':'下架'}}</label></td>
                                        <!-- <td><a href="javascript:void(0);" class="btn btn-success btn-sm">View Order</a></td> -->
                                        <td>
                                        	<a href="/advideo/{{$row->id}}/edit" style="float:left" class="btn btn-success btn-sm">修改</a>
                                            <form action="/advideo/{{$row->id}}" method="post">
                                               {{csrf_field()}}
                                               {{method_field('DELETE')}} 
                                        	   <button class="btn btn-danger btn-sm" >删除</i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div  class="pagination">{{$video->render()}}</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection