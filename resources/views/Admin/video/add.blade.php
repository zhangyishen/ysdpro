@extends("Admin.AdminPublic.public")
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>视频管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">视频管理</li>
                            <li class="breadcrumb-item active">添加视频</li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="row clearfix">
                <div class="col-lg-7 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>添加视频</h2>
                        </div>
                         @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                              @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                        <form action="/advideo" method="post" enctype="multipart/form-data">
                        <div class="body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">片名</button>
                                </div>
                                <input type="text" class="form-control" name="name" placeholder="" aria-label="" aria-describedby="basic-addon1" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">导演</button>
                                </div>
                                <input type="text" class="form-control" name="director" placeholder="" aria-label="" aria-describedby="basic-addon1" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">主演</button>
                                </div>
                                <input type="text" class="form-control" name="to_star" placeholder="" aria-label="" aria-describedby="basic-addon1" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">类别</button>
                                </div>
                                <select class="custom-select" id="type" name="category">
                                    <option selected value="k">--请选择--</option>
                                    @foreach($cate as $row)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group mb-3">
                               <div class="input-group-prepend">
                                   <button class="btn btn-outline-secondary" type="button">类型</button>
                               </div>
                               <select class="custom-select" id="type_2" name="type">
                                   <option selected value="k">--请选择--</option>
                                   <!-- <option value="0">完结</option>
                                   <option value="1">未完结</option> -->
                               </select>
                           </div>
                           <div class="input-group mb-3">
                               <div class="input-group-prepend">
                                   <button class="btn btn-outline-secondary" type="button">地区</button>
                               </div>
                               <select class="custom-select" id="inputGroupSelect03" name="region">
                                   <option selected value="k">--请选择--</option>
                                   @foreach($country as $row)
                                      <option value="{{$row->name}}">{{$row->name}}</option>
                                   @endforeach
                               </select>
                           </div>
                           <div class="input-group mb-3">
                               <div class="input-group-prepend">
                                   <button class="btn btn-outline-secondary" type="button">年份</button>
                               </div>
                               <select class="custom-select" id="inputGroupSelect03" name="year">
                                   <option selected value="k">--请选择--</option>
                                   @foreach($year as $row)
                                      @if($row->status == 0)
                                      <option value="{{$row->name}}">{{$row->name}}</option>
                                      @endif
                                   @endforeach
                               </select>
                           </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">语言</button>
                                </div>
                                <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="language" required>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">视频地址</button>
                                </div>
                                <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="video_address" required>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">视频状态</button>
                                </div>
                                <select class="custom-select" id="inputGroupSelect03" name="status">
                                    <!-- <option selected>--请选择--</option> -->
                                    <option value="0">完结</option>
                                    <option value="1">未完结</option>
                                </select>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">是否上架</button>
                                </div>
                                <select class="custom-select" id="inputGroupSelect03" name="shangjia">
                                    <!-- <option selected>--请选择--</option> -->
                                    <option value="0">上架</option>
                                    <option value="1">下架</option>
                                </select>
                            </div>
  
							               <div class="input-group mb-3">
                                <!-- <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">年份</button>
                                </div> -->
                                <input type="file" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="pic">
                            </div>

							             <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">电影介绍</span>
                                </div>
                                <textarea class="form-control" aria-label="With textarea" name="jieshao"></textarea>
                            </div>
                            <br>
                            {{csrf_field()}}
                           <button type="submit" class="btn btn-success">添加</button>
                           <button type="reset" class="btn btn-danger">重置</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script type="text/javascript">
    $(".a").remove();
    $("#type").change(function(){
        var id = $(this).val();
        // alert(id)
        $(".a").remove();
        $.get("/son_cate",{id:id},function(data){

             data = eval(data);
             var info;
             for (var i = 0; i < data.length; i++) {

                 info += '<option class="a" value='+data[i].name+'>'+data[i].name+'</option>'
             }
            $('#type_2').append(info);
        });
    });
</script>
@endsection