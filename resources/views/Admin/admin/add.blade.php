@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>管理员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">管理员管理</li>
                            <li class="breadcrumb-item active">添加管理员</li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>1.设置管理员用户名密码</h2>
                        </div>
                        @if(session('success'))
                                {{session('success')}}
                         @endif

                         
                            @if(session('error'))
                                     {{session('error')}}
                            @endif
                        <div class="body">
                        	<form action="/adusers" method="post">
                        		@if (count($errors)>0)
	                            <div class="alert alert-danger" id="disx">
	                                <ul>
	                                    @foreach ($errors->all() as $error)
	                                        <li>{{ $error }}</li>
	                                    @endforeach
	                                </ul>
	                            </div>
	                            @endif
                        		{{csrf_field()}}
	                            <div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">用户名</button>
	                                </div>
	                                <input type="text" class="form-control" placeholder="请填写用户名" name="username" aria-label="" aria-describedby="basic-addon1">
	                            </div>
								
								<div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">密码</button>
	                                </div>
	                                <input type="password" class="form-control" placeholder="请填写密码" aria-label="" name="password" aria-describedby="basic-addon1">
	                            </div>
								
								<div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">重复密码</button>
	                                </div>
	                                <input type="repassword" class="form-control" placeholder="确认密码" aria-label="" name="repassword" aria-describedby="basic-addon1">
	                            </div>
								<!-- <h6>2.选择管理员权限</h6>
	                          	<div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <label class="input-group-text" for="inputGroupSelect01">权限</label>
	                                </div>
	                                <select class="custom-select" id="inputGroupSelect01">
	                                    <option selected>--请选择--</option>
	                                    <option value="1">One</option>
	                                    <option value="2">Two</option>
	                                    <option value="3">Three</option>
	                                </select>
	                            </div> -->

	                            <button type="submit" class="btn btn-primary">添加</button>
	                            <button type="reset" class="btn btn-danger">重置</button>
							</form>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection