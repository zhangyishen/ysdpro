@extends('Admin.AdminPublic.public')
@section('content');
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>管理员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">管理员管理</li>
                            <li class="breadcrumb-item active">分配角色</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>当前用户:{{$user->username}} 的角色信息</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            	<form action="/saverole" method="post">
                                <table class="table table-hover m-b-0 c_list">
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>角色</th>                                    
                                            <!-- <th>Phone</th>                                     -->
                                            <!-- <th>Address</th> -->
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>

										
                                        <tbody>
										@foreach($role as $v)
	                                        <tr>
	                                            <td style="width: 50px;">
	                                                <label class="fancy-checkbox">
	                                                    <input class="checkbox-tick" type="checkbox" name="rid[]" value="{{$v->id}}" @if(in_array($v->id,$rids)) checked @endif>
	                                                    <span></span>
	                                                </label>
	                                            </td>
	                                            <td>
	                                                <!-- <img src="../assets/images/xs/avatar1.jpg" class="rounded-circle avatar" alt=""> -->
	                                                <p class="c_name">{{$v->name}}</p>
	                                            </td>
	                                           <!--  <td>
	                                                <span class="phone"><i class="fa fa-phone m-r-10"></i>264-625-2583</span>
	                                            </td>                                   
	                                            <td>
	                                                <address><i class="fa fa-map-marker"></i>123 6th St. Melbourne, FL 32904</address>
	                                            </td> -->
	                                        </tr>
	                                       @endforeach
                                    	</tbody>

									

                                </table>
                                	{{csrf_field()}}
                                	<input type="hidden" name="uid" value="{{$user->id}}">
                                	<button type="submit" class="btn btn-success">提交</button>
                                	<button type="reset" class="btn btn-info">重置</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection