@extends('Admin.AdminPublic.public')
@section("content")
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>公告管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">公告管理</li>
                            <li class="breadcrumb-item active">添加公告</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>添加公告</h2>
                        </div>
                        <div class="body">
                            <form id="basic-form" method="post" action="/adgg" novalidate>
                            	{{csrf_field()}}
                                <!-- <div class="form-group">
                                    <label>Text Input</label>
                                    <input type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Email Input</label>
                                    <input type="email" class="form-control" required>
                                </div> -->
                                <div class="form-group">
                                    <label>编写公告</label>
                                    <textarea class="form-control" rows="5" cols="30" name="content" required>请编写公告.......</textarea>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">发布</button>
                                <button type="reset" class="btn btn-info">重置</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection