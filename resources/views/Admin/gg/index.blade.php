@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>公告管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">公告管理</li>
                            <li class="breadcrumb-item active">公告列表</li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>公告列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            	@if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                                <table class="table m-b-0">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>添加时间</th>
                                            <th>公告</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										@foreach($data as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td><span>{{date('Y-m-d',$row->addtime)}}</span></td>
                                            <td><span class="text-info">{{$row->content}}</span></td>
                                            <td>
                                            	<a href="/adgg/{{$row->id}}/edit"><button class="btn btn-info" style="float:left">修改</button></a>
                                            	<form action="/adgg/{{$row->id}}" method="post">
                                            		{{csrf_field()}}
                                            		{{method_field("DELETE")}}
                                            		<button type="submit" class="btn btn-danger" style="float:left">删除</button>
                                            	</form>
                                            	
                                            </td>
                                        </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection