@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>轮播图管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">轮播图管理</li>
                            <li class="breadcrumb-item active">添加轮播图</li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="row clearfix">
                <div class="col-lg-7 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>添加轮播图</h2>
                        </div>
                         @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                              @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                        <form action="/adlunbo" method="post" enctype="multipart/form-data">
                        	{{csrf_field()}}
                        <div class="body">
                            
  								<div class="input-group mb-3" id="ImgCon"></div>
							  <div class="input-group mb-3">
                                <!-- <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">年份</button>
                                </div> -->

                                <input type="file" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="pic" id="changeMore">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">图片对应电影名称</button>
                                </div>

                                <input type="txet" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" name="name" required>
                            </div>
                            <br>
                            {{csrf_field()}}
                           <button type="submit" class="btn btn-success">添加</button>
                           <button type="reset" class="btn btn-danger">重置</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">

  function changeM(){
          var inputJ = $("#changeMore"),
              input  = inputJ[0],
              Con    = $("#ImgCon");
  
          inputJ.change(function(e){
              var file     = e.target.files[0],//拿到原始对象
                  thisType = file.type,//获取到表面的名称，可判断文件类型
                  thisSize = file.size,//文件的大小
                 reader   = new FileReader();
 
                 //readAsDataURL(file),读取文件，将文件以数据URL的形式保存在result的属性中
                 reader.readAsDataURL(file);

                 //文件加载成功以后，渲染到页面
                 reader.onload = function(e) {
                     var $img = $('<img style="with:100%;height:200px">').attr("src", e.target.result);
                     Con.append($img)
                 }
         });    
 
     }//changeM end!
     changeM();
</script>
@endsection