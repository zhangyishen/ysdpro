@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content" class="file_manager">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>轮播图列表</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">轮播图管理</li>
                            <li class="breadcrumb-item active">轮播图列表</li>
                        </ul>
                    </div>
                </div>
            </div>
                         @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                              @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
            <div class="row clearfix">


				@foreach($data as $row)
                <div class="col-lg-3 col-md-4 col-sm-12">
                    <div class="card">
                        <div class="file">
                            <div class="hover">
                            	<form action="/adlunbo/{{$row->id}}" method="post">
                            		{{csrf_field()}}
                            		{{method_field("DELETE")}}
                                	<button type="suubmit" class="btn btn-icon btn-danger" style="float: left">删除</button>
                            	</form>
                                <a href="/adlunbo/{{$row->id}}/edit" style="float: left"><button class="btn btn-info" >修改</button></a>
                            </div>
                            <a href="javascript:void(0);">
                                <div class="image">
                                    <img src="../uploads/lunbo/{{$row->pic}}" alt="img" class="img-fluid">
                                </div>
                                <div class="file-name">
                                    <p class="m-b-5 text-muted">名称 : {{$row->name}}</p>
                                    <small>ID: {{$row->id}} <span class="date text-muted">添加时间 : {{date('Y-m-d',$row->addtime)}}</span></small>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>                            
               @endforeach


            </div>
        </div>
    </div>
@endsection