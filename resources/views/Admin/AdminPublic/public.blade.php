<!doctype html>
<html lang="en">

<head>
<title>YSD影视后台管理</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Mplify Bootstrap 4.1.1 Admin Template">
<meta name="author" content="ThemeMakker, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="/admin/index/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/admin/index/vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="/admin/index/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/admin/index/vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="/admin/index/vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="/admin/index/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" type="text/css" href="/admin/index/common/myPagination.css">



<!-- MAIN CSS -->
<link rel="stylesheet" href="/admin/index/css/main.css">
<link rel="stylesheet" href="/admin/index/css/color_skins.css">
</head>
<body class="theme-blue">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="/admin/index/images/thumbnail.png" width="48" height="48" alt="Mplify"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a href="index.html">
                    <img src="/admin/index/images/logo-icon.svg" alt="Mplify Logo" class="img-responsive logo">
                    <span class="name">mplify</span>
                </a>
            </div>
            
            <div class="navbar-right">
                <ul class="list-unstyled clearfix mb-0">
                    <li>
                        <div class="navbar-btn btn-toggle-show">
                            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                        </div>                        
                        <a href="javascript:void(0);" class="btn-toggle-fullwidth btn-toggle-hide"><i class="fa fa-bars"></i></a>
                    </li>
                    <li>
                        <!-- <form id="navbar-search" class="navbar-form search-form">
                            <input value="" class="form-control" placeholder="Search here..." type="text">
                            <button type="button" class="btn btn-default"><i class="icon-magnifier"></i></button>
                        </form> -->
                    </li>
                    <li>
                        <div id="navbar-menu">
                            <ul class="nav navbar-nav">
                               



                                <!-- <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                        <i class="icon-bell"></i>
                                        <span class="notification-dot"></span>
                                    </a>
                                    <ul class="dropdown-menu animated bounceIn notifications">
                                        <li class="header"><strong>You have 4 new Notifications</strong></li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-info text-warning"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Campaign <strong>Holiday Sale</strong> is nearly reach budget limit.</p>
                                                        <span class="timestamp">10:00 AM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>                               
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-like text-success"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Your New Campaign <strong>Holiday Sale</strong> is approved.</p>
                                                        <span class="timestamp">11:30 AM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-pie-chart text-info"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Website visits from Twitter is 27% higher than last week.</p>
                                                        <span class="timestamp">04:00 PM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-info text-danger"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Error on website analytics configurations</p>
                                                        <span class="timestamp">Yesterday</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="footer"><a href="javascript:void(0);" class="more">See all notifications</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-flag"></i><span class="notification-dot"></span></a>
                                    <ul class="dropdown-menu animated bounceIn task">
                                        <li class="header"><strong>Project</strong></li>
                                        <li class="body">
                                            <ul class="menu tasks list-unstyled">
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Clockwork Orange <span class="float-right">29%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-turquoise" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Blazing Saddles <span class="float-right">78%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-slategray" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Project Archimedes <span class="float-right">45%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Eisenhower X <span class="float-right">68%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-coral" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Oreo Admin Templates <span class="float-right">21%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-amber" role="progressbar" aria-valuenow="21" aria-valuemin="0" aria-valuemax="100" style="width: 21%;"></div>
                                                        </div>
                                                    </a>
                                                </li>                        
                                            </ul>
                                        </li>
                                        <li class="footer"><a href="javascript:void(0);">View All</a></li>
                                    </ul>
                                </li> -->
                               




                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                        <img class="rounded-circle" src="../uploads/moren/moren.jpg" width="30" alt="">
                                    </a>
                                    <div class="dropdown-menu animated flipInY user-profile">
                                        <div class="d-flex p-3 align-items-center">
                                            <div class="drop-left m-r-10">
                                                <img src="../uploads/moren/moren.jpg" class="rounded" width="50" alt="">
                                            </div>
                                            <div class="drop-right">
                                                <h4>{{session('name')}}</h4>
                                                <!-- <p class="user-name">samuelmorris@info.com</p> -->
                                            </div>
                                        </div>
                                        <!-- <div class="m-t-10 p-3 drop-list">
                                            <ul class="list-unstyled">
                                                <li><a href="page-profile.html"><i class="icon-user"></i>个人中心</a></li>
                                                <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Messages</a></li>
                                                <li><a href="javascript:void(0);"><i class="icon-settings"></i>Settings</a></li>
                                                <li class="divider"></li>
                                                <li><a href="page-login.html"><i class="icon-power"></i>Logout</a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                </li>
                        



                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div id="leftsidebar" class="sidebar">
        <div class="sidebar-scroll">
            <nav id="leftsidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="heading">首页</li>
                    
                    <li ><a href="/ad"><i class="icon-home"></i><span>后台首页</span></a></li>



                    <!-- start管理员模块 -->
                    <li class="heading">管理员</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class=" icon-graduation"></i><span>管理员管理</span></a>
                        <ul>                                    
                            <li><a href="/adusers">管理员列表</a></li>
                            <li><a href="/adusers/create">添加管理员</a></li>
                            <li><a href="/rolelist">角色列表</a></li>
                            <li><a href="/rolelist/create">添加角色</a></li>
                            <li><a href="/authlist">权限列表</a></li>
                            <li><a href="/authlist/create">添加权限</a></li>
                        </ul>
                    </li>
                    <!-- end管理员模块 -->
                    
                    <!-- start公告模块 -->
                    <li class="heading">公告</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-bubble"></i><span>公告管理</span></a>
                        <ul>                                    
                            <li><a href="/adgg">公告列表</a></li>
                            <li><a href="/adgg/create">添加公告</a></li>
                        </ul>
                    </li>
                    <!-- end公告模块 -->

                    <!-- start轮播图模块 -->
                    <li class="heading">轮播图</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-picture"></i><span>轮播图管理</span></a>
                        <ul>                                    
                            <li><a href="/adlunbo">轮播图列表</a></li>
                            <li><a href="/adlunbo/create">添加轮播图</a></li>
                        </ul>
                    </li>
                    <!-- end轮播图模块 -->

                    <!-- start分类模块 -->
                    <li class="heading">分类</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-tag"></i><span>分类管理</span></a>
                        <ul>                                    
                            <li><a href="/adcate">分类列表</a></li>
                            <li><a href="/adcate/create">添加分类</a></li>
                            <li><a href="/adyear">年份列表</a></li>
                            <li><a href="/adyear/create">添加年份</a></li>
                            <li><a href="/adcountry">地区列表</a></li>
                            <li><a href="/adcountry/create">添加地区</a></li>
                        </ul>
                    </li>
                    <!-- end分类模块 -->

                     <!-- start添加电影模块 -->
                    <li class="heading">视频</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-control-play"></i><span>视频管理</span></a>
                        <ul>                                    
                            <li><a href="/advideo">视频列表</a></li>
                            <li><a href="/advideo/create">添加视频</a></li>
                        </ul>
                    </li>
                    <!-- end添加电影模块 -->
                    
                    <!-- start会员模块 -->
                    <li class="heading">会员</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-diamond"></i><span>会员管理</span></a>
                        <ul>                                    
                            <li><a href="/adorder">会员订单</a></li>
                            <li><a href="/advip">会员套餐</a></li>
                            <li><a href="/advip/create">添加套餐</a></li>
                        </ul>
                    </li>
                    <!-- end会员模块 -->

                    <!-- start用户模块 -->
                    <li class="heading">用户</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class=" icon-users"></i><span>用户管理</span></a>
                        <ul>                                    
                            <li><a href="/aduser">用户列表</a></li>
                            <li><a href="/aduser/create">添加用户</a></li>
                        </ul>
                    </li>
                    <!-- end用户模块 -->

                    <!-- start帖子模块 -->
                    <li class="heading">帖子</li>
                    <li>
                        <a href="#FileManager" class="has-arrow"><i class="icon-book-open"></i><span>帖子管理</span></a>
                        <ul>                                    
                            <li><a href="/tiezi">帖子列表</a></li>
                            <li><a href="/tiezitype">帖子板块</a></li>
                            <li><a href="/tiezi/create">添加板块</a></li>
                        </ul>
                    </li>
                    <!-- end帖子模块 -->
                    
                    <!-- 退出登录 -->
                    <li class="heading">退出登录</li>
                    <li ><a href="/adlogin"><i class="icon-logout"></i><span>LogOut</span></a></li>
                    <!-- end退出登录 -->
                  
                   
                </ul>
            </nav>
        </div>
    </div>

    <div id="mega_menubar" class="mega_menubar">
        <div class="container">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Subscribe</h2>
                        </div>
                        <div class="body">
                            <form>
                                <div class="form-group">
                                    <input type="text" value="" placeholder="Enter Name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="text" value="" placeholder="Enter Email" class="form-control">
                                </div>
                                <button class="btn btn-primary">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Accordion</h2>
                        </div>
                        <div class="body">
                            <ul class="accordion2">
                                <li class="accordion-item is-active">
                                    <h3 class="accordion-thumb"><span>Lorem ipsum</span></h3>
                                    <p class="accordion-panel">
                                        Lorem ipsum dolor sit amet, elit. Placeat, quibusdam! Voluptate nobis
                                    </p>
                                </li>
                                
                                <li class="accordion-item">
                                    <h3 class="accordion-thumb"><span>Dolor sit amet</span></h3>
                                    <p class="accordion-panel">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing  Voluptate nobis
                                    </p>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Company</h2>
                        </div>
                        <div class="body">
                            <ul class="list-unstyled links">
                                <li><a href="javascript:void(0);" title="" >Our Facts</a></li>
                                <li><a href="javascript:void(0);" title="" >Confidentiality</a></li>                                
                                <li><a href="javascript:void(0);" title="" >About Us</a></li>
                                <li><a href="javascript:void(0);" title="" >Testimonials</a></li>
                                <li><a href="javascript:void(0);" title="" >Contact Us</a></li>                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2>Image Gallery</h2>
                        </div>
                        <div class="body">
                            <div class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img src="/admin/index/images/image-gallery/1.jpg" class="img-fluid" alt="img" />
                                    </div>
                                    <div class="carousel-item">
                                        <img src="/admin/index/images/image-gallery/2.jpg" class="img-fluid" alt="img" />
                                    </div>
                                    <div class="carousel-item">
                                        <img src="/admin/index/images/image-gallery/3.jpg" class="img-fluid" alt="img" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs tab-nav-right" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#skins" aria-expanded="true">Mplify</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#contact" aria-expanded="false">Contact</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#Timeline" aria-expanded="false">Timeline </a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane animated fadeIn in active" id="skins" aria-expanded="true">
                <div class="sidebar-scroll">
                    <div class="card">
                        <div class="header">
                            <h2>General Setting</h2>
                        </div>
                        <div class="body">
                            <ul class="setting-list list-unstyled">
                                <li>
                                    <label for="checkbox1" class="fancy-checkbox">
                                        <input id="checkbox1" type="checkbox">
                                        <span>Report Panel Usage</span>
                                    </label>
                                </li>
                                <li>
                                    <label for="checkbox2" class="fancy-checkbox">
                                        <input id="checkbox2" type="checkbox" checked>
                                        <span>Email Redirect</span>
                                    </label>
                                </li>
                                <li>
                                    <label for="checkbox3" class="fancy-checkbox">
                                        <input id="checkbox3" type="checkbox" checked>
                                        <span>Notifications</span>
                                    </label>         
                                </li>
                                <li>
                                    <label for="checkbox4" class="fancy-checkbox">
                                        <input id="checkbox4" type="checkbox">
                                        <span>Auto Updates</span>
                                    </label>
                                </li>
                                <li>
                                    <label for="checkbox5" class="fancy-checkbox">
                                        <input id="checkbox5" type="checkbox">
                                        <span>Offline</span>
                                    </label>
                                </li>
                                <li>
                                    <label for="checkbox6" class="fancy-checkbox">
                                        <input id="checkbox6" type="checkbox">
                                        <span>Location Permission</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Color Skins</h2>
                        </div>
                        <div class="body">
                            <ul class="choose-skin list-unstyled">
                                <li data-theme="purple">
                                    <div class="purple"></div>
                                    <span>Purple</span>
                                </li>                   
                                <li data-theme="blue" class="active">
                                    <div class="blue"></div>
                                    <span>Blue</span>
                                </li>
                                <li data-theme="cyan">
                                    <div class="cyan"></div>
                                    <span>Cyan</span>
                                </li>
                                <li data-theme="green">
                                    <div class="green"></div>
                                    <span>Green</span>
                                </li>
                                <li data-theme="orange">
                                    <div class="orange"></div>
                                    <span>Orange</span>
                                </li>
                                <li data-theme="blush">
                                    <div class="blush"></div>
                                    <span>Blush</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Storage</h2>
                        </div>
                        <div class="body">
                            <div class="progress progress-xs mb-0">
                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 89%;">
                                </div>
                            </div>
                            <small>50MB of 10GB Used</small>
                            <button type="button" class="btn btn-primary btn-block mt-3">Upgrade Storage</button>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane animated fadeIn" id="contact" aria-expanded="false">
                <div class="sidebar-scroll">
                    <div class="card">
                        <div class="header">
                            <h2>Recent Chat</h2>
                        </div>
                        <div class="body">
                            <ul class="right_chat list-unstyled">
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img class="media-object " src="/admin/index/images/xs/avatar4.jpg" alt="">
                                            <div class="media-body">
                                                <span class="name">Chris Fox</span>
                                                <span class="message">Angular Champ</span>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </a>                            
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img class="media-object " src="/admin/index/images/xs/avatar5.jpg" alt="">
                                            <div class="media-body">
                                                <span class="name">Joge Lucky</span>
                                                <span class="message">Sales Lead</span>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </a>                            
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img class="media-object " src="/admin/index/images/xs/avatar2.jpg" alt="">
                                            <div class="media-body">
                                                <span class="name">Isabella</span>
                                                <span class="message">CEO, Thememakker</span>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </a>                            
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img class="media-object " src="/admin/index/images/xs/avatar1.jpg" alt="">
                                            <div class="media-body">
                                                <span class="name">Folisise Chosielie</span>
                                                <span class="message">PHP Expert</span>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </a>                            
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img class="media-object " src="/admin/index/images/xs/avatar3.jpg" alt="">
                                            <div class="media-body">
                                                <span class="name">Alexander</span>
                                                <span class="message">eCommerce Master</span>
                                                <span class="badge badge-outline status"></span>
                                            </div>
                                        </div>
                                    </a>                            
                                </li>                        
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="header">
                            <h2>Contact List</h2>
                        </div>
                        <div class="body">
                            <ul class="list-unstyled contact-list">
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar1.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Vincent Porter <span class="d-block">London UK</span></h4>
                                </li>
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar2.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Mike Thomas <span class="d-block">London UK</span></h4>
                                </li>
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar3.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Aiden Chavaz</h4>
                                </li>
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar4.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Vincent Porter <span class="d-block">London UK</span></h4>
                                </li>
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar5.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Mike Thomas <span class="d-block">London UK</span></h4>
                                </li>
                                <li class="d-flex align-items-center">
                                    <span class="contact-img">
                                        <img src="/admin/index/images/xs/avatar6.jpg" class="rounded" alt="">
                                    </span>
                                    <h4 class="contact-name">Aiden Chavaz</h4>
                                </li>                             
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane animated fadeIn" id="Timeline" aria-expanded="false">
                <div class="sidebar-scroll">
                    <div class="card">
                        <div class="header">
                            <h2>My Stats</h2>
                        </div>
                        <div class="body">
                            <ul class="list-unstyled basic-list">
                                <li><i class="icon-book-open m-r-5"></i> Active Projects <span class="badge badge-primary">21</span></li>
                                <li><i class="icon-list m-r-5"></i> Task Pending <span class="badge-purple badge">50</span></li>
                                <li><i class="fa fa-ticket m-r-5"></i> Support Tickets<span class="badge-success badge">9</span></li>
                                <li><i class="icon-tag m-r-5"></i> New Projects<span class="badge-info badge">7</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">
                            <div class="new_timeline mt-3">
                                <div class="header">
                                    <div class="color-overlay">
                                        <div class="day-number">8</div>
                                        <div class="date-right">
                                        <div class="day-name">Monday</div>
                                        <div class="month">July 2018</div>
                                        </div>
                                    </div>                                
                                </div>
                                <ul>
                                    <li>
                                        <div class="bullet pink"></div>
                                        <div class="time">11am</div>
                                        <div class="desc">
                                            <h3>Attendance</h3>
                                            <h4>Computer Class</h4>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="bullet green"></div>
                                        <div class="time">12pm</div>
                                        <div class="desc">
                                            <h3>Developer Team</h3>
                                            <h4>Hangouts</h4>
                                            <ul class="list-unstyled team-info margin-0 p-t-5">                                            
                                                <li><img src="/admin/index/images/xs/avatar1.jpg" alt="Avatar"></li>
                                                <li><img src="/admin/index/images/xs/avatar2.jpg" alt="Avatar"></li>
                                                <li><img src="/admin/index/images/xs/avatar3.jpg" alt="Avatar"></li>
                                                <li><img src="/admin/index/images/xs/avatar4.jpg" alt="Avatar"></li>                                            
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="bullet orange"></div>
                                        <div class="time">1:30pm</div>
                                        <div class="desc">
                                            <h3>Lunch Break</h3>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="bullet green"></div>
                                        <div class="time">2pm</div>
                                        <div class="desc">
                                            <h3>Finish</h3>
                                            <h4>Go to Home</h4>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- 占位 -->

    @section('content')
    
    @show

    <!-- end -->



   
    
</div>

<!-- Javascript -->
<script src="/admin/index/bundles/libscripts.bundle.js"></script>    
<script src="/admin/index/bundles/vendorscripts.bundle.js"></script>

<script src="/admin/index/bundles/chartist.bundle.js"></script>
<script src="/admin/index/bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="/admin/index/bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="/admin/index/vendor/flot-charts/jquery.flot.selection.js"></script>

<script src="/admin/index/bundles/mainscripts.bundle.js"></script>
<script src="/admin/index/js/index.js"></script>

<script src="/admin/index/bundles/morrisscripts.bundle.js"></script>
<script>
    $('.sparkbar').sparkline('html', { type: 'bar' });
</script>


<script src="/admin/index/bundles/datatablescripts.bundle.js"></script>
<script src="/admin/index/vendor/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/admin/index/vendor/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/admin/index/vendor/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/admin/index/vendor/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/admin/index/vendor/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="/admin/index/vendor/sweetalert/sweetalert.min.js"></script> <!-- SweetAlert Plugin Js --> 


<!-- <script src="/admin/index/bundles/mainscripts.bundle.js"></script> -->
<script src="/admin/index/bundles/morrisscripts.bundle.js"></script>
<script src="/admin/index/js/pages/tables/jquery-datatable.js"></script>
<script src="/admin/index/common/myPagination.js"></script>
<script type="text/javascript">
    $("#disx").click(function(){
    $(this).fadeOut("slow");
  });
</script>

</body>
</html>
