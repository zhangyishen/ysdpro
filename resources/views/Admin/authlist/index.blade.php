@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>管理员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">管理员管理</li>
                            <li class="breadcrumb-item active">权限列表</li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <h2>权限列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>权限名</th>
                                        <th>控制器</th>
                                        <th>方法</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $v)
	                                    <tr>
	                                        <td>{{$v->id}}</td>
	                                        <td>{{$v->name}}</td>
                                            <td>{{$v->mname}}</td>
                                            <td>{{$v->aname}}</td>
	                                        <!-- <td><label class="badge">Approved</label></td> -->
	                                        <td>
	                                        	<a href="/authlist/{{$v->id}}/edit" class="btn btn-info btn-sm" style="float:left">修改</i></a>
	                                        	<form action="/authlist/{{$v->id}}" method="post">
	                                        		{{csrf_field()}}
	                                        		{{method_field('DELETE')}}
	                                        		<button class="btn btn-danger btn-sm" onclick="if(confirm('确定删除此条数据?')==false)return false" style="float:left">删除</button>
	                                        	</form>
	                                        </td>

	                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div  class="pagination">{{$data->render()}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection