@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>管理员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">管理员管理</li>
                            <li class="breadcrumb-item active">管理员列表</li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <h2>管理员列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>账号</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($role as $row)
	                                    <tr>
	                                        <td>{{$row->id}}</td>
	                                        <td>{{$row->name}}</td>
	                                        <!-- <td><label class="badge">Approved</label></td> -->
	                                        <td>
	                                        	<a href="/rolelist/{{$row->id}}/edit" class="btn btn-info btn-sm" style="float:left">修改</i></a>
	                                        	<form action="/rolelist/{{$row->id}}" method="post">
	                                        		{{csrf_field()}}
	                                        		{{method_field('DELETE')}}
	                                        		<button class="btn btn-danger btn-sm" style="float:left">删除</button>
	                                        	</form>
                                                <a href="/auth/{{$row->id}}" style="float:left"><button class="btn btn-success btn-sm">分配权限</button></a>
	                                        </td>

	                                    </tr>
	                               @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection