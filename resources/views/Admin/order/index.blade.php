@extends("Admin.AdminPublic.public")
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>会员管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">会员管理</li>
                            <li class="breadcrumb-item active">会员订单</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card product_item_list">
                        <div class="header">
                            <h2>会员订单</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            	 @if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                                <table class="table table-hover m-b-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>ID</th>
                                            <th>订单号</th>
                                            <th data-breakpoints="sm xs">用户</th>
                                            <th data-breakpoints="xs">套餐</th>
                                            <th data-breakpoints="xs md">创建时间</th>
                                            <th data-breakpoints="sm xs md">状态</th>
                                            <th>操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										@foreach($order as $row)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            <td><h5>{{$row->order_id}}</h5></td>
                                            <td><span class="text-muted">{{$row->user_user}}</span></td>
                                            <td>{{$row->setmeal}}</td>
                                            <td>{{date("Y-m-d H:i:s",$row->addtime)}}</td>
                                            <td><span class="col-green">{{$row->status==0?'订单成功':'订单失败'}}</span></td>
                                            <td>
                                                <!-- <a href="javascript:void(0);" class="btn btn-outline-secondary"><i class="icon-pencil"></i></a> -->
												<form action="/adorder/{{$row->id}}" method="post">
                                                	{{csrf_field()}}
                                                	{{method_field('DELETE')}}
                                                	<button type="submit" class="btn btn-outline-danger"><i class="icon-trash"></i></button>
													
												</form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>

        </div>
    </div>
@endsection