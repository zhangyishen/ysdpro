@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>帖子管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">帖子管理</li>
                            <li class="breadcrumb-item active">修改板块</li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>修改板块</h2>
                        </div>
                        @if(session('success'))
                                {{session('success')}}
                         @endif

                         
                            @if(session('error'))
                                     {{session('error')}}
                            @endif
                        <div class="body">
                        	<form action="/tiezi/{{$data->id}}" method="post">
                        		@if (count($errors)>0)
	                            <div class="alert alert-danger" id="disx">
	                                <ul>
	                                    @foreach ($errors->all() as $error)
	                                        <li>{{ $error }}</li>
	                                    @endforeach
	                                </ul>
	                            </div>
	                            @endif
                        		{{csrf_field()}}
                        		{{method_field('PUT')}}
	                            <div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">地区</button>
	                                </div>
	                                <input type="text" class="form-control" placeholder="请填写地区" name="name" aria-label="" aria-describedby="basic-addon1" value="{{$data->name}}">
	                            </div>
								
								<!-- <div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">密码</button>
	                                </div>
	                                <input type="password" class="form-control" placeholder="请填写密码" aria-label="" name="password" aria-describedby="basic-addon1">
	                            </div> -->
								
								<div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <button class="btn btn-outline-secondary" type="button">状态</button>
	                                </div>
	                                <select name="status" class="custom-select" id="inputGroupSelect03">
	                                    <!-- <option selected value="0">--请选择(不选择默认添加为父类)--</option> -->
	                                    <option value="0" @if($data->status==0) selected @endif>启用</option>
	                                    <option value="1" @if($data->status==1) selected @endif>禁用</option>
	                                </select>
                            	</div>
								<!-- <h6>2.选择管理员权限</h6>
	                          	<div class="input-group mb-3">
	                                <div class="input-group-prepend">
	                                    <label class="input-group-text" for="inputGroupSelect01">权限</label>
	                                </div>
	                                <select class="custom-select" id="inputGroupSelect01">
	                                    <option selected>--请选择--</option>
	                                    <option value="1">One</option>
	                                    <option value="2">Two</option>
	                                    <option value="3">Three</option>
	                                </select>
	                            </div> -->

	                            <button type="submit" class="btn btn-primary">修改</button>
	                            <button type="reset" class="btn btn-danger">重置</button>
							</form>

                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection