@extends('Admin.AdminPublic.public')
@section('content')
<div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>帖子管理</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">帖子管理</li>
                            <li class="breadcrumb-item active">帖子列表</li>
                        </ul>
                    </div>
                </div>
            </div>
            
          
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>帖子列表</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                            	@if(session('success'))
                                <div class="alert alert-success" id="disx">{{session('success')}}</div>
                         @endif

                         
                            @if(session('error'))
                                     <div class="alert alert-danger" id="disx">{{session('error')}}</div>
                            @endif
                                <table class="table center-aligned-table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>标题</th>
                                        <th>发布人</th>
                                        <th>发布时间</th>
                                        <th>浏览量</th>
                                        <th>点赞次数</th>
                                        <th>评论条数</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tiezi as $row)
                                    <tr>
                                        <td>{{$row->id}}</td>
                                        <td>{{$row->title}}</td>
                                        <td>{{$row->uname}}</td>
                                        <td>{{date("Y-m-d H:i:s",$row->addtime)}}</td>
                                        <td>{{$row->liulan}}次</td>
                                        <td>{{$row->zan}}次</td>
                                        <td>{{$row->num}}条</td>
                                        <td>
                                        	<form action="/tiezi/{{$row->id}}" method="post">
                                        	{{csrf_field()}}
                                        	{{method_field("DELETE")}}
                                        	<button class="btn btn-danger btn-sm"><i class="icon-trash"></i></button>
                                        		
                                        	</form>
                                        </td>
                                    </tr>
									@endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection