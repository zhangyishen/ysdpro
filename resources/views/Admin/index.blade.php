@extends("Admin.AdminPublic.public")
@section('content')
 <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>后台首页</h2>
                    </div>
                    @if(session('error'))
                        <div class="alert alert-danger" id="disx">
                            {{session('error')}}
                        </div>
                    @endif            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">后台首页</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card top_report">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x text-col-blue icon-user-follow"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>用户数量</h6>
                                            <span class="font700">{{$data}}</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-blue mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="{{$data}}"></div>
                                    </div>
                                    <small class="text-muted">{{$data}}位用户</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-bar-chart-o text-col-green"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>收入总额</h6>
                                            <span class="font700">{{$price}}</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-green mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="{{$price}}"></div>
                                    </div>
                                    <small class="text-muted">所有订单收入总额 {{$price}}</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-shopping-cart text-col-red"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>订单数量</h6>
                                            <span class="font700">{{$order}}</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-red mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="{{$order}}"></div>
                                    </div>
                                    <small class="text-muted">{{$order}}个订单</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-thumbs-up text-col-yellow"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>影视数量</h6>
                                            <span class="font700">{{$video}}</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-yellow mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="{{$video}}"></div>
                                    </div>
                                    <small class="text-muted">{{$video}}个视频</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
               <div class="col-lg-4 col-md-12">
                    <div class="card profile-header">
                        <div class="body">
                            <div class="profile-image"> <img src="../uploads/moren/moren.jpg"  width="120px" class="rounded-circle" alt=""> </div><br>
                            <div>
                                <h4 class="m-b-0" style="text-align: center;"><strong>欢迎管理员 : </strong>{{session('name')}}</h4>
                                <span></span>
                                <!-- <p class="font-14 m-b-0 m-t-10"></p> -->

                            </div>
                            <!-- <div class="m-t-15">
                                <button class="btn btn-outline-secondary"><i class="fa fa-envelope"></i> Message</button>
                            </div> -->
                            <!-- <div class="border-top p-t-20 m-t-20 text-left">
                                <ul class="list-unstyled profile-social">
                                    <li><a class="text-col-dark" href="javascript:void(0);"><i class="fa fa-facebook"></i> Facebook</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-instagram"></i> Instagram</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-twitter"></i> Twitter</a></li>
                                    <li><a href="javascript:void(0);"><i class="fa fa-linkedin"></i> Linkedin</a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div>

                    
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="card text-center">
                        <div class="header">
                            <h2>用户状态</h2>
                        </div>
                        <div class="body pt-0">
                            <div class="row">
                                <div class="col-12 m-b-15">
                                    <h1><i class="icon-user"></i></h1>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h4 class="font-22 text-col-green font-weight-bold">
                                        <small class="font-12 text-col-dark d-block m-b-10">正常用户</small>
                                        {{$data1}}
                                    </h4>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h4 class="font-22 text-col-blue font-weight-bold">
                                        <small class="font-12 text-col-dark d-block m-b-10">禁用用户</small>
                                        {{$data2}}
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12">
                    <div class="card overflowhidden">
                        <div class="header">
                            <h2>后台数据</h2>
                           <!--  <ul class="header-dropdown">
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul> -->
                        </div>
                        <div class="body">
                            <div class="row text-center">
                                <div class="col-4">
                                    <h4>{{$data3}} <span class="font-13 d-block mt-2">会员数量</span></h4>
                                </div>
                                <div class="col-4 border-left border-right">
                                    <h4>{{$aduser}} <span class="font-13 d-block mt-2">管理员数量</span></h4>
                                </div>
                                <div class="col-4">
                                    <h4>{{$node}}<span class="font-13 d-block mt-2">权限数量</span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="sparkline" data-type="bar" data-offset="90" data-width="97%" data-height="50px" data-bar-Width="10" data-bar-Spacing="10" data-bar-Color="#7cb5ec">4,8,0,3,1,8,5,4,0,5,4,3,2,1,5,6,7,8,4,5,8,0,3</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection