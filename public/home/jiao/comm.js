var uname;
$(function() {
	$("#closereply").click(function() {
		$("#replycontent").val('');
		$("#rpnotice").html('');
		$("#reply-box").removeClass('reply-box-fd');
	    $("#closereply").hide();
	    $("#rpid").val('0');
	    $("#rpuid").val('0');
	    $("#reply-to-box").text('回复：楼主');
	});
});

function reply(id,uid,name) {
	uname = name;
	$("#reply-box").addClass('reply-box-fd');
	$("#rpid").val(id);
	$("#rpuid").val(uid);
	$("#reply-to-box").text("");
	$("#reply-to-box").append(name);
	$("#closereply").show();
}
function rpsubmit(link) {
	var id = $("#rpid").val();
	var uid = $("#rpuid").val();
	var content = $("#replycontent").val();
	if(!id || !uid || !content){
		$("#rpnotice").html("数据不完整！");
		return;
	}
	$.post(link, {
		id:id,uid:uid,tid:tid,content:content
	}, function(data) {
		if(data['msg'] == 'ok'){
			$("#hfnum").html(parseInt($("#hfnum").html())+1);
			$("#rpnotice").html("回复成功");
			$("#replycontent").val("");
			if(id>0){
				uname = uname.replace("回复：","");
                var html = '<div class="reply2'+data['id']+'" style="border-top:1px solid rgb(230,230,230);width:100%;margin:10px 0;"></div><div class="container-fluid reply2'+data['id']+'" id="reply'+data['id']+'"><div class="reply-author"><a href="javascript:;"><img width="28px" style="border-radius:3px" title="'+data['uname']+'" src="'+data['upic']+'"></a> <a href="javascript:;">'+data['uname']+'</a> <span class="list-time">'+data['addtime']+'</span></div><div class="reply-content reply-child">'+content+'<div style="overflow:auto"><a onclick="del(\''+data['dellink']+'\',\''+data['id']+'\')" class="reply-del" href="javascript:;">删除</a><a href="javascript:;" onclick="reply(\''+data['id']+'\',\''+data['uid']+'\',\'回复：'+data['uname']+'\')" class="reply-btn">回复</a></div></div></div>';
			    $("#reply"+id).append(html);
			}else{
                var html = '<li class="list-group-item list-topic-item reply2'+data['id']+'"><table style="width:100%;overflow:auto"><tbody><tr><td width="48px" valign="top" align="center"><a href="javascript:;"><img class="avatar-middle" title="'+data['uname']+'" src="'+data['upic']+'"></a></td><td width="10px"></td><td width="auto"><div class="reply-author"><a href="javascript:;">'+data['uname']+'</a> <span class="list-time">'+data['addtime']+'</span></div><div class="reply-content">'+content+'<div style="overflow:auto"><a onclick="del(\''+data['dellink']+'\',\''+data['id']+'\')" class="reply-del" href="javascript:;">删除</a><a href="javascript:;" onclick="reply(\''+data['id']+'\',\''+data['uid']+'\',\'回复：'+data['uname']+'\')" class="reply-btn">回复</a></div></div></td></tr></tbody></table></li>';
			    $(".list-group").append(html);
			}
			//window.location.reload();
		}else{
			$("#rpnotice").html(data['msg']);
			$("#replycontent").val('');
		}
	},'json');
}

function clike(link){
	$.post(link, {
		tid:tid
	}, function(data) {
		if(data == '1'){
			var z = $('#zan').attr('z');
			var zhits = $('#zan').attr('zhits');
			if(z=='1'){
			   zhits = parseInt(zhits)-1;
               $('#zan').css('color','').attr('z','0').attr('zhits',zhits);
			   $('#zhits').html(' '+zhits+' 赞');
			}else{
			   zhits = parseInt(zhits)+1;
               $('#zan').css('color','#0e90d2').attr('z','1').attr('zhits',zhits);
			   $('#zhits').html(' '+zhits+' 已赞');
			}
			//window.location.reload();
		}else{
			alert(data);
		}
	});
}
function collect(link){
	$.post(link, {
		tid:tid
	}, function(data) {
		if(data == '1'){
			var s = $('#fav').attr('s');
			if(s=='1'){
               $('#fav').css('color','').attr('s','0');
			   $('#sc').html(' 加入收藏');
			}else{
               $('#fav').css('color','#0e90d2').attr('s','1');
			   $('#sc').html(' 已收藏');
			}
			//window.location.reload();
		}else{
			alert(data);
		}
	});
}

function del(link,ids){
	if(confirm("确定删除？")) { 
	    $.post(link, {id: ids}, function(data){
             if(data=='ok'){
				  $("#hfnum").html(parseInt($("#hfnum").html())-1);
                  $('.reply2'+ids).remove();
			 }else{
                  alert(data);
			 }
	    });
	}
}