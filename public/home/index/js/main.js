$(function () {
    if ($('.movie-screenshot').length>0) {
        $(".movie-screenshot a:has(img)").slimbox({
            overlayOpacity: 0.75,
            overlayFadeDuration: 200,
            imageFadeDuration: 200,
            captionAnimationDuration: 200,
            loop:false,
            counterText:"Image {x} of {y}"
        });
    };
    $(".casts-swith").bind('click',
        function(){
		    var sid = $(this).attr('sid');
            if ( sid == 1 ) {
				$('#casts').children('a').each(function(i,n){
					var obj = $(n);
                    if(i > 5) obj.hide();
				});
				$(this).attr('sid','0');
                $(this).html('<i class="glyphicon glyphicon-chevron-down"></i>展开全部');
            }else{
				 $('#casts').children('a').each(function(i,n){
					var obj = $(n);
                    obj.show();
				});
				$(this).attr('sid','1');
                $(this).html('<i class="glyphicon glyphicon-chevron-up"></i>收起部分');
            };
			$(".casts-swith").show();
        }
    );
});
function fav(url) {
	$.get(url, function(data) {
		if (data == 'ok') {
			alert('恭喜你，收藏成功！')
		} else {
			alert(data);
		}
	})
}