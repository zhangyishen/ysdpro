var uname;
$(function() {
	$("#closereply").click(function() {
		$("#replycontent").val('');
		$("#rpnotice").html('');
		$("#reply-box").removeClass('reply-box-fd').css('display','none');
	    $("#closereply").hide();
	    $("#rpid").val('0');
	    $("#rpuid").val('0');
	    $("#reply-to-box").text('回复：楼主');
	});
});

function reply(id,uid,name) {
	uname = name;
	$("#reply-box").addClass('reply-box-fd').css('display','block');
	$("#ping_id").val(id);
	$("#tie_id").val(uid);
	$("#reply-to-box").text("");
	$("#reply-to-box").append(name);
	$("#closereply").show();
}

function rpsubmit(link) {
	var ping_id = $("#ping_id").val();
	var tie_id = $("#tie_id").val();
	var content = $("#replycontent").val();
	if(!content){
		$("#rpnotice").html("数据不完整！");
		return;
	}
	$.get("/gethui", {
		ping_id:ping_id,tie_id:tie_id,content:content
	}, function(data) {
		// alert(tie_id);
			if(data==1){
				location.href='/jiaoliu/'+tie_id;
			}else if(data==3){
				alert('请先登录,再评论');
				location.href='/login/create';
			}else{
				alert('评论失败');
			}
	});
}

function clike(link){
	// alert(1);
	$.post(link, {
		tid:tid
	}, function(data) {
		if(data == '1'){
			var z = $('#zan').attr('z');
			var zhits = $('#zan').attr('zhits');
			if(z=='1'){
			   zhits = parseInt(zhits)-1;
               $('#zan').css('color','').attr('z','0').attr('zhits',zhits);
			   $('#zhits').html(' '+zhits+' 赞');
			}else{
			   zhits = parseInt(zhits)+1;
               $('#zan').css('color','#0e90d2').attr('z','1').attr('zhits',zhits);
			   $('#zhits').html(' '+zhits+' 已赞');
			}
			//window.location.reload();
		}else{
			alert(data);
		}
	});
}
function collect(link){
	$.post(link, {
		tid:tid
	}, function(data) {
		if(data == '1'){
			var s = $('#fav').attr('s');
			if(s=='1'){
               $('#fav').css('color','').attr('s','0');
			   $('#sc').html(' 加入收藏');
			}else{
               $('#fav').css('color','#0e90d2').attr('s','1');
			   $('#sc').html(' 已收藏');
			}
			//window.location.reload();
		}else{
			alert(data);
		}
	});
}

function del(link,ids){
	if(confirm("确定删除？")) { 
	    $.post(link, {id: ids}, function(data){
             if(data=='ok'){
				  $("#hfnum").html(parseInt($("#hfnum").html())-1);
                  $('.reply2'+ids).remove();
			 }else{
                  alert(data);
			 }
	    });
	}
}