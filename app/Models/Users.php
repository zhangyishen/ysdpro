<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    //定义与模型关联的数据表
    protected $table="user";
    //主键
    protected $primaryKey="id";
    //设置是否需要自动维护时间戳
    public $timestamps = true;

 //可被批量赋值的属性
 protected $fillable = ['user','pass','email','status','token','phone'];
 //修改器的方法
 //对完成的数据(状态 status)做自动处理
 public function getStatusAttribute($value){
 	$status=[1=>'禁用',0=>'激活'];
 	return $status[$value];
 }
 
}
