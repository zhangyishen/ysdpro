<?php

/**
 * @Author: Mr.YS
 * @Date:   2018-12-24 12:09:36
 * @Last Modified by:   Mr.YS
 * @Last Modified time: 2018-12-28 13:01:02
 */
//做支付
function pay($order,$payfu,$aname,$price){
	/* *
 * 功能：即时到账交易接口接入页
 * 
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

require_once("epay.config.php");
require_once("lib/epay_submit.class.php");

/**************************请求参数**************************/
        $notify_url = "http://pay.sddyun.cn/SDK/notify_url.php";
        //需http://格式的完整路径，不能加?id=123这类自定义参数

        //页面跳转同步通知页面路径
        $return_url = "http://ysd.ydtun.com/returnurl";
        //需http://格式的完整路径，不能加?id=123这类自定义参数，不能写成http://localhost/

        //商户订单号
        // $out_trade_no = $_POST['WIDout_trade_no'];
        $out_trade_no = $order;
        //商户网站订单系统中唯一订单号，必填


		//支付方式
        $type = $payfu;
        //商品名称
        $name = $aname;
		//付款金额
        $money = $price;
		//站点名称
        $sitename = 'YSD影视';
        //必填

        //订单描述


/************************************************************/

//构造要请求的参数数组，无需改动
$parameter = array(
		"pid" => trim($alipay_config['partner']),
		"type" => $type,
		"notify_url"	=> $notify_url,
		"return_url"	=> $return_url,
		"out_trade_no"	=> $out_trade_no,
		"name"	=> $name,
		"money"	=> $money,
		"sitename"	=> $sitename
);

//建立请求
$alipaySubmit = new AlipaySubmit($alipay_config);
$html_text = $alipaySubmit->buildRequestForm($parameter);
echo $html_text;
}

//发送短信校验码(调用短信接口)
function sendphone($p){
	// echo "this is phone";
	//初始化必填
	//填写在开发者控制台首页上的Account Sid
	$options['accountsid']='2c0e28b715555c51ff213b723d3cb92a';
	//填写在开发者控制台首页上的Auth Token
	$options['token']='592b43dfe2c4ca8b73e8139a9d1c3f22';

	//初始化 $options必填
	$ucpass = new Ucpaas($options);

	$appid = "865090c99124485db7bbcfc4f13b4e03";	//应用的ID，可在开发者控制台内的短信产品下查看
	$templateid = "411778";    //可在后台短信产品→选择接入的应用→短信模板-模板ID，查看该模板ID
	$param = rand(1,10000); //多个参数使用英文逗号隔开（如：param=“a,b,c”），如为参数则留空
	//存储在cookie
	\Cookie::queue('fcode',$param,1);
	$mobile = $p;
	$uid = "";

	//70字内（含70字）计一条，超过70字，按67字/条计费，超过长度短信平台将会自动分割为多条发送。分割后的多条短信将按照具体占用条数计费。

	echo $ucpass->SendSms($appid,$templateid,$param,$mobile,$uid);
}