<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class LunboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //加载模板
        $data = DB::table('lunbo')->get();
        return view("Admin.lunbo.index",['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //加载模板
        return view("Admin.lunbo.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //执行添加
        $data = $request->except('_token');
        // dd($data);
        if($request->hasFile('pic')){
            //初始化名字
            $name = time()+rand(1,99999);
            //获取文件上传后缀
            $ext=$request->file("pic")->getClientOriginalExtension();
            //设置文件名
            $request->file("pic")->move("./uploads/lunbo",$name.".".$ext);

            $pic = $name.'.'.$ext;
            $data['pic']=$pic;
            $data['addtime']=time();

            $db = DB::table('lunbo')->insert($data);
            if($db){
                    return redirect('/adlunbo')->with('success','添加成功');
                }else{
                    return back()->with('error','添加失败');
                }
        }else{
            return back()->with('error','请添加图片');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('lunbo')->where('id','=',$id)->first();
        //加载模板
        return view("Admin.lunbo.edit",['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //执行修改
        $first = DB::table('lunbo')->where('id','=',$id)->first();

        $data = $request->except('_token','_method');
        if($request->hasFile('pic')){
            //初始化名字
            $name = time()+rand(1,99999);
            //获取文件上传后缀
            $ext=$request->file("pic")->getClientOriginalExtension();
            //设置文件名
            $request->file("pic")->move("./uploads/lunbo",$name.".".$ext);

            $pic = $name.'.'.$ext;
            $data['pic']=$pic;
            $data['addtime']=time();

            $db = DB::table('lunbo')->where('id','=',$id)->update($data);
            if($db){
                    unlink('./uploads/lunbo/'.$first->pic);
                    return redirect('/adlunbo')->with('success','修改成功');
                }else{
                    return back()->with('error','修改失败');
                }
        }else{
            $data['pic']=$first->pic;
            $data['addtime']=time();
            $db = DB::table('lunbo')->where('id','=',$id)->update($data);
            if($db){
                return redirect('/adlunbo')->with('success','修改成功');
            }else{
                return back()->with('error','修改失败');
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //执行删除
        $db = DB::table("lunbo")->where('id','=',$id)->first();

        $db2 = DB::table("lunbo")->where('id','=',$id)->delete();
        if($db2){
            unlink('./uploads/lunbo/'.$db->pic);
            return redirect('/adlunbo')->with('success'.'删除成功');
        }else{
            return back()->with('error','删除失败');
        }
    }
}
