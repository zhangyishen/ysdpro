<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class TieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //加载模板
        $tiezi = DB::table('tiezi')->get();
        return view('Admin.tiezi.index',['tiezi'=>$tiezi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //加载添加类型模板
        return view('Admin.tiezi.add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       $data = $request->except('_token');
       $db = DB::table('tietype')->insert($data);
       if($db){
            return redirect('/tiezi')->with('success','添加成功');
        }else{
            return back()->with('error','添加失败');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('tietype')->where('id','=',$id)->first();
        return view("Admin.tiezi.edit",['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // echo "up";
        $data = $request->except('_token','_method');
        // dd($data);
        $db = DB::table('tietype')->where('id','=',$id)->update($data);
        if($db){
            return redirect('/tiezitype')->with('success','修改成功');
        }else{
            return back()->with('error','修改失败');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // echo 1;
        $tiezi = DB::table('tiezi')->where('id','=',$id)->delete();
        $erhui = DB::table('erhui')->where('tie_id','=',$id)->delete();
        $huifu = DB::table('huifu')->where('tie_id','=',$id)->delete();
        if($tiezi && $erhui && $huifu){
            return redirect('/tiezi')->with('success','删除成功');
        }else{
            return back()->with('error','删除失败');
        }
    }

    public function tietype(){
        $data = DB::table('tietype')->get();
        return view('Admin.tiezi.type',['data'=>$data]);
    }

    public function tiezidel($id){
        // echo "1";
        $db = DB::table('tietype')->where('where','=',$id)->delete();
        if($db){
            return redirect('/tiezitype')->with('success','删除成功');
        }else{
            return back()->with('error','删除失败');
        }
    }
}
