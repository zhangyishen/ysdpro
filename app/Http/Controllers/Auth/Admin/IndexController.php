<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;//引入数据库方法
class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // dd(session('nodelist'));
       $data = DB::table('user')->count();

       $data1 = DB::table('user')->where('status','=',0)->count();
       $data2 = DB::table('user')->where('status','=',1)->count();
       $data3 = DB::table('user')->where('vipstatus','=',1)->count();

       $aduser = DB::table('admin_users')->count();
       $video = DB::table('video')->count();
       $node = DB::table('node')->count();
       $order= DB::table('order')->count();
       $price = DB::table('order')->where('status','=','1')->sum('price');
       // dd($price);
       $numprice = DB::table('order')->where('status','=',0)->get();
       // dd($video);
        return view("Admin.index",['data'=>$data,'data1'=>$data1,'data2'=>$data2,'video'=>$video,'data3'=>$data3,'aduser'=>$aduser,'node'=>$node,'order'=>$order,'price'=>$price]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
