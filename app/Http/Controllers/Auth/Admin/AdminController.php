<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;//导入数据库方法
use Hash;
use App\Http\requests\AdminUsersinsert;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //管理员列表页
        //获取管理员列表数据
        $aduser=DB::table('admin_users')->paginate(10);
        return view('Admin.admin.index',['aduser'=>$aduser]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //添加管理员
        return view('Admin.admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //执行管理员添加
        
        $data = $request->all();
            if($data['password']==$data['repassword']){
                $data1 = $request->except('_token','repassword');
                $data1['password']=Hash::make($data1['password']);
                // dd($data1);
                $db = DB::table('admin_users')->insert($data1);
                if($db){
                    return redirect('/adusers')->with('success','添加成功');
                }else{
                    return back()->with('error','添加失败');
                }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //执行添加
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //加载修改模板
        $edit = DB::table('admin_users')->where('id','=',$id)->first();
        // dd($edit);
        return view('Admin.admin.edit',['edit'=>$edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //执行修改
        $list=$request->except('_token','_method');
        // dd($list);
            $up=DB::table('admin_users')->where('id','=',$id)->update($list);
            if($up){
                return redirect('/adusers')->with('success','修改成功');
            }else{
                return back()->with('error','修改失败');
            }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //执行删除
        $del = DB::table('admin_users')->where('id','=',$id)->delete();
        // dd($del);
        if($del){
            return redirect('/adusers')->with('success','删除成功');
        }else{
            return back()->with('error','删除失败');
        }
    }


    /**
     * 
     */
    //分配角色
    public function role($id){
        //获取当前管理员的信息
        $user = DB::table("admin_users")->where('id','=',$id)->first();
        //获取所有的角色
        $role=DB::table("role")->get();
        //获取当前用户已有的角色信息
        $data=DB::table("user_role")->where('uid','=',$id)->get();
        // var_dump($data);
        if(count($data)){
            //当前用户有角色
            //遍历
            foreach($data as $v){
                $rids[]=$v->rid;
            }
            //加载模板
            return view("Admin.admin.role",['user'=>$user,'role'=>$role,'rids'=>$rids]);
        }else{
            //当前用户没有角色信息
            return view("Admin.admin.role",['user'=>$user,'role'=>$role,'rids'=>array()]);
        } 
    }

    /**
     * 
     */
    //保存角色
    public function saverole(Request $request){
        // echo "这是保存角色";
        //把表单的数据入库到 user_role
        // dd($request->all());
        // 角色id(数组)
        $rid = $_POST['rid'];
        //获取管理员用户id
        $uid=$request->input("uid");
        //把当前用户已有的角色删除
        DB::table("user_role")->where('uid','=',$uid)->delete();
        echo '<pre>';
        var_dump($rid);
        //遍历$rid
        foreach($rid as $key=>$v){
            //封装要插入的数据
            $data['uid']=$uid;
            $data['rid']=$v;
            //插入
            DB::table("user_role")->insert($data);
        }

        return redirect('./adusers')->with('success','角色分配成功');
    }


}
