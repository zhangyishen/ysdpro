<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //加载模板
        $k=$request->input('keywords');
        $video = DB::table('video')->where("name",'like',"%".$k."%")->paginate(10);
        return view('Admin.video.index',['video'=>$video]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        //加载模板
        //查询cate表所有顶级分类
        $cate = DB::table('cate')->where('pid','=','0')->get();

        $year = DB::table('year')->get();

        $country = DB::table('country')->get();
        // dd($cate->id);
        //查询顶级分类下子分类
        // $path = '0'.','.$v->id;
        // dd($path);
        
        // dd($cate2);
        return view("Admin.video.add",['cate'=>$cate,'year'=>$year,'country'=>$country]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //执行添加
        $data = $request->except('_token');
        if($data['category']=='k' && $data['type']=='k' && $data['year']=='k' && $data['region']=='k'){
            return back()->with('error','请将所有选项选择完毕再进行添加');
        }else{
            // dd($request->hasFile('pic'));
            // 检测是否有文件上传
            if($request->hasFile('pic')){
                //初始化名字
                $name = time()+rand(1,99999);
                //获取文件上传后缀
                $ext=$request->file("pic")->getClientOriginalExtension();
                //设置文件名
                $request->file("pic")->move("./uploads/video",$name.".".$ext);

                $pic = $name.'.'.$ext;

                $data['pic'] = $pic;

                // dd($data);
                $db = DB::table('video')->insert($data);
                if($db){
                    return redirect('/advideo')->with('success','添加成功');
                }else{
                    return back()->with('error','添加失败');
                }
            }else{
                return back()->with('error','请上传视频封面图片');
            }   
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cate = DB::table('cate')->where('pid','=','0')->get();

        $year = DB::table('year')->get();

        $country = DB::table('country')->get();

        $data =DB::table('video')->where('id','=',$id)->first();
        //加载修改模板
        return view('Admin.video.edit',['cate'=>$cate,'year'=>$year,'country'=>$country,'data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //执行修改
        $db = DB::table("video")->where('id','=',$id)->first();

        $data = $request->except('_token','_method');
        // dd($data);
        if($data['category']=='k' && $data['type']=='k' && $data['year']=='k' && $data['region']=='k'){
            if($request->hasFile('pic')){
                //初始化名字
                $name = time()+rand(1,99999);
                //获取文件上传后缀
                $ext=$request->file("pic")->getClientOriginalExtension();
                //设置文件名
                $request->file("pic")->move("./uploads/video",$name.".".$ext);

                $pic = $name.'.'.$ext;

                $data['pic'] = $pic;

                $data['category'] = $db->category;
                $data['type'] = $db->type;
                $data['year'] = $db->year;
                $data['region'] = $db->region;
                // dd($data);
                $dd = DB::table('video')->where('id','=',$id)->update($data);
                if($dd){
                    unlink('./uploads/video/'.$db->pic);
                    return redirect('/advideo')->with('success','修改成功');
                }else{
                    return back()->with('error','修改成功');
                }
            }else{
                $data['category'] = $db->category;
                $data['type'] = $db->type;
                $data['year'] = $db->year;
                $data['region'] = $db->region;
                $data['pic'] = $db->pic;
                // dd($data);
                $dd = DB::table('video')->where('id','=',$id)->update($data);
                if($dd){
                    return redirect('/advideo')->with('success','修改成功');
                }else{
                    return back()->with('error','修改成功');
                }
            }
        }else{

            if($request->hasFile('pic')){
                //初始化名字
                $name = time()+rand(1,99999);
                //获取文件上传后缀
                $ext=$request->file("pic")->getClientOriginalExtension();
                //设置文件名
                $request->file("pic")->move("./uploads/video",$name.".".$ext);

                $pic = $name.'.'.$ext;

                $data['pic'] = $pic;

                $dd = DB::table('video')->where('id','=',$id)->update($data);
                if($dd){
                    unlink('./uploads/video/'.$db->pic);
                    return redirect('/advideo')->with('success','修改成功');
                }else{
                    return back()->with('error','修改成功');
                }
            }else{
                $data['pic'] = $db->pic;
                $dd = DB::table('video')->where('id','=',$id)->update($data);
                if($dd){
                    return redirect('/advideo')->with('success','修改成功');
                }else{
                    return back()->with('error','修改成功');
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //执行删除
        // echo "这是删除";
        // $db = ;
        // dd($db);
        $dd = DB::table('video')->where('id','=',$id)->first();
        if(DB::table('video')->where('id','=',$id)->delete()){
            unlink("./uploads/video/".$dd->pic);
            return redirect('/advideo')->with('success','删除成功');
        }else{
            return back()->with('error','删除失败');
        }
    }

    public function son_cate(Request $request){
        // echo $request->input('id');
        $id = $request->get('id');
        $db = DB::table('cate')->where('pid','=',$id)->get();
        $arr = [];
        foreach ($db as $key => $value) {
            $arr[$key]['id'] = $value->id;
            $arr[$key]['name'] = $value->name;
        }
        return json_encode($arr);
    }
    
}
