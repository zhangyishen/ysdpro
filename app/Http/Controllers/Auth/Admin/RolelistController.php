<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class RolelistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //获取角色信息
        $role=DB::table("role")->get();
        //加载角色列表模板
        return view("Admin.rolelist.index",['role'=>$role]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    //分配权限操作
    public function auth($id){
        //获取当前对应的角色信息
        $role=DB::table("role")->where("id",'=',$id)->first();
        //获取所有的权限
        $auth = DB::table("node")->get();
        //获取当前角色已有权限信息
        $data=DB::table('role_node')->where("rid",'=',$id)->get();
        //判断
        if(count($data)){
            //当前角色有权限信息
            //遍历
            foreach($data as $v){
                $nid[]=$v->nid;
            }
            // 加载模板
            return view("Admin.rolelist.auth",['role'=>$role,'auth'=>$auth,'nid'=>$nid]);
        }else{
            //当前角色没有权限信息
            // 加载模板
            return view("Admin.rolelist.auth",['role'=>$role,'auth'=>$auth,'nid'=>array()]);
        }
        // echo $id;
    }


    //保存权限
    public function saveauth(Request $request){
        // echo "这是保存权限";
        // 获取角色id
        $rid=$request->input('rid');
        //获取去分配的权限信息
        $nid=$_POST['nid'];
        //删除当前已有的权限信息
        DB::table("role_node")->where('rid','=',$rid)->delete();
        //遍历
        foreach($nid as $key=>$value){
            //封装需要插入数据
            $data['rid']=$rid;
            $data['nid']=$value;
            //插入
            DB::table('role_node')->insert($data);
        }

        return redirect('/rolelist')->with('success','权限分配成功');
    }
}
