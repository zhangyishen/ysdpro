<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;//引入数据库方法
use Hash;//导入hash类
use App\Http\Requests\AdminUserinsert;//
use App\Models\Users;//导入模型类
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //加载模板
        $k=$request->input('keywords');
        //查询数据库遍历数据
        $data = Users::where("user",'like',"%".$k."%")->paginate(6);
        return view('Admin.user.index',['data'=>$data,'request'=>$request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //添加用户
        return view('Admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminUserinsert $request)
    {  

    	
    	// dd($data);
    	//执行添加  
    	//判断是否 是post传值
    	if($request->isMethod('post')){

			// echo "1";
			$data = $request->except('_token','top_pic');
			// dd($data);
					// if(!$data['name'] == '' && !$data['user'] == '' && !$data['pass'] == '' && !$data['phone'] == '' && !$data['email'] == ''){
					// 判断是否有文件上传
					if($request->hasFile('top_pic')){
						//初始化名字
						$name = time()+rand(1,99999);
						//获取文件上传后缀
						$ext=$request->file("top_pic")->getClientOriginalExtension();
						//设置文件名
						$request->file("top_pic")->move("./uploads",$name.".".$ext);

						$picname = $name.".".$ext;

						$data = $request->except('_token');

						if($data['vipstatus']==0){
							$data['pass'] = Hash::make($data['pass']);
							$data['useradd_time']=time();
							$data['vipstart_time']=0;
							$data['vipstop_time']=0;
							// $data['collection_id']=0;
							// $data['status']=0;
							// $data['lavel']=1;
							$data['top_pic']=$picname;
							$data['_token']=MD5(mt_rand(1,999999));
							// dd($data);
							$db = DB::table('user')->insert($data);

							if($db){
								return redirect("/aduser")->with('success','添加成功'); //在跳转的时候 设置session信息
							}else{
								return back()->with('error','添加失败');
							}
						}else{
							$data['pass'] = Hash::make($data['pass']);
							$data['useradd_time']=time();
							$data['vipstart_time']=time();
							$data['vipstop_time']=time()+(50*54000);
							// $data['collection_id']=0;
							// $data['status']=0;
							// $data['lavel']=1;
							$data['top_pic']=$picname;
							$data['_token']=MD5(mt_rand(1,999999));
							// dd($data);
							$db = DB::table('user')->insert($data);
							if($db){
								return redirect("/aduser")->with('success','添加成功'); //在跳转的时候 设置session信息
							}else{
								return back()->with('error','添加失败');
							}


						}
					}else{
						$data = $request->except('_token');
						if($data['vipstatus']==0){
							$data['pass'] = Hash::make($data['pass']);
							$data['useradd_time']=time();
							$data['vipstart_time']=0;
							$data['vipstop_time']=0;
							// $data['collection_id']=0;
							// $data['status']=0;
							// $data['lavel']=1;
							$data['top_pic']='moren/moren.jpg';
							$data['_token']=MD5(mt_rand(1,999999));
							// dd($data);
							$db = DB::table('user')->insert($data);

							if($db){
								return redirect("/aduser")->with('success','添加成功'); //在跳转的时候 设置session信息
							}else{
								return back()->with('error','添加失败');
							}
						}else{
							$data['pass'] = Hash::make($data['pass']);
							$data['useradd_time']=time();
							$data['vipstart_time']=time();
							$data['vipstop_time']=time()+(50*54000);
							// $data['collection_id']=0;
							// $data['status']=0;
							// $data['lavel']=1;
							$data['top_pic']='moren/moren.jpg';
							$data['_token']=MD5(mt_rand(1,999999));
							// dd($data);
							$db = DB::table('user')->insert($data);
							if($db){
								return redirect("/aduser")->with('success','添加成功'); //在跳转的时候 设置session信息
							}else{
								return back()->with('error','添加失败');
							}


						}


					}//end


		    	//  }else{
		    	// 	return back()->with('error','不能为空');
		    	// }
    		
    	}//end
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //信息
        // echo $id;
        $data = DB::table('user')->where('id','=',$id)->first();
        // dd($data);
        return view('Admin.user.show',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //修改
        // echo $id;
        $data = DB::table('user')->where('id','=',$id)->first();
        // dd($data);
        return view('Admin.user.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //执行修改方法
        // echo $id;
        // $data = $request->all();
        // dd($data);
        $db = DB::table('user')->where('id','=',$id)->first();
        // dd($db->top_pic); 
        if($request->isMethod('put')){
        	
        	$data = $request->except('_token','_method');
        	// dd($data);
			//判断密码是否修改
			if($data['pass']==$db->pass){
				if($request->hasFile('top_pic')){
					//初始化名字
					$name = time()+rand(1,99999);
					//获取文件上传后缀
					$ext=$request->file("top_pic")->getClientOriginalExtension();
					//设置文件名
					$request->file("top_pic")->move("./uploads",$name.".".$ext);

					$picname = $name.".".$ext;

					

						if($data['vipstatus']==1){
							if(!empty($db->vipstart_time) && !empty($db->vipstop_time)){
								$data['vipstart_time']=$db->vipstart_time;
								$data['vipstop_time']=$db->vipstop_time;
								$data['top_pic']=$picname;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								// dd($data);
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
							}else{
								$data['vipstart_time']=time();
								$data['vipstop_time']=time()+(50*54000);
								$data['top_pic']=$picname;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
								// $data['a']=1;
								// dd($data);
							}
						}else{
							$data['top_pic']=$picname;
							$data['vipstart_time']=$db->vipstart_time;
							$data['vipstop_time']=$db->vipstop_time;
							// $data['lavel']=$db->lavel;
							$data['useradd_time']=$db->useradd_time;
							$data['_token']=$db->_token;
							// $data['collection_id']=$db->collection_id;
							// $data['a']=2;
							// dd($data);
							$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
						}
					}else{
						if($data['vipstatus']==1){
							if(!empty($db->vipstart_time) && !empty($db->vipstop_time)){
								$data['vipstart_time']=$db->vipstart_time;
								$data['vipstop_time']=$db->vipstop_time;
								$data['top_pic']=$db->top_pic;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								// dd($data);
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
							}else{
								$data['vipstart_time']=time();
								$data['vipstop_time']=time()+(50*54000);
								$data['top_pic']=$db->top_pic;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								// $data['a']=1;
								// dd($data);
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
							}
						}else{
							$data['top_pic']=$db->top_pic;
							$data['vipstart_time']=$db->vipstart_time;
							$data['vipstop_time']=$db->vipstop_time;
							// $data['lavel']=$db->lavel;
							$data['useradd_time']=$db->useradd_time;
							$data['_token']=$db->_token;
							// $data['collection_id']=$db->collection_id;
							// $data['a']=2;
							// dd($data);
							$db = DB::table('user')->where('id','=',$id)->update($data);
							if($db){
								return redirect('/aduser')->with('success','修改成功');
							}else{
								return back()->with('error','修改失败');
							}
						}
					}
        		}else{
        			if($request->hasFile('top_pic')){
					//初始化名字
					$name = time()+rand(1,99999);
					//获取文件上传后缀
					$ext=$request->file("top_pic")->getClientOriginalExtension();
					//设置文件名
					$request->file("top_pic")->move("./uploads",$name.".".$ext);

					$picname = $name.".".$ext;

					

						if($data['vipstatus']==1){
							if(!empty($db->vipstart_time) && !empty($db->vipstop_time)){
								$data['pass'] = Hash::make($data['pass']);
								$data['vipstart_time']=$db->vipstart_time;
								$data['vipstop_time']=$db->vipstop_time;
								$data['top_pic']=$picname;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
								// dd($data);
							}else{
								$data['pass'] = Hash::make($data['pass']);
								$data['vipstart_time']=time();
								$data['vipstop_time']=time()+(50*54000);
								$data['top_pic']=$picname;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}

								// $data['a']=1;
								// dd($data);
							}
						}else{
							$data['pass'] = Hash::make($data['pass']);
							$data['top_pic']=$picname;
							$data['vipstart_time']=$db->vipstart_time;
							$data['vipstop_time']=$db->vipstop_time;
							// $data['lavel']=$db->lavel;
							$data['useradd_time']=$db->useradd_time;
							$data['_token']=$db->_token;
							// $data['collection_id']=$db->collection_id;
							$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
							// $data['a']=2;
							// dd($data);
						}
					}else{
						if($data['vipstatus']==1){
							if(!empty($db->vipstart_time) && !empty($db->vipstop_time)){
								$data['pass'] = Hash::make($data['pass']);
								$data['vipstart_time']=$db->vipstart_time;
								$data['vipstop_time']=$db->vipstop_time;
								$data['top_pic']=$db->top_pic;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
								// dd($data);
							}else{
								$data['pass'] = Hash::make($data['pass']);
								$data['vipstart_time']=time();
								$data['vipstop_time']=time()+(50*54000);
								$data['top_pic']=$db->top_pic;
								// $data['lavel']=$db->lavel;
								$data['useradd_time']=$db->useradd_time;
								$data['_token']=$db->_token;
								// $data['collection_id']=$db->collection_id;
								$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
								// $data['a']=1;
								// dd($data);
							}
						}else{
							$data['pass'] = Hash::make($data['pass']);
							$data['top_pic']=$db->top_pic;
							$data['vipstart_time']=$db->vipstart_time;
							$data['vipstop_time']=$db->vipstop_time;
							// $data['lavel']=$db->lavel;
							$data['useradd_time']=$db->useradd_time;
							$data['_token']=$db->_token;
							// $data['collection_id']=$db->collection_id;
							$db = DB::table('user')->where('id','=',$id)->update($data);
								if($db){
									return redirect('/aduser')->with('success','修改成功');
								}else{
									return back()->with('error','修改失败');
								}
							// $data['a']=2;
							// dd($data);
						}
					}
       		}
    	}
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //删除方法
        // echo $id;
       	$dbpic = DB::table('user')->where('id','=',$id)->first();
       	$pic = $dbpic->top_pic;

       	if($pic=='moren/moren.jpg'){
	       $db = DB::table('user')->where('id','=',$id)->delete();
	       if($db){
	       		return redirect("/aduser")->with('success','添加成功');
	       }else{
	       		return back()->with('error','添加失败');
	       }
	   }else{
	   		unlink('./uploads/'.$pic);
	   		$db = DB::table('user')->where('id','=',$id)->delete();
	       if($db){
	       		return redirect("/aduser")->with('success','添加成功');
	       }else{
	       		return back()->with('error','添加失败');
	       }
	   }

     
    }



}
