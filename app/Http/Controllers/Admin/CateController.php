<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class CateController extends Controller
{
    public function getcates(){
        //获取列表数据
        //连贯方法结合原始表达式 防止sql语句注入
        $cate = DB::table("cate")->select(DB::raw("*,concat(path,',',id) as paths"))->orderBy('paths')->get();
        //遍历
        foreach($cate as $key=>$value){
            //转换为数组
            $arr=explode(",",$value->path);
            //获取逗号个数
            $len=count($arr)-1;
            //字符串重复函数
            $cate[$key]->name=str_repeat("--|",$len).$value->name;
        }
        return $cate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //连贯方法结合原始表达式 防止sql语句注入
        $cate=DB::table("cate")->select(DB::raw('*,concat(path,",",id) as paths'))->where('name','like',"%".$request->input("keywords")."%")->orderBy('paths')->paginate(6);
        //遍历
        foreach($cate as $key=>$value){
            //转换为数组
            $arr=explode(",",$value->path);
            //获取逗号个数
            $len=count($arr)-1;
            //字符串重复函数
            $cate[$key]->name=str_repeat("--|",$len).$value->name;
        }
        //加载模板分配数据
        return view("Admin.cate.index",['cate'=>$cate,'request'=>$request->all()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //获取所有分类信息
        $cate=$this->getcates();
        //加载添加模板
        return view("Admin.cate.add",['cate'=>$cate]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //获取需要添加数据
        $data=$request->only(['name','pid']);
        //获取pid
        $pid=$request->input("pid");
        //添加顶级分类信息
        if($pid==0){
            //拼接path
            $data['path']='0';
            // if(empty($data['luyou'])){
            //     return back()->with('error','顶级分类路由不能为空');
            // }
        }else{
            //添加子类信息
            //获取父类信息
            // $data['luyou']='0';
            $info=DB::table("cate")->where("id",'=',$pid)->first();
            //拼接path
            $data['path']=$info->path.",".$info->id;
        }
        // dd($data);

        //执行数据库插入
        // dd($data);
        if(DB::table("cate")->insert($data)){
            return redirect('/adcate')->with('success','分类添加成功');
        }else{
            return back()->with("error",'分类添加失败');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //加载模板
        $cate=$this->getcates();
        $data = DB::table('cate')->where('id','=',$id)->first();
        return view('Admin.cate.edit',['data'=>$data,'cate'=>$cate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //获取需要修改数据
        $data=$request->only(['name','pid']);
        // dd($data);
        //获取pid
        $pid=$request->input("pid");
        //添加顶级分类信息
        if($pid==0){
            //拼接path
            $data['path']='0';
            $aa = DB::table('cate')->where('id','=',$id)->update($data);
            if($aa){
                return redirect('/adcate')->with('success','修改成功');
            }else{
                return back()->with('error','修改失败');
            }
        }else{
            //添加子类信息
            //获取父类信息
            $info=DB::table("cate")->where("id",'=',$pid)->first();
            //拼接path
            $data['path']=$info->path.",".$info->id;
            $bb = DB::table('cate')->where('id','=',$id)->update($data);
            if($bb){
                return redirect('/adcate')->with('success','修改成功');
            }else{
                return back()->with('error','修改失败');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //获取id
        //获取删除分类的子类个数
        $res=DB::table("cate")->where('pid','=',$id)->count();
        //判断
        if($res>0){
            return back()->with('error','请先删除子类');
        }

        //直接删除当前分类信息
        if(DB::table('cate')->where("id",'=',$id)->delete()){
            return redirect("/adcate")->with('success','删除成功');
        }else{
            return redirect("/adcate")->with("error",'删除失败');
        }
    }
}
