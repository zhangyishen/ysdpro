<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //退出
        //销毁session模块
        $request->session()->pull('id');
        $request->session()->pull('name');
        $request->session()->pull('nodelist');
        return redirect('/adlogin/create');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('Admin.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //获取登录信息
        // $data = $request->all();
        //获取用户名
        $username=$request->input('username');
        $password=$request->input('password');
        //检测名字
        $info=DB::table('admin_users')->where('username','=',$username)->first();

        //判断
        if($info){
            // echo "ok";
            //检测密码
            if(Hash::check($password,$info->password)){
                //把登录信息写入session
                session(['name'=>$username]);
                //1.获取登录用户的所有权限列表
                $list=DB::select("select n.name,n.mname,n.aname from user_role as ur,role_node as rn,node as n where ur.rid=rn.rid and rn.nid=n.id and uid={$info->id}");
                // dd($list);

                // echo '<pre>';
                // var_dump($list);exit;
                //2.初始化权限列表
                //写入所有管理员公共模块->后台首页
                $nodelist['IndexController'][]="index";
                //遍历
                foreach($list as $v){
                    //把所有权限写入到$nodelist数组里
                    $nodelist[$v->mname][]=$v->aname;
                    //判断如果有create方法 添加store
                    if($v->aname=="create"){
                        $nodelist[$v->mname][]="store";
                    }

                    //判断 如果有edit方法 添加update
                    if($v->aname=="edit"){
                        $nodelist[$v->mname][]="update";
                    }

                }
                // echo '<pre>';
                // var_dump($nodelist);exit;
                //3.将登录用户的权限存在session里
                session(['nodelist'=>$nodelist]);
               
                
                return redirect('/ad')->with('success','登录成功');
            }else{
                return back()->with('error','密码有误');
            }
            
        }else{
            // echo 'error';
            return back()->with('error','管理员不存在');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
