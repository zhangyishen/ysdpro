<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //加载模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view("Home.user.index",['shou'=>$shou]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //加载模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view("Home.user.edit",['shou'=>$shou]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //执行修改
        $data = $request->except('_token','_method');
        // dd($data);
        $db = DB::table('user')->where('id','=',$id)->update($data);
        // dd($db);
        
        if($db){
            $user = DB::table('user')->where('id','=',$id)->first();
            session(['user'=>$user]);
            return redirect('/user')->with('success','修改成功');
        }else{
            return back()->with('error','修改失败');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function useredit(Request $request,$id){
        // dd($id);
        $data = DB::table('user')->where('id','=',$id)->first();
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view("Home.user.edit",['shou'=>$shou,'data'=>$data]);
    }


    public function pay(Request $request,$id){
        // echo "pay";
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $meal = DB::table('vipmeal')->get();
        // dd($meal);
        return view("Home.user.pay",['shou'=>$shou,'meal'=>$meal]);
    }

    public function payed(Request $request){
        $pay = $request->except('_token');
        // dd($pay);
        $aa =  array_keys($pay);
        $cc = explode(',',$aa[0]);
        $bb =  array_values($pay);
        $dd = explode(',',$bb[0]);
        $arr = array($cc[0]=>$dd[0],$cc[1]=>$dd[1],$cc[2]=>$dd[2]);
        // dd($arr);        
        // dd($cc);
        // $aa = time().rand(1,999999);
        $order = MD5(time().rand(1,999999));
        $payfu = $pay['type'];
        $aname = $arr['name'];
        $price = $arr['price'];
        // dd();
        $data['order_id']=$order;
        $data['user_id']=session('user')->id;
        $data['user_user']=session('user')->name;
        $data['setmeal']=$aname;
        $data['addtime']=time();
        $data['status']=0;
        $data['price']=$price;
        $data['day']=$arr['time'];
        // $data['stoptime']=time()+900;
        $db = DB::table('order')->insert($data);
        // (time()+30*(3600*24));
        // dd($db);
        $id = session('user')->id;
        session(['id'=>$id]);
        pay($order,$payfu,$aname,$price);
    }

     //通知界面
    public function returnurl(Request $request){
            // echo "支付成功";
            $id = session('id');
            $order_id = $_GET['out_trade_no'];
            $db = DB::table('order')->where('order_id','=',$order_id)->first();
            // echo date('Y-m-d H:i:s',$db->stoptime);
            // $db->status = 1;
            // dd($db);
            $data['order_id']=$order_id;
            $data['user_id']=$db->user_id;
            $data['user_user']=$db->user_user;
            $data['setmeal']=$db->setmeal;
            $data['addtime']=$db->addtime;
            $data['status']=1;
            $data['price']=$db->price;
            $up = DB::table('order')->where('order_id','=',$order_id)->update($data);
            if($up){
                $ua = DB::table('user')->where('id','=',$id)->first();
                $vip['vipstatus']=1;
                if(empty($ua->vipstatus) || date("Y-m-d",$ua->vipstop_time)<date("Y-m-d",time())){
                    $vip['vipstart_time']=time();
                    $vip['vipstop_time']=time()+($db->day*(3600*24));
                }else{
                    $vip['vipstart_time']=$ua->vipstart_time;
                    $vip['vipstop_time']=$ua->vipstop_time+($db->day*(3600*24));
                }
                $upu = DB::table('user')->where('id','=',$id)->update($vip);
                if($upu){
                    $user = DB::table('user')->where('id','=',$id)->first();
                    session(['user'=>$user]);
                    $request->session()->pull('id');
                    return redirect('/')->with('success','购买成功');exit;
                }
                $user = DB::table('user')->where('id','=',$id)->first();
                session(['user'=>$user]);
                return redirect('/')->with('success','购买成功');exit;
            }else{
                return redirect('/')->with('error','购买失败');
            }
    }

    public function order(){

        $id = session('user')->id;

        $shou = DB::table('cate')->where('pid','=',0)->get();
        $data = DB::table('order')->where('user_id','=',$id)->get();
        return view('Home.user.order',['shou'=>$shou,'data'=>$data]);
    }


    public function shoucang(){
        $id = session('user')->id;
        $shou = DB::table('cate')->where('pid','=',0)->get();

        $ss = DB::table('shoucang') ->join('video','video.id','=','shoucang.vod_id') ->select('shoucang.id as sid','shoucang.vod_id as void','shoucang.usr_id as usid','shoucang.addtime as atime','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('usr_id','=',$id)->get();
        // dd($ss);
        return view('Home.user.shoucang',['shou'=>$shou,'ss'=>$ss]);
    }

    public function cang(Request $request){
        if(empty(session('user'))){
            echo "4";
        }else{
            $id = $request->input('id');
            $aa = DB::table('shoucang')->where('vod_id','=',$id)->first();
            if($aa){
                echo "3";
            }else{
                $data['vod_id']=$id;
                $data['usr_id']=session('user')->id;
                $data['addtime']=time();
                $db = DB::table('shoucang')->insert($data);
                if($db){
                    echo "1";
                }else{
                    echo "2";
                }
            }
        }
    }



    public function delcang(Request $request,$id){
        // echo $id;
        $del = DB::table('shoucang')->where('id','=',$id)->delete();
        if($del){
            return redirect('/shoucang')->with('success','删除成功');
        }else{
            return back()->with("error",'删除失败');
        }
    }

    public function quandel(Request $request,$id){
        echo $id;
        $quan = DB::table('shoucang')->where('usr_id','=',$id)->delete();
        if($quan){
            return redirect('/shoucang')->with('success','删除成功');
        }else{
            return back()->with("error",'删除失败');
        }
    }

    public function wenzhang($id){
        $data = DB::table('tiezi')->where('uid','=',$id)->get();
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view('Home.user.wen',['shou'=>$shou,'data'=>$data]);
    }
}
