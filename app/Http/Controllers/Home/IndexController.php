<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class IndexController extends Controller
{
    public function home(){
        // dd(session('user')->name);
        //加载首页模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        // dd($shou);
        $img = DB::table('lunbo')->get();

        $count = DB::table('lunbo')->count();

        $gonggao= DB::table('gonggao')->orderBy('id', 'desc')->first();

        $video = DB::table('video')->count();
        $vi=DB::select('SELECT * FROM video ORDER BY RAND() LIMIT 4');

        // $ss = DB::table('video') ->join('cate','video.category','=','cate.id')->join('country','video.region','=','country.id')->join('year','video.year','=','year.id') ->select('cate.id as cid','cate.name as cname','country.id as coid','country.name as coname','country.status as coust','year.id as yid','year.name as yname','year.status as ysta','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->get();
        // $ss = DB::table('video')->where('category','=','1')->orderBy('id', 'desc','limt 8')->get();
        // $ss = DB::table('video') ->join('year','video.year','=','year.id') ->select('year.id as yid','year.name as yname','year.status as ysta','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('category','=','1')->orderBy('vid', 'desc')->limit(8)->get();
        $ss = DB::table('video')->where('category','=','1')->orderBy('id', 'desc')->limit(8)->get();
        $ss1 = DB::table('video')->where('category','=','5')->orderBy('id', 'desc')->limit(5)->get();
        $ss2 = DB::table('video')->where('category','=','15')->orderBy('id', 'desc')->limit(5)->get();
        $ss3 = DB::table('video')->where('category','=','19')->orderBy('id', 'desc')->limit(5)->get();
        // $i = 1;
        // dd($ss);
        $paihang= DB::table('video')->orderBy('id', 'desc')->limit(10)->get();
        $paihang2= DB::table('video')->orderBy('id', 'asc')->limit(10)->get();
        // var_dump($gonggao);exit;
        return view('Home.index.index',['img'=>$img,'count'=>$count,'gonggao'=>$gonggao,'vi'=>$vi,'ss'=>$ss,'ss1'=>$ss1,'ss2'=>$ss2,'ss3'=>$ss3,'shou'=>$shou,'paihang'=>$paihang,'paihang2'=>$paihang2]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //加载信息模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $data =DB::table('video')->where('id','=',$id)->first();
        // dd($data->id);
        // foreach ($data as $key => $value) {
        //     $data[$key]=$data->$key;
        // }
        // dd($data);
        $paihang= DB::table('video')->orderBy('id', 'desc')->limit(10)->get();
        $paihang2= DB::table('video')->orderBy('id', 'asc')->limit(10)->get();
        return view('Home.index.show',['data'=>$data,'shou'=>$shou,'paihang'=>$paihang,'paihang2'=>$paihang2]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function bofang($id){
        //加载模板
        // echo '1';
        if(empty(session('user'))){
            return redirect('/login/create')->with('error','请先登录');
        }else{
            $paihang2= DB::table('video')->orderBy('id', 'asc')->limit(10)->get();
        $vi=DB::select('SELECT * FROM video ORDER BY RAND() LIMIT 4');
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $data = DB::table('video')->where('id','=',$id)->first();
        // dd($data);
       return view('Home.index.bofang',['data'=>$data,'shou'=>$shou,'vi'=>$vi,'paihang2'=>$paihang2]);
        }
        
    }


    public function vipget(Request $request){
        // echo "111";
        if(empty(session('user'))){
            echo "4";
        }else{
            $user_id = session('user')->id;
            $video_id = $request->get('id');
            // echo $video_id;
            $vip = DB::table('user')->where('id','=',$user_id)->first();
            if($vip->vipstatus==1){
                echo "vip";
            }else{

                $num = DB::table('vipget')->where('user_id','=',$user_id)->count();
                // dd($num);
                if($num < 5){
                    $data['user_id']=$user_id;
                    $data['video_id']=$video_id;
                    DB::table('vipget')->insert($data);
                    $aa = 5-$num;
                    echo $aa;
                }elseif($num == 5){
                    echo "0";
                }
            }
            

        }
        
    }
}
