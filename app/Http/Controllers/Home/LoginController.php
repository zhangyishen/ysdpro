<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Hash;
use Mail;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //注销登录
        // echo "1111";
        $request->session()->pull('user');
        return redirect('/login/create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //加载模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view('Home.login.login',['shou'=>$shou]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //对比登录信息
        $rr = $request->except('_token','code');
        // dd($data);
        $db = DB::table('user')->where('user','=',$rr['name'])->first();
        $db2 = DB::table('user')->where('email','=',$rr['name'])->first();
        $db3 = DB::table('user')->where('phone','=',$rr['name'])->first();
        // dd($db);
        if($db){

            if(Hash::check($rr['pass'],$db->pass)){

                if($db->status==0){

                    if(date("Y-m-d",$db->vipstop_time)<date("Y-m-d",time())){

                        $data['vipstatus']=0;
                        $data['vipstart_time']=0;
                        $data['vipstop_time']=0;
                        $up = DB::table('user')->where('id','=',$db->id)->update($data);
                        $aa = DB::table('user')->where('id','=',$db->id)->first();

                        // dd($data);
                        session(['user'=>$aa]);
                    }else{
                        session(['user'=>$db]);
                    }
                    return redirect('/');

                }else{

                    return back()->with('error','您的账号被冻结请联系站长');
                }

            }else{
                    return back()->with('error','密码不对请重新登录');
                }
            // echo '1';
        }elseif($db2){
            if(Hash::check($rr['pass'],$db2->pass)){
                 if($db2->status==0){
                    if(date("Y-m-d",$db2->vipstop_time)<date("Y-m-d",time())){

                        $data['vipstatus']=0;
                        $data['vipstart_time']=0;
                        $data['vipstop_time']=0;

                        // dd($data);
                        $up = DB::table('user')->where('id','=',$db2->id)->update($data);
                        $aa = DB::table('user')->where('id','=',$db2->id)->first();
                        session(['user'=>$aa]);
                    }else{
                        session(['user'=>$db2]);
                    }
                    // session(['user'=>$db2]);
                    return redirect('/');
                }else{
                    return back()->with('error','您的账号被冻结请联系站长');
                }
            }else{
                    return back()->with('error','密码不对请重新登录');
                }
            // echo '2';
        }elseif($db3){
            // echo '3';
            if(Hash::check($rr['pass'],$db3->pass)){
                 if($db3->status==0){
                    if(date("Y-m-d",$db3->vipstop_time)<date("Y-m-d",time())){

                        $data['vipstatus']=0;
                        $data['vipstart_time']=0;
                        $data['vipstop_time']=0;
                        // dd($data);
                        $up = DB::table('user')->where('id','=',$db3->id)->update($data);
                        $aa = DB::table('user')->where('id','=',$db3->id)->first();
                        session(['user'=>$aa]);
                    }else{
                        session(['user'=>$db3]);
                    }
                    // session(['user'=>$db3]);
                    return redirect('/');
                }else{
                    return back()->with('error','您的账号被冻结请联系站长');
                }
            }else{
                    return back()->with('error','密码不对请重新登录');
                }
        }else{
            return back()->with('error','抱歉不存在此用户');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
