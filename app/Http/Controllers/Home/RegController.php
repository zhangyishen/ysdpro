<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//导入mail
use Mail;
use DB;
use Hash;
use A;
class RegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // echo "111111";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //加载模板
        return view("Home.reg.add");
    }

    public function checkphone(Request $request)
    {
        $p=$request->input('p');
        // echo $p;
        $phone = DB::table('user')->pluck('phone');
        $arr = array();
        //获取对象集合 转换为数组
        foreach($phone as $key=>$v){
            $arr[$key]=$v;
        }

        // echo json_encode($arr);
        //对比
        if(in_array($p,$arr)){
            echo 1;//手机号已经注册
        }else{
            echo 0;//手机号可以使用
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //用户注册
        $user = $request->except('_token','code','repass');
        // dd($user);
        $user['pass']=Hash::make($user['pass']);
        $user['name']='YSD用户_'.rand(1,9999);
        $user['email']='email@ysd.com';
        $user['status']=0;
        $user['top_pic']='moren/moren.jpg';
        $user['useradd_time']=time();
        $user['vipstatus']=0;
        $user['vipstart_time']=0;
        $user['vipstop_time']=0;
        $user['content']='他/她很懒.....什么也没留下';
        $user['_token']=MD5(mt_rand(1,999999));

        // dd($user);
        $db = DB::table('user')->insert($user);
        if($db){
            return redirect('/login/create')->with('success','注册成功');
        }else{
            return back()->with('error','注册失败');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //调用自定义函数
    public function sendphone(Request $request){
        // pay();
        // 实例化类
        // $ceshi = new A();
        // $ceshi->sendmessage();
        $phone = $request->input('pp');
        //调用短信接口
        sendphone($phone);
        // echo $phone;

    }

    public function checkcode(Request $request){
        //获取输入的校验码
        $code = $request->input('code');
        if(isset($_COOKIE['fcode']) && !empty($code)){
            //获取手机号接收到的验证码
            $fcode=$request->cookie('fcode');
            if($fcode==$code){
                echo 1;//校验码一直
            }else{
                echo 2;//校验码不一致
            }
        }elseif(empty($code)){
            echo 3;//输入的校验码为空
        }else{
            echo 4;//校验码过期
        }

    }



    public function checkmima(Request $request)
    {   
        //获取附加参数(注册的密码)
        $mm=$request->input('mm');
        // echo $mm;
        //获取附加参数(注册的重复密码)
        $cfm=$request->input('cfm');
        // echo $cfm;
        //对比
        if($mm=='' || $cfm==''){            
            echo 2;//密码为空
        }elseif($mm==$cfm){            
            echo 1;//密码一致
        }else{
            echo 0;//密码不一致
           
        }
    }



    public function checkuser(Request $request){
        $user = $request->input('user');

        $uu = DB::table('user')->pluck('user');
        // dd($uu);
        $arr = array();
        //获取对象集合 转换为数组
        foreach($uu as $key=>$v){
            $arr[$key]=$v;
        }

        // echo json_encode($arr);
        //对比
        if(in_array($user,$arr)){
            echo 1;//已存在用户名
        }else{
            echo 0;//用户名可以使用
        }
        
    }

}
