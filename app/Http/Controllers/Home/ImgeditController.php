<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ImgeditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $id;exit;
        $db = DB::table('user')->where('id','=',$id)->first();
        $pic = $db->top_pic;
        // echo "1";
        //执行修改
        // dd($request->hasFile('top_pic'));
        // var_dump($_FILES);exit;
        if($request->hasFile('top_pic')){
                    //初始化名字
                    $name = time()+rand(1,99999);
                    //获取文件上传后缀
                    $ext=$request->file("top_pic")->getClientOriginalExtension();
                    //设置文件名
                    $request->file("top_pic")->move("./uploads",$name.".".$ext);

                    $picname = $name.".".$ext;
                    // dd($picname);
                    $data['top_pic']=$picname;
                    if($pic=='moren/moren.jpg'){
                        $dd = DB::table('user')->where('id','=',$id)->update($data);
                        $user = DB::table('user')->where('id','=',$id)->first();
                        $request->session()->pull('user');
                        session(['user'=>$user]);
                        if($dd){
                            return redirect('/user')->with('success','修改成功');
                        }else{
                            return back()->with('error','修改失败');
                        }
                    }else{
                        unlink("./uploads/$pic");
                        $dd = DB::table('user')->where('id','=',$id)->update($data);
                        $user = DB::table('user')->where('id','=',$id)->first();
                        $request->session()->pull('user');
                        session(['user'=>$user]);
                        if($dd){
                            return redirect('/user')->with('success','修改成功');
                        }else{
                            return back()->with('error','修改失败');
                        }
                    }
                    
        }else{
            return back()->with('error','请上传头像');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function doedit($id){
             // echo "1";
        // 加载模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view('Home.img.edit',['shou'=>$shou]);
    }
}
