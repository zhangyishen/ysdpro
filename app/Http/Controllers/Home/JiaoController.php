<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class JiaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //加载模板
        // echo "1111";
        if(empty(session('user'))){
            echo "<script>alert('请先登录');location.href='/login/create';</script>";
        }else{
             $user= DB::table('user')->orderBy('id', 'desc')->limit(16)->get();
        // dd($user);
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $tie = DB::table('tiezi')->join('tietype','tiezi.type','=','tietype.id')->select('tiezi.id as tid','tiezi.title as ttitle','tiezi.content as tcontent','tiezi.uid as tuid','tiezi.uname as tuname','tiezi.u_pic as tu_pic','tiezi.addtime as taddtime','tiezi.type as ttype','tiezi.zan as tzan','tiezi.liulan as tliulan','tiezi.num as tnum','tietype.id as typeid','tietype.name as typename','tietype.status as typestatus')->get();
        // dd($tie);
        
        $xin= DB::table('tiezi')->orderBy('id', 'desc')->limit(8)->get();
        $tietype = DB::table('tietype')->where('status','=',0)->get();
        return view("Home.jiao.index",['shou'=>$shou,'user'=>$user,'tie'=>$tie,'xin'=>$xin,'tietype'=>$tietype]);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //加载模板
        // echo "1";
        $user= DB::table('user')->orderBy('id', 'desc')->limit(16)->get();
        $shou = DB::table('cate')->where('pid','=',0)->get();

        $tie = DB::table('tiezi')->get();
        $tietype = DB::table('tietype')->where('status','=',0)->get();
        return view('Home.jiao.add',['shou'=>$shou,'user'=>$user,'tie'=>$tie,'tietype'=>$tietype]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //执行添加
        // echo "1111";
        $data = $request->except('_token');
        if($data['title']=='' || $data['content']==''){
            return back()->with('error','标题和内容不能为空');
        }else{
            $data['uid']=session('user')->id;
            $data['uname']=session('user')->name;
            $data['u_pic']=session('user')->top_pic;
            $data['addtime']=time();
            $data['num']=0;
            $data['zan']=0;
            $data['liulan']=0;
            // dd($data);
            $db = DB::table('tiezi')->insert($data);
            // $id = DB::getPdo()->lastInsertId();
            // dd($id);
            if($db){
                return redirect('/jiaoliu')->with('success','发布成功');
            }else{
                return back()->with('error','发布失败');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // echo "1111";
        
        $data = DB::table('tiezi')->where('id','=',$id)->first();
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $num = DB::table('huifu')->where('tie_id','=',$id)->count();
        $huifu = DB::table('huifu')->where('tie_id','=',$id)->get();
        // dd($data);
        $erhui = DB::table('erhui')->get();
        return view("Home.jiao.show",['shou'=>$shou,'data'=>$data,'num'=>$num,'huifu'=>$huifu,'erhui'=>$erhui]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function huifu(Request $request){

       $data = $request->except('_token');

       // dd($data);
       if(empty(session('user'))){
            return back()->with('error','请先登录');
       }else{
           if($data['content']==null){
                return back()->with('error','回复不能为空');
           }else{
                $data['user_id']=session('user')->id;
                $data['user_pic']=session('user')->top_pic;
                $data['addtime']=time();
                $data['user_name']=session('user')->name;
                // $data['tie_id']=$data['id'];
                // dd($data);
                $db = DB::table('huifu')->insert($data);
                $aa = DB::table('tiezi')->where('id','=',$data['tie_id'])->first();
                $num['num'] = $aa->num+1;
                $cc = DB::table('tiezi')->where('id','=',$data['tie_id'])->update($num);

                if($db){
                    return redirect('/jiaoliu/'.$data['tie_id'])->with('success','评论成功');
                }else{
                    return back()->with('error','请先登录');
                }
           }
       }
    }


    public function gethui(Request $request){
        if(empty(session('user'))){
            echo 3;
        }else{
            $data = $request->all();

            // dd($data);
            $data['user_id']=session('user')->id;
            $data['user_pic']=session('user')->top_pic;
            $data['addtime']=time();
            $data['u_name']=session('user')->name;
            $db = DB::table('erhui')->insert($data);
            if($db){
                echo 1;
            }else{
                echo 2;
            }
        }
    }


    public function liulan(Request $request){
        $id = $request->get('id');
        // echo $data;
        $db = DB::table('tiezi')->where('id','=',$id)->first();

        $data['liulan']=$db->liulan+1;

        $up = DB::table('tiezi')->where('id','=',$id)->update($data);

        // dd($up);
        if($up){
            echo "1";
        }else{
            echo "2";
        }
    }

    public function dianzan(Request $request){
        // echo "1111";exit;
            $id = session('user')->id;
            session(['id'=>$id]);
            $tie = $request->get('id');
            $uid = session('id');
            // dd($uid);
                $kk = DB::table('zan')->where('uid','=',$uid)->where('tie','=',$tie)->first();
                // var_dump($kk);
                // dd(session('user'));
                if($kk==null){
                    $tt['uid']=null;
                    $tt['tie']=null;
                }else{
                    $tt['uid']=$kk->uid;
                    $tt['tie']=$kk->tie;
                }
                
                // dd($tt);
                if($uid == $tt['uid'] && $tie == $tt['tie']){
                    $del = DB::table('zan')->where('tie','=',$tie)->where('uid','=',$uid)->delete();
                    $nn = DB::table('tiezi')->where('id','=',$tie)->first();
                    if($nn->zan==0){
                        $zan['zan']=0;
                    }else{
                        $zan['zan']=$nn->zan-1;
                    }
                    $up1 = DB::table('tiezi')->where('id','=',$tie)->update($zan);
                    if($up1){
                        echo 3;
                    }
                }else{
                    // echo "22222";exit;
                    $data['uid'] = $uid;
                    $data['tie'] = $tie;
                    $data['status'] = 1;
                    $db = DB::table('zan')->insert($data);
                    // $id = DB::getPdo('zan')->lastInsertId();

                    $nn = DB::table('tiezi')->where('id','=',$tie)->first();
                    $zan['zan']=$nn->zan+1;
                    $up = DB::table('tiezi')->where('id','=',$tie)->update($zan);

                    // $cha = DB::table('zan')->where('tie','=',$tie)->first();
                    $cha = DB::table('zan')->where('tie','=',$tie)->where('uid','=',$uid)->first();

                    if($cha->uid == $uid && $cha->tie==$tie){
                        echo 1;
                    }else{
                        echo 2;
                    }
                }
                
        }


        public function chazan(Request $request){
            $tie = $request->get('id');
            $uid = session('user')->id;
            // $id = DB::getPdo('zan')->lastInsertId();

            // dd($id);
            $db = DB::table('zan')->where('tie','=',$tie)->where('uid','=',$uid)->first();
            // dd($db);
            if($db==null){
                    $tt['uid']=null;
                    $tt['tie']=null;
                }else{
                    $tt['uid']=$db->uid;
                    $tt['tie']=$db->tie;
                }
            if($tt['uid'] == $uid && $tt['tie'] == $tie){
                echo 1;
            }else{
                echo 2;
            }
        }

        public function jiaotype($id){
            $shou = DB::table('cate')->where('pid','=',0)->get();
            $name = DB::table('tietype')->where('id','=',$id)->first();
            $user= DB::table('user')->orderBy('id', 'desc')->limit(16)->get();

            // $data = DB::table('tiezi')->where('type','=',$id)->get();
            $tie = DB::table('tiezi')->join('tietype','tiezi.type','=','tietype.id')->select('tiezi.id as tid','tiezi.title as ttitle','tiezi.content as tcontent','tiezi.uid as tuid','tiezi.uname as tuname','tiezi.u_pic as tu_pic','tiezi.addtime as taddtime','tiezi.type as ttype','tiezi.zan as tzan','tiezi.liulan as tliulan','tiezi.num as tnum','tietype.id as typeid','tietype.name as typename','tietype.status as typestatus')->where('type','=',$id)->get();
            // dd($tie);
            $tietype = DB::table('tietype')->where('status','=',0)->get();
            $xin= DB::table('tiezi')->orderBy('id', 'desc')->limit(8)->get();
            return view("Home.jiao.type",['shou'=>$shou,'name'=>$name,'tie'=>$tie,'user'=>$user,'tietype'=>$tietype,'xin'=>$xin]);
        }

}
