<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class YingController extends Controller
{

    public function ying(){
         //加载模板
        // $ss = DB::table('video') ->join('cate','video.category','=','cate.id') ->select('cate.id as cid','cate.name as cname','cate.pid as cpid','cate.path as cpath','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('category','=','1')->paginate(20);
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $ss = DB::table('video')->where('category','=','1')->where('shangjia','=',0)->paginate(20);
        // dd($ss);
        return view("Home.yingshi.ying.index",['ss'=>$ss,'shou'=>$shou]);
    }

    public function ju(){
         //加载模板
        // $ss = DB::table('video') ->join('cate','video.category','=','cate.id') ->select('cate.id as cid','cate.name as cname','cate.pid as cpid','cate.path as cpath','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('category','=','1')->paginate(20);
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $ss = DB::table('video')->where('category','=','5')->where('shangjia','=',0)->paginate(20);
        // dd($ss);
        return view("Home.yingshi.ju.index",['ss'=>$ss,'shou'=>$shou]);
    }

    public function zong(){
         //加载模板
        // $ss = DB::table('video') ->join('cate','video.category','=','cate.id') ->select('cate.id as cid','cate.name as cname','cate.pid as cpid','cate.path as cpath','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('category','=','1')->paginate(20);
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $ss = DB::table('video')->where('category','=','19')->where('shangjia','=',0)->paginate(20);
        // dd($ss);
        return view("Home.yingshi.zong.index",['ss'=>$ss,'shou'=>$shou]);
    }

    public function man(){
         //加载模板
        // $ss = DB::table('video') ->join('cate','video.category','=','cate.id') ->select('cate.id as cid','cate.name as cname','cate.pid as cpid','cate.path as cpath','video.id as vid','video.name as vname','video.status as vsta','video.pic','video.director','video.to_star','video.video_address','video.jieshao','video.category','video.type','video.region','video.language','video.year','video.shangjia')->where('category','=','1')->paginate(20);
        $shou = DB::table('cate')->where('pid','=',0)->get();
        $ss = DB::table('video')->where('category','=','15')->where('shangjia','=',0)->paginate(20);
        // dd($ss);
        return view("Home.yingshi.man.index",['ss'=>$ss,'shou'=>$shou]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //加载模板
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function shaixuan($id){
        //筛选
        // echo $id;
        $year = DB::table('year')->get();
        $regoin = DB::table('country')->get();
        // dd($regoin);
        $data = DB::table('cate')->where('pid','=',$id)->get();
        $shou = DB::table('cate')->where('pid','=',0)->get();

        $video = DB::table('video')->where('category','=',$id)->get();
        // dd($video);
        return view("Home.yingshi.index",['shou'=>$shou,'year'=>$year,'regoin'=>$regoin,'data'=>$data,'video'=>$video,'id'=>$id]);
    }

    public function getvalue(Request $request){
        $id = $request->input('id');
        $year = $request->get('year');
        $region = $request->get('region');
        $type = $request->get('type');
        if(empty($year)){
            $year = '';
        }

        if(empty($region)){
            $region = '';
        }

        if(empty($type)){
            $type = '';
        }
            if(!empty($year)){
                $data = DB::table('video')->where('category','=',$id)->orWhere('year','=',$year)->get();
            }elseif(!empty($region)){
                $data = DB::table('video')->where('category','=',$id)->orWhere('region','=',$region)->get();
            }elseif(!empty($type)){
                $data = DB::table('video')->where('category','=',$id)->orWhere('type','=',$type)->get();
            }else{
                $data = DB::table('video')->where('category','=',$id)->get();
            }
            
            $arr = [];

            foreach ($data as $key => $value) {
            if($value->shangjia==0){
                $arr[$key]['id'] = $value->id;
                $arr[$key]['pic'] = $value->pic;
                $arr[$key]['name'] = $value->name;
                $arr[$key]['director'] = $value->director;
                $arr[$key]['to_star'] = $value->to_star;
                $arr[$key]['video_address'] = $value->video_address;
                $arr[$key]['jieshao'] = $value->jieshao;
                $arr[$key]['category'] = $value->category;
                $arr[$key]['type'] = $value->type;
                $arr[$key]['region'] = $value->region;
                $arr[$key]['language'] = $value->language;
                $arr[$key]['year'] = $value->year;
                $arr[$key]['status'] = $value->status;
                $arr[$key]['shangjia'] = $value->shangjia;
                }
            }
            return json_encode($arr);
    }  


    // public function getshai(Request $request){
    //    $year = $request->get('year');
    //    $id = $request->get('id');
    //    $region = $request->get('region');
    //    $type = $request->get('type');

    //    \Cookie::queue('year',$year,2);
    //    \Cookie::queue('id',$id,2);
    //    \Cookie::queue('region',$region,2);
    //    \Cookie::queue('type',$type,2);
    //    // echo $request->cookie('year');
    //    // echo $request->cookie('id');
    //    $data['id']=$request->cookie('id');
    //    $data['year']=$request->cookie('year');
    //    $data['region']=$request->cookie('region');
    //    $data['type']=$request->cookie('type');
    //    var_dump($data);
    // // echo "111111";
    // }
}
