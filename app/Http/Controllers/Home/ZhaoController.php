<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Gregwar\Captcha\CaptchaBuilder;
use Mail;
use Hash;
class ZhaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //找回密码
        //加载模板
        $shou = DB::table('cate')->where('pid','=',0)->get();
        return view('Home.zhao.add',['shou'=>$shou]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // echo "执行对比";
        $code = $request->input('code');
        if($code == session('vcode')){
            // echo "1";
            $data = $request->except('_token','code');
            // dd($data);
            $db = DB::table('user')->where('user','=',$data['user'])->first();
            if($db->email=='email@ysd.com'){
                return back()->with('error','默认邮箱无法发送邮件');
            }else{
                if($db->user==$data['user'] && $db->email==$data['email']){
                    $aa = $db->_token;
                    $this->sendmail($data['email'],$db->id,$aa);
                    return redirect('/zhao/create')->with('success','邮件发送成功');
                }else{
                    return back()->with('error','邮件发送失败');
                }
            }
            
        }else{
            return back()->with('error','验证码错误');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->except('_token','_method');
       // $data['id'] = $id;
       if($data['pass']==$data['repass']){
        $data = $request->except('_token','_method','repass');
            $data['_token']=MD5(mt_rand(1,999999));
            $data['pass']=Hash::make($data['pass']);
            // dd($data);
            $db = DB::table('user')->where('id','=',$id)->update($data);
            if($db){
                return redirect('/login/create')->with('success','请登录');
            }else{
                return back()->with('error','修改失败');
            }
       }else{
            return back()->with('error','重复密码不正确');
       }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function code(){
        //生成校验码代码
        ob_clean();//清除操作
        $builder = new CaptchaBuilder;
        //可以设置图片宽高及字体
        $builder->build($width = 100, $height = 40, $font = null);
        //获取验证码的内容
        $phrase = $builder->getPhrase();
        //把内容存入session
        session(['vcode'=>$phrase]);
        //生成图片
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-Type: image/jpeg');
        $builder->output();
        // die;

    }

    public function sendmail($email,$id,$_token){
        Mail::send('Home.zhao.a',['id'=>$id,'_token'=>$_token],function($message)use($email){
            //发送主题
            $message->subject('重置密码');
            //接收方
            $message->to($email);
        });
    }


    public function gaimi(Request $request){
        // echo "1111";
        $id = $request->get('id');
        // echo $id;
        $_token = $request->get('_token');
        $db = DB::table('user')->where('id','=',$id)->first();
        if($db->_token==$_token){
            return view('Home.zhao.gaimi',['id'=>$id,'_token'=>$_token]);
        }else{
            echo "此链接已失效";
        }
        
        
    }
}
