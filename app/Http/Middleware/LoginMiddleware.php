<?php

namespace App\Http\Middleware;

use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $request 请求报文
        //检测用户是否具有登录的session
        if($request->session()->has('name')){
            //获取访问模块控制器和方法名
            $actions=explode('\\', \Route::current()->getActionName());
            //或$actions=explode('\\', \Route::currentRouteAction());
            $modelName=$actions[count($actions)-2]=='Controllers'?null:$actions[count($actions)-2];
            $func=explode('@', $actions[count($actions)-1]);
            //控制器的名字
            $controllerName=$func[0];
            //方法
            $actionName=$func[1];
            // echo $controllerName.":".$actionName;
            //4.获取访问模块控制器和权限列表做对比
            //获取等于用户的所有权限
            $nodelist=session('nodelist');
            if(empty($nodelist[$controllerName]) || !in_array($actionName,$nodelist[$controllerName])){
                //提示
               return redirect("/ad")->with('error','抱歉,您没有权限访问该模块,请联系超级管理员(点击消失)');
            }
            //执行下个请求
            return $next($request);
        }else{
        //跳转到登录界面
            return redirect("/adlogin/create");
        }
    }
}
