<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUsersinsert extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'username'=>'required|unique:admin_users',
                //密码和重复密码
                'password'=>'required|regex:/\w{6,18}/',
                'repassword'=>'required|regex:/\w{6,18}/|same:password'
        ];
    }

    public function messages(){
         return[
                'username.required'=>'用户名不能为空',
                'username.unique'=>'用户名已存在',
                'password.required'=>'密码不能为空',
                'password.regex'=>'密码必须为6-18位的任意数字字母下划线',
                'repassword.required'=>'重复密码不能为空',
                'repassword.regex'=>'重复密码必须为6-18位的任意数字字母下划线',
                'repassword.same'=>'两次密码不一致'
            ];
    }
}
