<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUserinsert extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    //校验规则
    public function rules()
    {
        return [
                //会员名做规则设置
                'user'=>'required|regex:/\w{4,8}/|unique:user',
                //密码和重复密码
                'pass'=>'required|regex:/\w{6,16}/',
                // 'repassword'=>'required|regex:/\w{8,16}/|same:password',
                //邮箱
                'email'=>'required|email|unique:user',
                //电话
                'phone'=>'required|regex:/\d{11}/|unique:user'
        ];
    }

   public function messages(){
        return[
                'user.required'=>'用户名不能为空',
                'user.regex'=>'用户名必须为4-8位的任意数字字母下划线',
                'user.unique'=>'已有此用户',
                'pass.required'=>'密码不能为空',
                'pass.regex'=>'密码必须为6-16位的任意数字字母下划线',
                // 'repassword.required'=>'重复密码不能为空',
                // 'repassword.regex'=>'重复密码必须为8-16位的任意数字字母下划线',
                // 'repassword.same'=>'两次密码不一致',
                //邮箱
                'email.required'=>'邮箱不能为空',
                'email.email'=>'邮箱格式不符合',
                'email.unique'=>'已有此邮箱',
                //电话
                'phone.required'=>'电话不能为空',
                'phone.regex'=>'电话不符合规则',
                'phone.unique'=>'已有此号码'
            ];
    }


}
