<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// 前台模块
// Route::get('/', function () {
//     return view('Home.index');
// });

//首页
Route::get('/','Home\IndexController@home');
//首页的单个影视信息
Route::resource('/show','Home\IndexController');
//播放页
Route::get('/bofang/{id}','Home\IndexController@bofang');
//综合筛选页面
Route::resource('/yingshi','Home\YingController');

Route::get('/ying','Home\YingController@ying');

Route::get('/ju','Home\YingController@ju');

Route::get('/zong','Home\YingController@zong');

Route::get('/man','Home\YingController@man');

Route::get('/shaixuan/{id}','Home\YingController@shaixuan');
//ajax传值
Route::get('/getvalue','Home\YingController@getvalue');
// Route::get('/getshai','Home\YingController@getshai');
//会员限制
Route::get('/vippr',"Home\IndexController@vipget");
//首页注册
Route::resource('/reg','Home\RegController');
//ajax
Route::get('/checkphone','Home\RegController@checkphone');
//laravel结合云之讯调用短信接口
Route::get('/sendphone','Home\RegController@sendphone');
//检测校验码
Route::get('/checkcode','Home\RegController@checkcode');
//检测重复密码
Route::get('/checkmima','Home\RegController@checkmima');
//检测重复用户名
Route::get('/checkuser','Home\RegController@checkuser');
//首页登录
Route::resource('/login','Home\LoginController');
//用户中心
Route::resource('/user','Home\UserController');
//修改用户资料
Route::get('/edit/{id}','Home\UserController@useredit');

Route::resource('/img','Home\ImgeditController');
////修改用户头像
Route::get('/imgedit/{id}','Home\ImgeditController@doedit');
//用户充值
Route::get('/pay/{id}','Home\UserController@pay');
//支付宝接口调用
Route::post('/payed','Home\UserController@payed');
//通知给客户端的界面
Route::get("/returnurl","Home\UserController@returnurl");
//消费记录
Route::get("/order","Home\UserController@order");
//收藏视频
Route::get("/shoucang","Home\UserController@shoucang");
//ajax收藏视频
Route::get("/cang","Home\UserController@cang");
//删除收藏视频
Route::get("/delcang/{id}","Home\UserController@delcang");
//删除全部收藏视频
Route::get("/quandel/{id}","Home\UserController@quandel");
//我的文章
Route::get('/wenzhang/{id}',"Home\UserController@wenzhang");
//搜索视频
Route::resource('/search','Home\SearchController');
//交流圈
Route::resource('/jiaoliu','Home\JiaoController');
//交流圈分类
Route::get('/jiaotype/{id}','Home\JiaoController@jiaotype');
//回复
Route::post('/huifu','Home\JiaoController@huifu');
//ajax回复
Route::get('/gethui',"Home\JiaoController@gethui");
//浏览
Route::get('/liulan',"Home\JiaoController@liulan");
//点赞
Route::get('/zan',"Home\JiaoController@dianzan");
//查赞
Route::get('/chazan',"Home\JiaoController@chazan");

//找回密码
Route::resource('/zhao',"Home\ZhaoController");
//校验码
Route::get('/code',"Home\ZhaoController@code");
//改密
Route::get('/gaimi',"Home\ZhaoController@gaimi");







//后台登录
Route::resource('/adlogin','Admin\LoginController');

// 后台模块
Route::group(['middleware'=>'login'],function(){
	//后台首页
	Route::resource('/ad', 'Admin\IndexController');
	//后台管理员模块
	Route::resource('/adusers','Admin\AdminController');
	//分配角色
	Route::get('/adrole/{id}',"Admin\AdminController@role");
	//保存角色
	Route::post('/saverole',"Admin\AdminController@saverole");
	//角色管理
	Route::resource('/rolelist','Admin\RolelistController');
	//分配权限操作
	Route::get('/auth/{id}',"Admin\RolelistController@auth");
	//保存权限
	Route::post('/saveauth','Admin\RolelistController@saveauth');
	//权限管理
	Route::resource("/authlist","Admin\AuthlistController");
	//分类模块
	Route::resource("/adcate",'Admin\CateController');
	//年份管理
	Route::resource("/adyear",'Admin\YearController');
	//地区管理
	Route::resource("adcountry","Admin\CountryController");
	//视频模块
	Route::resource("/advideo",'Admin\VideoController');
	//ajax遍历 视频添加
	Route::get("/son_cate",'Admin\VideoController@son_cate');
	//后台用户 删除修改添加
	Route::resource('/aduser','Admin\UserController');
	//公告模块
	Route::resource('/adgg',"Admin\NoticeController");
	//轮播图模块
	Route::resource('/adlunbo',"Admin\LunboController");
	//会员订单
	Route::resource('/adorder','Admin\OrderController');
	//会员套餐
	Route::resource('/advip','Admin\VipController');
	//帖子管理
	Route::resource('/tiezi','Admin\TieController');
	//帖子类别
	Route::get('/tiezitype','Admin\TieController@tietype');
	//删除板块
	Route::get('/tiezidel/{id}','Admin\TieController@tiezidel');
});


